#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import logging

log = logging.getLogger(__name__)


def function():
    log.trace('Setting grains["company"] to "xanadu"')
    grains = {}
    grains['company'] = "xanadu"
    return grains
