{%- import 'xanadu-components/xdc1-integration-module-common/settings.sls' as xdc1imc with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdc1imc_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1imc.artifacts_url }}

xdc1imc_version_file:
  file.managed:
    - name: {{ xdc1imc.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1imc.artifact_id }}
        version: {{ xdc1imc.version }}
        sha1: {{ xdc1imc.hash }}
    - require:
      - health_check: xdc1imc_nexus_available

xdc1imc_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdc1imc.artifact_id }}-*
    - watch:
      - file: xdc1imc_version_file


xdc1imc_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdc1imc.artifact_id }}-{{ xdc1imc.version }}.jar
    - source: {{ xdc1imc.artifacts_war_content_url }}
    - source_hash: {{ xdc1imc.hash }}
    - user: {{ xdc1imc.user }}
    - group: {{ xdc1imc.group }}
    - mode: 644
    - watch:
      - file: xdc1imc_version_file
    - require:
      - cmd: xdc1imc_remove_previous_versions

xdc1imc_restart_container:
  cmd.run:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdc1imc_version_file
