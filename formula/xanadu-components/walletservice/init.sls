{%- import 'xanadu-components/walletservice/settings.sls' as walletservice with context %}

include:
  - third-party-components.tomcat

walletservice_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ walletservice.artifacts_url }}

walletservice_version_file:
  file.managed:
    - name: {{ walletservice.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ walletservice.artifact_id }}
        version: {{ walletservice.version }}
        sha1: {{ walletservice.hash }}
    - require:
      - health_check: walletservice_nexus_available

walletservice_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: walletservice_version_file


walletservice-api_clean:
  cmd.wait:
    - name: rm -rf {{ walletservice.war_loc }}
    - watch:
      - file: walletservice_version_file
    - require:
      - cmd: walletservice_stop

walletservice_install_war:
  file.managed:
    - name: {{ walletservice.war_loc }}
    - source: {{ walletservice.artifacts_war_content_url }}
    - source_hash: {{ walletservice.hash }}
    - require:
      - cmd: walletservice-api_clean
    - require_in:
      - service: tomcat_service


walletservice_log_permissions:
  file.directory:
    - name: {{ walletservice.logs }}
    - user: {{ walletservice.user }}
    - group: {{ walletservice.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: walletservice_install_war

successful_walletservice_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ walletservice.healtcheck_port }}/{{ walletservice.artifact_name }}/rest/ping
    - match_text: "pong"
    - require:
      - file: walletservice_install_war
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ walletservice.name }} version is {{ walletservice.version }} and {{ walletservice.hash }}"

{%- if walletservice.hipchat_enabled == True %}

walletservice_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ walletservice.auth_token }}
    - room_id: {{ walletservice.room_id }}
    - message: '[{{ walletservice.environment }}] - Artifact version [{{ walletservice.version }}] of component [{{ walletservice.artifact_name }}] was successfully deployed to the [{{ walletservice.environment }}] environment [{{ walletservice.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_walletservice_deployment
    - onchanges:
      - service: tomcat_service

walletservice_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ walletservice.auth_token }}
    - room_id: {{ walletservice.room_id }}
    - message: '[{{ walletservice.environment }}] - Deployment of artifact version [{{ walletservice.version }}] for component [{{ walletservice.artifact_name }}] to the [{{ walletservice.environment }}] environment [{{ walletservice.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: walletservice_hipchat_notifier_deploy_finished

{%- endif %}


{%- if walletservice.slack_enabled == True %}

walletservice_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ walletservice.slack_channel }}'
    - from_name: {{ walletservice.from }}
    - message: '*[{{ walletservice.environment }}]* - Artifact version *[{{ walletservice.version }}]* of component *[{{ walletservice.artifact_name }}]* was successfully deployed to the *[{{ walletservice.environment }}]* environment *[{{ walletservice.host }}]*.'
    - api_key: {{ walletservice.slack_auth_token }}
    - require:
      - health_check: successful_walletservice_deployment
    - onchanges:
      - service: tomcat_service

walletservice_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ walletservice.slack_channel }}'
    - from_name: {{ walletservice.from }}
    - message: '*[{{ walletservice.environment }}]* - Deployment of artifact version *[{{ walletservice.version }}]* for component *[{{ walletservice.artifact_name }}]* to the *[{{ walletservice.environment }}]* environment *[{{ walletservice.host }}]* failed.'
    - api_key: {{ walletservice.slack_auth_token }}
    - onfail:
      - slack: walletservice_slack_notifier_deploy_finished

{%- endif %}
