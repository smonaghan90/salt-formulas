{%- import 'xanadu-components/bp-admin/settings.sls' as bpadmin with context %}

include:
  - third-party-components.tomcat

bp-admin_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ bpadmin.artifacts_url }}

bp-admin_version_file:
  file.managed:
    - name: {{ bpadmin.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ bpadmin.artifact_id }}
        version: {{ bpadmin.version }}
        sha1: {{ bpadmin.hash }}
    - require:
      - health_check: bp-admin_nexus_available

bp-admin_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: bp-admin_version_file
      - file: bp-admin_props

bp-admin-api_clean:
  cmd.wait:
    - name: rm -rf {{ bpadmin.war_dir }}
    - watch:
      - file: bp-admin_version_file
      - file: bp-admin_props
    - require:
      - cmd: bp-admin_stop

bp-admin_install_war:
  file.managed:
    - name: {{ bpadmin.war_loc }}
    - source: {{ bpadmin.artifacts_war_content_url }}
    - source_hash: {{ bpadmin.hash }}
    - require:
      - cmd: bp-admin-api_clean
      - file: bp-admin_props
    - require_in:
      - service: tomcat_service

bp-admin_props:
  file.managed:
    - name: {{ bpadmin.config_dir }}/bp-admin-config.properties
    - source: {{ bpadmin.artifacts_props_content_url }}
    - user: {{ bpadmin.user }}
    - group: {{ bpadmin.group }}
    - makedirs: True
    - mode: 644
    - template: jinja

bp-admin_log_permissions:
  file.directory:
    - name: {{ bpadmin.logs }}
    - user: {{ bpadmin.user }}
    - group: {{ bpadmin.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: bp-admin_install_war

successful_bp-admin_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ bpadmin.healtcheck_port }}/{{ bpadmin.artifact_name }}
    - match_text: "Betting Platform Administration"
    - require:
      - file: bp-admin_install_war
      - file: bp-admin_props
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ bpadmin.name }} version is {{ bpadmin.version }} and {{ bpadmin.hash }}"

{%- if bpadmin.hipchat_enabled == True %}

bp-admin_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ bpadmin.auth_token }}
    - room_id: {{ bpadmin.room_id }}
    - message: '[{{ bpadmin.environment }}] - Artifact version [{{ bpadmin.version }}] of component [{{ bpadmin.artifact_name }}] was successfully deployed to the [{{ bpadmin.environment }}] environment [{{ bpadmin.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_bp-admin_deployment
    - onchanges:
      - service: tomcat_service

bp-admin_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ bpadmin.auth_token }}
    - room_id: {{ bpadmin.room_id }}
    - message: '[{{ bpadmin.environment }}] - Deployment of artifact version [{{ bpadmin.version }}] for component [{{ bpadmin.artifact_name }}] to the [{{ bpadmin.environment }}] environment [{{ bpadmin.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: bp-admin_hipchat_notifier_deploy_finished

{%- endif %}


{%- if bpadmin.slack_enabled == True %}

bpadmin_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ bpadmin.slack_channel }}'
    - from_name: {{ bpadmin.from }}
    - message: '*[{{ bpadmin.environment }}]* - Artifact version *[{{ bpadmin.version }}]* of component *[{{ bpadmin.artifact_name }}]* was successfully deployed to the *[{{ bpadmin.environment }}]* environment *[{{ bpadmin.host }}]*.'
    - api_key: {{ bpadmin.slack_auth_token }}
    - require:
      - health_check: successful_bp-admin_deployment
    - onchanges:
      - service: tomcat_service

bpadmin_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ bpadmin.slack_channel }}'
    - from_name: {{ bpadmin.from }}
    - message: '*[{{ bpadmin.environment }}]* - Deployment of artifact version *[{{ bpadmin.version }}]* for component *[{{ bpadmin.artifact_name }}]* to the *[{{ bpadmin.environment }}]* environment *[{{ bpadmin.host }}]* failed.'
    - api_key: {{ bpadmin.slack_auth_token }}
    - onfail:
      - slack: bpadmin_slack_notifier_deploy_finished

{%- endif %}
