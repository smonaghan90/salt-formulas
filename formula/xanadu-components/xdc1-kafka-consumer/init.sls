{%- import 'xanadu-components/xdc1-kafka-consumer/settings.sls' as xdc1kc with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdc1kc_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1kc.artifacts_url }}

xdc1kc_version_file:
  file.managed:
    - name: {{ xdc1kc.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1kc.artifact_id }}
        version: {{ xdc1kc.version }}
        sha1: {{ xdc1kc.hash }}
    - require:
      - health_check: xdc1kc_nexus_available

xdc1kc_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdc1kc.artifact_id }}-*
    - watch:
      - file: xdc1kc_version_file


xdc1kc_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdc1kc.artifact_id }}-{{ xdc1kc.version }}.jar
    - source: {{ xdc1kc.artifacts_war_content_url }}
    - source_hash: {{ xdc1kc.hash }}
    - user: {{ xdc1kc.user }}
    - group: {{ xdc1kc.group }}
    - mode: 644
    - watch:
      - file: xdc1kc_version_file
    - require:
      - cmd: xdc1kc_remove_previous_versions

xdc1kc_restart_container:
  cmd.run:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdc1kc_version_file
