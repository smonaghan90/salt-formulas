{%- import 'xanadu-components/xdc-subscriptions/settings.sls' as xdcsubscriptions with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcsubscriptions_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcsubscriptions.artifacts_url }}

xdcsubscriptions_version_file:
  file.managed:
    - name: {{ xdcsubscriptions.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcsubscriptions.artifact_id }}
        version: {{ xdcsubscriptions.version }}
        sha1: {{ xdcsubscriptions.hash }}
    - require:
      - health_check: xdcsubscriptions_nexus_available

xdcsubscriptions_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{  xdcsubscriptions.basedir }}/{{  xdcsubscriptions.artifact_id }}-*

xdcsubscriptions_get_war_file:
  file.managed:
    - name: {{ xdcsubscriptions.jar_loc }}
    - source: {{ xdcsubscriptions.artifacts_war_content_url }}
    - source_hash: {{ xdcsubscriptions.hash }}
    - require:
      - cmd: xdcsubscriptions_remove_previous_versions

#Upload artifact to HDFS
xdcsubscriptions_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcsubscriptions.admin_server }}:9393/modules/processor/{{ xdcsubscriptions.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcsubscriptions.jar_loc }}'
    - watch:
      - file: xdcsubscriptions_version_file

#xdcsubscriptions_remove_stream:
 #cmd.run:
  #  - name:  'curl -s -X DELETE {{ xdcsubscriptions.admin_server }}:9393/streams/definitions/{{ xdcsubscriptions.stream_name }} '


#xdcsubscriptions_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcsubscriptions.admin_server }}:9393/streams/definitions -d name={{ xdcsubscriptions.stream_name }} -d "definition= http --port=9003 | xdc-json-to-object | xdc-subscriptions | xdc-object-to-json |file --name=outfile" -d deploy=true'
#    - watch:
#      - file: xdcsubscriptions_version_file
