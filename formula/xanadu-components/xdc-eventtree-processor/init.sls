{%- import 'xanadu-components/xdc-eventtree-processor/settings.sls' as xdcetprocessor with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcetprocessor_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcetprocessor.artifacts_url }}

xdcetprocessor_version_file:
  file.managed:
    - name: {{ xdcetprocessor.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcetprocessor.artifact_id }}
        version: {{ xdcetprocessor.version }}
        sha1: {{ xdcetprocessor.hash }}
    - require:
      - health_check: xdcetprocessor_nexus_available

xdcetprocessor_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcetprocessor.basedir }}/{{ xdcetprocessor.artifact_id }}-*

xdcetprocessor_get_war_file:
  file.managed:
    - name: {{ xdcetprocessor.jar_loc }}
    - source: {{ xdcetprocessor.artifacts_war_content_url }}
    - source_hash: {{ xdcetprocessor.hash }}
    - require:
      - cmd: xdcetprocessor_remove_previous_versions

#Upload artifact to HDFS
xdcetprocessor_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcetprocessor.admin_server }}:9393/modules/processor/{{ xdcetprocessor.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcetprocessor.jar_loc }}'
    - watch:
      - file: xdcetprocessor_version_file

#xdcetprocessor_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdcetprocessor.admin_server }}:9393/streams/definitions/{{ xdcetprocessor.stream_name }} '


#xdcetprocessor_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcetprocessor.admin_server }}:9393/streams/definitions -d name={{ xdcetprocessor.stream_name }} -d "definition=http | xdc-json-to-object | xdc-eventtree-processor | xdc-object-to-json |file --name=outfile" -d deploy=true'
#    - watch:
#      - file: xdcetprocessor_version_file
