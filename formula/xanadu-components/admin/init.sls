{%- import 'xanadu-components/admin/settings.sls' as admin with context %}

include:
  - third-party-components.tomcat

admin_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ admin.artifacts_url }}

admin_version_file:
  file.managed:
    - name: {{ admin.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ admin.artifact_id }}
        version: {{ admin.version }}
        sha1: {{ admin.hash }}
    - require:
      - health_check: admin_nexus_available

admin_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: admin_version_file


admin-api_clean:
  cmd.wait:
    - name: rm -rf {{ admin.war_loc }}
    - watch:
      - file: admin_version_file
    - require:
      - cmd: admin_stop

admin_install_war:
  file.managed:
    - name: {{ admin.war_loc }}
    - source: {{ admin.artifacts_war_content_url }}
    - source_hash: {{ admin.hash }}
    - require:
      - cmd: admin-api_clean
    - require_in:
      - service: tomcat_service


admin_log_permissions:
  file.directory:
    - name: {{ admin.logs }}
    - user: {{ admin.user }}
    - group: {{ admin.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: admin_install_war

successful_admin_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in secondsf
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ admin.healtcheck_port }}/{{ admin.artifact_name }}/rest/ping
    - match_text: "pong"
    - require:
      - file: admin_install_war
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ admin.name }} version is {{ admin.version }} and {{ admin.hash }}"

{%- if admin.hipchat_enabled == True %}

admin_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ admin.auth_token }}
    - room_id: {{ admin.room_id }}
    - message: '[{{ admin.environment }}] - Artifact version [{{ admin.version }}] of component [{{ admin.artifact_name }}] was successfully deployed to the [{{ admin.environment }}] environment [{{ admin.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_admin_deployment
    - onchanges:
      - service: tomcat_service

admin_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ admin.auth_token }}
    - room_id: {{ admin.room_id }}
    - message: '[{{ admin.environment }}] - Deployment of artifact version [{{ admin.version }}] for component [{{ admin.artifact_name }}] to the [{{ admin.environment }}] environment [{{ admin.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: admin_hipchat_notifier_deploy_finished

{%- endif %}

{%- if admin.slack_enabled == True %}

admin_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ admin.slack_channel }}'
    - from_name: {{ admin.from }}
    - message: '*[{{ admin.environment }}]* - Artifact version *[{{ admin.version }}]* of component *[{{ admin.artifact_name }}]* was successfully deployed to the *[{{ admin.environment }}]* environment *[{{ admin.host }}]*.'
    - api_key: {{ admin.slack_auth_token }}
    - require:
      - health_check: successful_admin_deployment
    - onchanges:
      - service: tomcat_service

admin_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ admin.slack_channel }}'
    - from_name: {{ admin.from }}
    - message: '*[{{ admin.environment }}]* - Deployment of artifact version *[{{ admin.version }}]* for component *[{{ admin.artifact_name }}]* to the *[{{ admin.environment }}]* environment *[{{ admin.host }}]* failed.'
    - api_key: {{ admin.slack_auth_token }}
    - onfail:
      - slack: admin_slack_notifier_deploy_finished

{%- endif %}
