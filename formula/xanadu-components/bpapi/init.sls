{%- import 'xanadu-components/bpapi/settings.sls' as bpapi with context %}

include:
  - third-party-components.tomcat

bpapi_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ bpapi.artifacts_url }}

bpapi_version_file:
  file.managed:
    - name: {{ bpapi.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ bpapi.artifact_id }}
        version: {{ bpapi.version }}
        sha1: {{ bpapi.hash }}
    - require:
      - health_check: bpapi_nexus_available

bpapi_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: bpapi_version_file
      - file: bpapi_props

bpapi-api_clean:
  cmd.wait:
    - name: rm -rf {{ bpapi.war_dir }}
    - watch:
      - file: bpapi_version_file
      - file: bpapi_props
    - require:
      - cmd: bpapi_stop

bpapi_install_war:
  file.managed:
    - name: {{ bpapi.war_loc }}
    - source: {{ bpapi.artifacts_war_content_url }}
    - source_hash: {{ bpapi.hash }}
    - user: {{ bpapi.user }}
    - group: {{ bpapi.group }}
    - mode: 744
    - require:
      - cmd: bpapi-api_clean
      - file: bpapi_props
    - require_in:
      - service: tomcat_service

bpapi_props:
  file.managed:
    - name: {{ bpapi.config_dir }}/bpapi-config.properties
    - source: {{ bpapi.artifacts_props_content_url }}
    - user: {{ bpapi.user }}
    - group: {{ bpapi.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - require_in:
      - service: tomcat_service

bpapi_log_permissions:
  file.directory:
    - name: {{ bpapi.logs }}
    - user: {{ bpapi.user }}
    - group: {{ bpapi.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require_in:
      - service: tomcat_service

successful_bpapi_deployment:
  health_check.wait_for_url:
    - timeout: 180 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ bpapi.healtcheck_port }}/{{ bpapi.artifact_name }}/rest/info
    - match_text: "application-info"
    - require:
      - file: bpapi_install_war
      - file: bpapi_props
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ bpapi.name }} version is {{ bpapi.version }} and {{ bpapi.hash }}"

{%- if bpapi.hipchat_enabled == True %}

bpapi_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ bpapi.auth_token }}
    - room_id: {{ bpapi.room_id }}
    - message: '[{{ bpapi.environment }}] - Artifact version [{{ bpapi.version }}] of component [{{ bpapi.artifact_name }}] was successfully deployed to the [{{ bpapi.environment }}] environment [{{ bpapi.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_bpapi_deployment
    - onchanges:
      - service: tomcat_service

bpapi_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ bpapi.auth_token }}
    - room_id: {{ bpapi.room_id }}
    - message: '[{{ bpapi.environment }}] - Deployment of artifact version [{{ bpapi.version }}] for component [{{ bpapi.artifact_name }}] to the [{{ bpapi.environment }}] environment [{{ bpapi.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: bpapi_hipchat_notifier_deploy_finished

{%- endif %}


{%- if bpapi.slack_enabled == True %}

bpapi_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ bpapi.slack_channel }}'
    - from_name: {{ bpapi.from }}
    - message: '*[{{ bpapi.environment }}]* - Artifact version *[{{ bpapi.version }}]* of component *[{{ bpapi.artifact_name }}]* was successfully deployed to the *[{{ bpapi.environment }}]* environment *[{{ bpapi.host }}]*.'
    - api_key: {{ bpapi.slack_auth_token }}
    - require:
      - health_check: successful_bpapi_deployment
    - onchanges:
      - service: tomcat_service

bpapi_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ bpapi.slack_channel }}'
    - from_name: {{ bpapi.from }}
    - message: '*[{{ bpapi.environment }}]* - Deployment of artifact version *[{{ bpapi.version }}]* for component *[{{ bpapi.artifact_name }}]* to the *[{{ bpapi.environment }}]* environment *[{{ bpapi.host }}]* failed.'
    - api_key: {{ bpapi.slack_auth_token }}
    - onfail:
      - slack: bpapi_slack_notifier_deploy_finished

{%- endif %}
