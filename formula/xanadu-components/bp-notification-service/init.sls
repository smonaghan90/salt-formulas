{%- import 'xanadu-components/bp-notification-service/settings.sls' as bpns with context %}
{%- from "xanadu-components/bp-notification-service/map.jinja" import bpns_map with context %}

include:
  - third-party-components.java

bpns_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ bpns.artifacts_url }}

bpns_version_file:
  file.managed:
    - name: {{ bpns.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ bpns.artifact_id }}
        version: {{ bpns.version }}
        sha1: {{ bpns.hash }}
    - require:
      - health_check: bpns_nexus_available

bpns_stop:
  cmd.wait:
    - name: service bp-notification-service stop
    - onlyif: ps -ef | grep bp-notification-service | grep -v grep
    - watch:
      - file: bpns_version_file
    - require:
      - file: {{ bpns_map.service_script }}

bpns_clean:
  cmd.wait:
    - name: rm -rf {{ bpns.home }}
    - watch:
      - file: bpns_version_file
    - require:
      - cmd: bpns_stop

bpns_props:
  file.managed:
    - name: {{ bpns.config_dir }}/application-{{ bpns.environment }}.properties
    - source: {{ bpns.artifacts_props_content_url }}
    - user: {{ bpns.user }}
    - group: {{ bpns.group }}
    - makedirs: True
    - mode: 644
    - template: jinja

bpns_install_jar:
  file.managed:
    - name: {{ bpns.jar_loc }}
    - source: {{ bpns.artifacts_jar_content_url }}
    - source_hash: {{ bpns.hash }}
    - user: {{ bpns.user }}
    - group: {{ bpns.group }}
    - makedirs: True
    - mode: 644
    - require:
      - cmd: bpns_stop

bpns_log_permissions:
  file.directory:
    - name: {{ bpns.logs }}
    - user: {{ bpns.user }}
    - group: {{ bpns.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: bpns_install_jar

{%- if bpns_map.service_script %}
{{ bpns_map.service_script }}:
  file.managed:
    - source: {{ bpns.tmpl_file_src }}/{{ bpns_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ bpns_map.service_script_mode }}
    - template: jinja
    - context:
       jar_loc: {{ bpns.jar_loc }}
       config_dir: {{ bpns.config_dir }}
       environment: {{ bpns.environment }}
       user: {{ bpns.user }}
       name: {{ bpns.name }}
       java_opts: {{ bpns.java_opts }}

bpns_service:
  service.running:
    - name: bp-notification-service
    - sig: java -Dspring
    - require:
      - file: bpns_install_jar
      - file: {{ bpns_map.service_script }}
    - watch:
      - file: bpns_props
      - file: bpns_install_jar
      - file: {{ bpns_map.service_script }}
{%- endif %}

successful_bpns_deployment:
#  health_check.wait_for_url:
#    - timeout: 60 #this is in seconds
#    - url: http://{{ salt['grains.get']('fqdn') }}:{{ bpns.healtcheck_port }}
#    - match_text: status":""
#    - require:
#      - file: bpns_install_jar
#      - file: bpns_props
#      - service: bpns_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ bpns.name }} version is {{ bpns.version }} and {{ bpns.hash }}"


{%- if bpns.hipchat_enabled == True %}

bpns_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ bpns.auth_token }}
    - room_id: {{ bpns.room_id }}
    - message: '[{{ bpns.environment }}] - Artifact version [{{ bpns.version }}] of component [{{ bpns.artifact_name }}] was successfully deployed to the [{{ bpns.environment }}] environment [{{ bpns.host }}].'
    - html: True
    - message_color: green
    - notify: True
#    - require:
#      - health_check: successful_bpns_deployment
    - onchanges:
      - service: bpns_service

bpns_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ bpns.auth_token }}
    - room_id: {{ bpns.room_id }}
    - message: '[{{ bpns.environment }}] - Deployment of artifact version [{{ bpns.version }}] for component [{{ bpns.artifact_name }}] to the [{{ bpns.environment }}] environment [{{ bpns.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: bpns_hipchat_notifier_deploy_finished

{%- endif %}


{%- if bpns.slack_enabled == True %}

bpns_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ bpns.slack_channel }}'
    - from_name: {{ bpns.from }}
    - message: '*[{{ bpns.environment }}]* - Artifact version *[{{ bpns.version }}]* of component *[{{ bpns.artifact_name }}]* was successfully deployed to the *[{{ bpns.environment }}]* environment *[{{ bpns.host }}]*.'
    - api_key: {{ bpns.slack_auth_token }}
#    - require:
#      - health_check: successful_bpns_deployment
    - onchanges:
      - service: bpns_service

bpns_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ bpns.slack_channel }}'
    - from_name: {{ bpns.from }}
    - message: '*[{{ bpns.environment }}]* - Deployment of artifact version *[{{ bpns.version }}]* for component *[{{ bpns.artifact_name }}]* to the *[{{ bpns.environment }}]* environment *[{{ bpns.host }}]* failed.'
    - api_key: {{ bpns.slack_auth_token }}
    - onfail:
      - slack: bpns_slack_notifier_deploy_finished

{%- endif %}
