{%- import 'xanadu-components/xdc-object-to-json/settings.sls' as xdcotj with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcotj_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcotj.artifacts_url }}

xdcotj_version_file:
  file.managed:
    - name: {{ xdcotj.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcotj.artifact_id }}
        version: {{ xdcotj.version }}
        sha1: {{ xdcotj.hash }}
    - require:
      - health_check: xdcotj_nexus_available

xdcotj_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcotj.basedir }}/{{ xdcotj.artifact_id }}-*

xdcotj_get_war_file:
  file.managed:
    - name: {{ xdcotj.jar_loc }}
    - source: {{ xdcotj.artifacts_war_content_url }}
    - source_hash: {{ xdcotj.hash }}
    - require:
      - cmd: xdcotj_remove_previous_versions

#Upload artifact to HDFS
xdcotj_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcotj.admin_server }}:9393/modules/processor/{{ xdcotj.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcotj.jar_loc }}'
    - watch:
      - file: xdcotj_version_file

#xdcotj_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdcotj.admin_server }}:9393/streams/definitions/{{ xdcotj.stream_name }} '


#xdcotj_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcotj.admin_server }}:9393/streams/definitions -d name={{ xdcotj.stream_name }} -d "definition= http | xdc-json-to-object | {{ xdcotj.artifact_id }} | log" -d deploy=true'
#    - watch:
#      - file: xdcotj_version_file
