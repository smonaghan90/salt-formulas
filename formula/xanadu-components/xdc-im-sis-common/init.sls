{%- import 'xanadu-components/xdc-im-sis-common/settings.sls' as xdcimsisc with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcimsisc_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcimsisc.artifacts_url }}

xdcimsisc_version_file:
  file.managed:
    - name: {{ xdcimsisc.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcimsisc.artifact_id }}
        version: {{ xdcimsisc.version }}
        sha1: {{ xdcimsisc.hash }}
    - require:
      - health_check: xdcimsisc_nexus_available

xdcimsisc_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcimsisc.basedir }}/{{ xdcimsisc.artifact_id }}-*

xdcimsisc_get_war_file:
  file.managed:
    - name: {{ xdcimsisc.jar_loc }}
    - source: {{ xdcimsisc.artifacts_war_content_url }}
    - source_hash: {{ xdcimsisc.hash }}
    - require:
      - cmd: xdcimsisc_remove_previous_versions

#Upload artifact to HDFS
xdcimsisc_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcimsisc.admin_server }}:9393/modules/processor/{{ xdcimsisc.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcimsisc.jar_loc }}'
    - watch:
      - file: xdcimsisc_version_file

#xdcimsisc_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdcimsisc.admin_server }}:9393/streams/definitions/{{ xdcimsisc.stream_name }} '


#xdcimsisc_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcimsisc.admin_server }}:9393/streams/definitions -d name={{ xdcimsisc.stream_name }} -d "definition={{ xdcimsisc.artifact_id }}" -d deploy=true'
#    - watch:
#      - file: xdcimsisc_version_file
