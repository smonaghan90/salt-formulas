{%- import 'xanadu-components/xdc-im-smartodds/settings.sls' as xdcimsmartodds with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcimsmartodds_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcimsmartodds.artifacts_url }}

xdcimsmartodds_version_file:
  file.managed:
    - name: {{ xdcimsmartodds.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcimsmartodds.artifact_id }}
        version: {{ xdcimsmartodds.version }}
        sha1: {{ xdcimsmartodds.hash }}
    - require:
      - health_check: xdcimsmartodds_nexus_available

xdcimsmartodds_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcimsmartodds.basedir }}/{{ xdcimsmartodds.artifact_id }}-*

xdcimsmartodds_get_war_file:
  file.managed:
    - name: {{ xdcimsmartodds.jar_loc }}
    - source: {{ xdcimsmartodds.artifacts_war_content_url }}
    - source_hash: {{ xdcimsmartodds.hash }}
    - require:
      - cmd: xdcimsmartodds_remove_previous_versions

#Upload artifact to HDFS
xdcimsmartodds_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcimsmartodds.admin_server }}:9393/modules/processor/{{ xdcimsmartodds.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcimsmartodds.jar_loc }}'
    - watch:
      - file: xdcimsmartodds_version_file
