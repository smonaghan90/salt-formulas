{%- import 'xanadu-components/xdc-prices-processor/settings.sls' as xdcpriprocessor with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcpriprocessor_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcpriprocessor.artifacts_url }}

xdcpriprocessor_version_file:
  file.managed:
    - name: {{ xdcpriprocessor.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcpriprocessor.artifact_id }}
        version: {{ xdcpriprocessor.version }}
        sha1: {{ xdcpriprocessor.hash }}
    - require:
      - health_check: xdcpriprocessor_nexus_available

xdcpriprocessor_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcpriprocessor.basedir }}/{{ xdcpriprocessor.artifact_id }}-*


xdcpriprocessor_get_war_file:
  file.managed:
    - name: {{ xdcpriprocessor.jar_loc }}
    - source: {{ xdcpriprocessor.artifacts_war_content_url }}
    - source_hash: {{ xdcpriprocessor.hash }}
    - require:
      - cmd: xdcpriprocessor_remove_previous_versions

#Upload artifact to HDFS
xdcpriprocessor_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcpriprocessor.admin_server }}:9393/modules/processor/{{ xdcpriprocessor.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcpriprocessor.jar_loc }}'
    - watch:
      - file: xdcpriprocessor_version_file

#xdcpriprocessor_remove_stream:
 #cmd.run:
  #  - name:  'curl -s -X DELETE {{ xdcpriprocessor.admin_server }}:9393/streams/definitions/{{ xdcpriprocessor.stream_name }} '


#xdcpriprocessor_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcpriprocessor.admin_server }}:9393/streams/definitions -d name={{ xdcpriprocessor.stream_name }} -d "definition= http --port=9003 | xdc-json-to-object | xdc-prices-processor | xdc-object-to-json |file --name=outfile" -d deploy=true'
#    - watch:
#      - file: xdcpriprocessor_version_file
