{%- import 'xanadu-components/xdc-im-sis/settings.sls' as xdcimsisp with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcimsisp_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcimsisp.artifacts_url }}

xdcimsisp_version_file:
  file.managed:
    - name: {{ xdcimsisp.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcimsisp.artifact_id }}
        version: {{ xdcimsisp.version }}
        sha1: {{ xdcimsisp.hash }}
    - require:
      - health_check: xdcimsisp_nexus_available

xdcimsisp_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcimsisp.basedir }}/{{ xdcimsisp.artifact_id }}-*

xdcimsisp_get_war_file:
  file.managed:
    - name: {{ xdcimsisp.jar_loc }}
    - source: {{ xdcimsisp.artifacts_war_content_url }}
    - source_hash: {{ xdcimsisp.hash }}
    - require:
      - cmd: xdcimsisp_remove_previous_versions

#Upload artifact to HDFS
xdcimsisp_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcimsisp.admin_server }}:9393/modules/processor/{{ xdcimsisp.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcimsisp.jar_loc }}'
    - watch:
      - file: xdcimsisp_version_file

#xdcimsisp_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdcimsisp.admin_server }}:9393/streams/definitions/{{ xdcimsisp.stream_name }} '


#xdcimsisp_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcimsisp.admin_server }}:9393/streams/definitions -d name={{ xdcimsisp.stream_name }} -d "definition={{ xdcimsisp.artifact_id }}" -d deploy=true'
#    - watch:
#      - file: xdcimsisp_version_file
