{%- import 'xanadu-components/message-generator/settings.sls' as mg with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

mg_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ mg.artifacts_url }}

mg_version_file:
  file.managed:
    - name: {{ mg.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ mg.artifact_id }}
        version: {{ mg.version }}
        sha1: {{ mg.hash }}
    - require:
      - health_check: mg_nexus_available

mg_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ mg.basedir }}/{{ mg.artifact_id }}-*


mg_get_war_file:
  file.managed:
    - name: {{ mg.jar_loc }}
    - source: {{ mg.artifacts_war_content_url }}
    - source_hash: {{ mg.hash }}
    - require:
      - cmd: mg_remove_previous_versions

message-generator-api_props:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/{{ mg.artifact_id }}.properties
    - source: {{ mg.artifacts_props_content_url }}
    - source_hash: {{ mg.artifacts_props_hash }}
    - user: {{ mg.user }}
    - group: {{ mg.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - watch:
      - file: mg_version_file


#Upload artifact + properties to HDFS
mg_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ mg.admin_server }}:9393/modules/source/{{ mg.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ mg.jar_loc }}'
    - watch:
      - file: mg_version_file

#mg_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ mg.admin_server }}:9393/streams/definitions/{{ mg.stream_name }} '


#mg_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ mg.admin_server }}:9393/streams/definitions -d name={{ mg.stream_name }} -d "definition= http | xdc-json-to-object | {{ mg.artifact_id }} | log" -d deploy=true'
#    - watch:
#      - file: mg_version_file
