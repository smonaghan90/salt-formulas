{%- import 'xanadu-components/xdc-mapping-editor/settings.sls' as mappingui with context %}
{%- import 'third-party-components/nginx/settings.sls' as nx with context %}

include:
  - third-party-components.nginx

xdc-mapping-editor_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ mappingui.artifacts_url }}

xdc-mapping-editor_version_file:
  file.managed:
    - name: {{ mappingui.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ mappingui.artifact_id }}
        version: {{ mappingui.version }}
        sha1: {{ mappingui.hash }}
    - require:
      - health_check: xdc-mapping-editor_nexus_available

xdc-mapping-editor_stop:
  cmd.wait:
    - name: service nginx stop
    - onlyif: ps -ef | grep nginx | grep -v grep
    - watch:
      - file: xdc-mapping-editor_version_file

xdc-mapping-editor_clean:
  cmd.wait:
    - name: rm -rf {{ mappingui.home }}
    - watch:
      - file: xdc-mapping-editor_version_file
    - require:
      - cmd: xdc-mapping-editor_stop

xdc-mapping-editor_install_tar:
  archive.extracted:
    - name: {{ mappingui.basedir }}/{{ mappingui.name }}
    - source: {{ mappingui.artifacts_tar_content_url }}
    - source_hash: {{ mappingui.hash }}
    - archive_format: tar
    - if_missing: {{ mappingui.home }}
    - require:
      - cmd: xdc-mapping-editor_clean
    - require_in:
      - service: nginx_service

xdc-mapping-editor_permissions:
  file.directory:
    - name: {{ mappingui.basedir }}/{{ mappingui.name }}
    - user: {{ mappingui.user }}
    - group: {{ mappingui.group }}
    - mode: 744
    - recurse:
      - user
      - group
      - mode
    - require:
      - archive: xdc-mapping-editor_install_tar
    - require_in:
      - service: nginx_service

mapping_editor_ui_remove_defaults:
  cmd.wait:
    - name: rm -r {{ nx.nginx_conf }}/sites-enabled/*
    - watch:
      - cmd: xdc-mapping-editor_stop

mapping_editor_ui_desktop_site_conf:
  file.managed:
    - name: {{ nx.nginx_conf }}/sites-enabled/desktop
    - source: {{ mappingui.tmpl_files}}/desktop
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
      user : {{ nx.user }}
      logs_dir : {{ nx.logs_dir }}
      api_host: {{ mappingui.api_host }}
      api_port: {{ mappingui.api_port }}
      host_name: {{ mappingui.host_name }}
      ui_port: {{ mappingui.ui_port }}
      desktop_assets: {{ mappingui.desktop_assets }}
    - require:
      - cmd: mapping_editor_ui_remove_defaults
    - require_in:
      - service: nginx_service

successful_xdc-mapping-editor_deployment:
  health_check.wait_for_url:
    - timeout: 10 #this is in seconds
    - url: http://{{ mappingui.host_name }}:{{ mappingui.ui_port }}
    - match_text: LOADING
    - require:
      - archive: xdc-mapping-editor_install_tar
      - file: xdc-mapping-editor_permissions
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ mappingui.name }} version is {{ mappingui.version }} and hash is {{ mappingui.hash }}"

{%- if mappingui.hipchat_enabled == True %}

xdc-mapping-editor_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ mappingui.auth_token }}
    - room_id: {{ mappingui.room_id }}
    - message: '[{{ mappingui.environment }}] - Artifact version [{{ mappingui.version }}] of component [{{ mappingui.artifact_name }}] was successfully deployed to the [{{ mappingui.environment }}] environment [{{ mappingui.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_xdc-mapping-editor_deployment
    - onchanges:
      - service: nginx_service

xdc-mapping-editor_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ mappingui.auth_token }}
    - room_id: {{ mappingui.room_id }}
    - message: '[{{ mappingui.environment }}] - Deployment of artifact version [{{ mappingui.version }}] for component [{{ mappingui.artifact_name }}] to the [{{ mappingui.environment }}] environment [{{ mappingui.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: xdc-mapping-editor_hipchat_notifier_deploy_finished

{%- endif %}
