#nexus base conf
{% set artifacts_url = pillar['component_base_url'] %}
{% set view_api = "service/local/artifact/maven/resolve" %}
{% set group = "com.xanaduconsultancy" %}
{% set environment = salt['grains.get']('environment') %}
{% set artifact_name = pillar[environment]['xdc-mapping-editor']['component']  %}
{% set repo_version  = salt['pillar.get'](environment + ':xdc-mapping-editor:repo_version', "LATEST") %}
{% set repository_id  = salt['pillar.get']('repository_id', "snapshots") %}
{% set content_api = "service/local/artifact/maven/content" %}
{% set extension = "tar.gz" %}
{% set sha1_extension = "tar.gz.sha1" %}

#nexus api
{% set hash = 'sha1=' + salt['cmd.run']( 'curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + '&e=' + extension  + "\"" + ' | xmllint --xpath \"/artifact-resolution/data/sha1/text()\" -' ) %}
{% set version = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + '&e=' + extension  + "\"" + ' | xmllint --xpath "/artifact-resolution/data/version/text()" -') %}
{% set artifact_id = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + '&e=' + sha1_extension  + "\"" + ' | xmllint --xpath "/artifact-resolution/data/artifactId/text()" -') %}
{% set artifacts_tar_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + '&e=' + extension %}

#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#base conf
{% set name = pillar[environment]['xdc-mapping-editor']['component'] %}
{% set home = pillar['base_install_dir'] + "/" + name %}
{% set host = salt['grains.get']('host') %}

{% set basedir =  pillar['base_install_dir'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_xdc-mapping-editor.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar[environment]['xdc-mapping-editor']['component'] %}
{% set config_dir = basedir + "/" + pillar[environment]['xdc-mapping-editor']['component'] + "/props" %}



{% set p  = salt['pillar.get']('xdc-mapping-editor_config', {}) %}
{%- set host_name = p.get('host_name' , 'ui' )  %}
{%- set ui_port = p.get('desktop_port' , '50080') %}
{%- set desktop_assets = p.get('desktop_assets' , 'desktop_assets') %}
{%- set api_port = p.get('api_port' , '8090') %}
{%- set api_host = p.get('api_host' , '192.168.0.0') %}


#conf
{% set p  = salt['pillar.get']('nginx', {}) %}
{%- set user = p.get('user', 'nginx') %}
{%- set group = p.get('group', 'nginx') %}
{%- set tech_group = p.get('tech_group', '_tech') %}

#templates
{% set tmpl_files = salt['pillar.get']('xdc-mapping-editor_config:tmpl_files','salt://' + environment + '/xdc-mapping-editor/files/') %}
