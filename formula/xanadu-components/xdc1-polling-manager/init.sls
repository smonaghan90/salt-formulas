{%- import 'xanadu-components/xdc1-polling-manager/settings.sls' as xdc1pm with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer


xdc1pm_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1pm.artifacts_url }}

xdc1pm_version_file:
  file.managed:
    - name: {{ xdc1pm.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1pm.artifact_id }}
        version: {{ xdc1pm.version }}
        sha1: {{ xdc1pm.hash }}
    - require:
      - health_check: xdc1pm_nexus_available

xdc1pm_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdc1pm.basedir }}/{{ xdc1pm.artifact_id }}-*

xdc1pm_get_war_file:
  file.managed:
    - name: {{ xdc1pm.jar_loc }}
    - source: {{ xdc1pm.artifacts_war_content_url }}
    - source_hash: {{ xdc1pm.hash }}
    - require:
      - cmd: xdc1pm_remove_previous_versions

xdc1pm-api_props:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/modules/source/{{ xdc1pm.artifact_id }}/{{ xdc1pm.artifact_id }}.properties
    - source: {{ xdc1pm.artifacts_props_content_url }}
    - source_hash: {{ xdc1pm.artifacts_props_hash }}
    - user: {{ xdc1pm.user }}
    - group: {{ xdc1pm.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - watch:
      - file: xdc1pm_version_file

xdc-polling-manager-rules-xsd:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/{{ xdc1pm.artifact_id }}/rules/rules.xsd
    - source: {{ xdc1pm.tmpl_file_src}}/rules.xsd
    - user: {{ xdc1pm.user }}
    - group: {{ xdc1pm.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - watch:
      - file: xdc1pm_version_file


xdc1pm-keystore:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/modules/source/{{ xdc1pm.artifact_id }}/quartz.properties
    - source: {{ xdc1pm.tmpl_file_src }}/quartz.properties
    - user: {{ xdc1pm.user }}
    - group: {{ xdc1pm.group }}
    - makedirs: True
    - mode: 644
    - watch:
      - file: xdc1pm_version_file



xdc1pm_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdc1pm.admin_server }}:9393/modules/source/{{ xdc1pm.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdc1pm.jar_loc }}'
    - watch:
      - file: xdc1pm_version_file



{%- if xdc1pm.hipchat_enabled == True %}

xdc1pm_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ xdc1pm.auth_token }}
    - room_id: {{ xdc1pm.room_id }}
    - message: '[{{ xdc1pm.environment }}] - Uploaded version [{{ xdc1pm.version }}] of component [{{ xdc1pm.artifact_name }}] to the [{{ xdc1pm.environment }}] environment [{{ xdc1pm.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - onchanges:
      - file: xdc1pm_version_file

xdc1pm_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ xdc1pm.auth_token }}
    - room_id: {{ xdc1pm.room_id }}
    - message: '[{{ xdc1pm.environment }}] - Uploaded  artifact version [{{ xdc1pm.version }}] for component [{{ xdc1pm.artifact_name }}] to the [{{ xdc1pm.environment }}] environment [{{ xdc1pm.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: xdc1pm_hipchat_notifier_deploy_finished

{%- endif %}


#xdc1pm_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdc1pm.admin_server }}:9393/streams/definitions/{{ xdc1pm.stream_name }} '


#xdc1pm_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdc1pm.admin_server }}:9393/streams/definitions -d name={{ xdc1pm.stream_name }} -d "definition={{ xdc1pm.artifact_id }}" -d deploy=true'
#    - watch:
#      - file: xdc1pm_version_file
