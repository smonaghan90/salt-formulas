{%- import 'xanadu-components/edge/settings.sls' as edge with context %}

include:
  - third-party-components.tomcat

edge_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ edge.artifacts_url }}

edge_version_file:
  file.managed:
    - name: {{ edge.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ edge.artifact_id }}
        version: {{ edge.version }}
        sha1: {{ edge.hash }}
    - require:
      - health_check: edge_nexus_available

edge_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: edge_version_file


edge-api_clean:
  cmd.wait:
    - name: rm -rf {{ edge.war_loc }}
    - watch:
      - file: edge_version_file
    - require:
      - cmd: edge_stop

edge_install_war:
  file.managed:
    - name: {{ edge.war_loc }}
    - source: {{ edge.artifacts_war_content_url }}
    - source_hash: {{ edge.hash }}
    - require:
      - cmd: edge-api_clean
    - require_in:
      - service: tomcat_service


edge_log_permissions:
  file.directory:
    - name: {{ edge.logs }}
    - user: {{ edge.user }}
    - group: {{ edge.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: edge_install_war

successful_edge_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ edge.healtcheck_port }}/{{ edge.artifact_name }}/rest/ping
    - match_text: "pong"
    - require:
      - file: edge_install_war
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ edge.name }} version is {{ edge.version }} and {{ edge.hash }}"

{%- if edge.hipchat_enabled == True %}

edge_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ edge.auth_token }}
    - room_id: {{ edge.room_id }}
    - message: '[{{ edge.environment }}] - Artifact version [{{ edge.version }}] of component [{{ edge.artifact_name }}] was successfully deployed to the [{{ edge.environment }}] environment [{{ edge.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_edge_deployment
    - onchanges:
      - service: tomcat_service

edge_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ edge.auth_token }}
    - room_id: {{ edge.room_id }}
    - message: '[{{ edge.environment }}] - Deployment of artifact version [{{ edge.version }}] for component [{{ edge.artifact_name }}] to the [{{ edge.environment }}] environment [{{ edge.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: edge_hipchat_notifier_deploy_finished

{%- endif %}


{%- if edge.slack_enabled == True %}

edge_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ edge.slack_channel }}'
    - from_name: {{ edge.from }}
    - message: '*[{{ edge.environment }}]* - Artifact version *[{{ edge.version }}]* of component *[{{ edge.artifact_name }}]* was successfully deployed to the *[{{ edge.environment }}]* environment *[{{ edge.host }}]*.'
    - api_key: {{ edge.slack_auth_token }}
    - require:
      - health_check: successful_edge_deployment
    - onchanges:
      - service: tomcat_service

edge_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ edge.slack_channel }}'
    - from_name: {{ edge.from }}
    - message: '*[{{ edge.environment }}]* - Deployment of artifact version *[{{ edge.version }}]* for component *[{{ edge.artifact_name }}]* to the *[{{ edge.environment }}]* environment *[{{ edge.host }}]* failed.'
    - api_key: {{ edge.slack_auth_token }}
    - onfail:
      - slack: edge_slack_notifier_deploy_finished

{%- endif %}

