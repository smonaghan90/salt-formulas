{%- import 'xanadu-components/documentation/settings.sls' as documentation with context %}

include:
  - third-party-components.tomcat

documentation_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ documentation.artifacts_url }}

documentation_version_file:
  file.managed:
    - name: {{ documentation.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ documentation.artifact_id }}
        version: {{ documentation.version }}
        sha1: {{ documentation.hash }}
    - require:
      - health_check: documentation_nexus_available

documentation_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: documentation_version_file


documentation-api_clean:
  cmd.wait:
    - name: rm -rf {{ documentation.war_loc }}
    - watch:
      - file: documentation_version_file
    - require:
      - cmd: documentation_stop

documentation_install_war:
  file.managed:
    - name: {{ documentation.war_loc }}
    - source: {{ documentation.artifacts_war_content_url }}
    - source_hash: {{ documentation.hash }}
    - require:
      - cmd: documentation-api_clean
    - require_in:
      - service: tomcat_service


documentation_log_permissions:
  file.directory:
    - name: {{ documentation.logs }}
    - user: {{ documentation.user }}
    - group: {{ documentation.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: documentation_install_war

successful_documentation_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ documentation.healtcheck_port }}/{{ documentation.artifact_name }}/rest/ping
    - match_text: "pong"
    - require:
      - file: documentation_install_war
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ documentation.name }} version is {{ documentation.version }} and {{ documentation.hash }}"

{%- if documentation.hipchat_enabled == True %}

documentation_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ documentation.auth_token }}
    - room_id: {{ documentation.room_id }}
    - message: '[{{ documentation.environment }}] - Artifact version [{{ documentation.version }}] of component [{{ documentation.artifact_name }}] was successfully deployed to the [{{ documentation.environment }}] environment [{{ documentation.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_documentation_deployment
    - onchanges:
      - service: tomcat_service

documentation_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ documentation.auth_token }}
    - room_id: {{ documentation.room_id }}
    - message: '[{{ documentation.environment }}] - Deployment of artifact version [{{ documentation.version }}] for component [{{ documentation.artifact_name }}] to the [{{ documentation.environment }}] environment [{{ documentation.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: documentation_hipchat_notifier_deploy_finished

{%- endif %}


{%- if documentation.slack_enabled == True %}

documentation_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ documentation.slack_channel }}'
    - from_name: {{ documentation.from }}
    - message: '*[{{ documentation.environment }}]* - Artifact version *[{{ documentation.version }}]* of component *[{{ documentation.artifact_name }}]* was successfully deployed to the *[{{ documentation.environment }}]* environment *[{{ documentation.host }}]*.'
    - api_key: {{ documentation.slack_auth_token }}
    - require:
      - health_check: successful_documentation_deployment
    - onchanges:
      - service: tomcat_service

documentation_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ documentation.slack_channel }}'
    - from_name: {{ documentation.from }}
    - message: '*[{{ documentation.environment }}]* - Deployment of artifact version *[{{ documentation.version }}]* for component *[{{ documentation.artifact_name }}]* to the *[{{ documentation.environment }}]* environment *[{{ documentation.host }}]* failed.'
    - api_key: {{ documentation.slack_auth_token }}
    - onfail:
      - slack: documentation_slack_notifier_deploy_finished

{%- endif %}

