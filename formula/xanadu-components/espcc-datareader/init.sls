{%- import 'xanadu-components/espcc-datareader/settings.sls' as espccdr with context %}
{%- from "xanadu-components/espcc-datareader/map.jinja" import espccdr_map with context %}

include:
  - third-party-components.java

espcc-datareader_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ espccdr.artifacts_url }}

espcc-datareader_version_file:
  file.managed:
    - name: {{ espccdr.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ espccdr.artifact_id }}
        version: {{ espccdr.version }}
        sha1: {{ espccdr.hash }}
    - require:
      - health_check: espcc-datareader_nexus_available

espcc-datareader_stop:
  cmd.wait:
    - name: service espcc-datareader stop
    - onlyif: ps -ef | grep espcc-datareader | grep -v grep
    - watch:
      - file: espcc-datareader_version_file
    - require:
      - file: {{ espccdr_map.service_script }}

espcc-datareader_clean:
  cmd.wait:
    - name: rm -rf {{ espccdr.home }}
    - watch:
      - file: espcc-datareader_version_file
    - require:
      - cmd: espcc-datareader_stop

espcc-datareader_props:
  file.managed:
    - name: {{ espccdr.config_dir }}/application-{{ espccdr.environment }}.properties
    - source: {{ espccdr.artifacts_props_content_url }}
    - user: {{ espccdr.user }}
    - group: {{ espccdr.group }}
    - makedirs: True
    - mode: 644
    - template: jinja

espcc-datareader_install_jar:
  file.managed:
    - name: {{ espccdr.jar_loc }}
    - source: {{ espccdr.artifacts_jar_content_url }}
    - source_hash: {{ espccdr.hash }}
    - user: {{ espccdr.user }}
    - group: {{ espccdr.group }}
    - makedirs: True
    - mode: 644
    - require:
      - cmd: espcc-datareader_stop

espcc-datareader_log_permissions:
  file.directory:
    - name: {{ espccdr.logs }}
    - user: {{ espccdr.user }}
    - group: {{ espccdr.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: espcc-datareader_install_jar

{%- if espccdr_map.service_script %}
{{ espccdr_map.service_script }}:
  file.managed:
    - source: {{ espccdr.tmpl_file_src }}/{{ espccdr_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ espccdr_map.service_script_mode }}
    - template: jinja
    - context:
       jar_loc: {{ espccdr.jar_loc }}
       config_dir: {{ espccdr.config_dir }}
       environment: {{ espccdr.environment }}
       user: {{ espccdr.user }}
       name: {{ espccdr.name }}

espccdr_service:
  service.running:
    - name: espcc-datareader
    - sig: java -Dspring
    - require:
      - file: espcc-datareader_install_jar
      - file: {{ espccdr_map.service_script }}
    - watch:
      - file: espcc-datareader_props
      - file: espcc-datareader_install_jar
{%- endif %}


successful_espcc-datareader_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ espccdr.healtcheck_port }}/manage-datareader/status
    - match_text: status":"UP"
    - require:
      - file: espcc-datareader_install_jar
      - file: espcc-datareader_props
      - service: espccdr_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ espccdr.name }} version is {{ espccdr.version }} and {{ espccdr.hash }}"

{%- if espccdr.hipchat_enabled == True %}

espcc-datareader_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ espccdr.auth_token }}
    - room_id: {{ espccdr.room_id }}
    - message: '[{{ espccdr.environment }}] - Artifact version [{{ espccdr.version }}] of component [{{ espccdr.artifact_name }}] was successfully deployed to the [{{ espccdr.environment }}] environment [{{ espccdr.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_espcc-datareader_deployment
    - onchanges:
      - service: espccdr_service

espcc-datareader_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ espccdr.auth_token }}
    - room_id: {{ espccdr.room_id }}
    - message: '[{{ espccdr.environment }}] - Deployment of artifact version [{{ espccdr.version }}] for component [{{ espccdr.artifact_name }}] to the [{{ espccdr.environment }}] environment [{{ espccdr.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: espcc-datareader_hipchat_notifier_deploy_finished

{%- endif %}
