## Check to see how many xanadu-components have been configured for deployment on the target minion

{% if salt['grains.get']('xanadu-components') %}
include:
{% for xccomponent in salt['grains.get']('xanadu-components') %}
    - xanadu-components.{{ xccomponent }}
{% endfor %}
{% elif salt['grains.get']('roles') != 'None' %}
include:
{% for thirdparty in salt['grains.get']('roles') %}
    - third-party-components.{{ thirdparty }}
{% endfor %}
{% endif %}
