if (headers['contentType'] == "SOURCE_EVENT_RECEIVED" || headers['contentType'] == "SOURCE_MARKETS_RECEIVED" || headers['contentType'] == "SOURCE_RUNNERS_RECEIVED") {
        return "topic:eventsProcessor"
}
else if (headers['contentType'] == "SOURCE_PRICES_RECEIVED"){
        return "topic:pricesProcessor"
}
else if (headers['contentType'] == "SOURCE_OFFERS_RECEIVED"){
        return "topic:offersProcessor"
}
else if (headers['contentType'] == "SOURCE_POSITIONS_RECEIVED"){
        return "topic:positionsProcessor"
}
else if (headers['contentType'] == "EVENT_INCIDENT_RECEIVED"){
        return "topic:eventIncidentProcessor"
}
else if (headers['contentType'] == "BETTING_PLATTFORM_RECEIVED"){
        return "topic:bettingPlatformsProcessor"
}
else if (headers['contentType'] == "ACCOUNT_RECEIVED"){
        return "topic:accountsProcessor"
}
