{%- import 'xanadu-components/xdc-mapper/settings.sls' as xdcmapper with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcmapper_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcmapper.artifacts_url }}

xdcmapper_version_file:
  file.managed:
    - name: {{ xdcmapper.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcmapper.artifact_id }}
        version: {{ xdcmapper.version }}
        sha1: {{ xdcmapper.hash }}
    - require:
      - health_check: xdcmapper_nexus_available

xdcmapper_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcmapper.basedir }}/{{ xdcmapper.artifact_id }}-*


xdcmapper_get_war_file:
  file.managed:
    - name: {{ xdcmapper.jar_loc }}
    - source: {{ xdcmapper.artifacts_war_content_url }}
    - source_hash: {{ xdcmapper.hash }}
    - require:
      - cmd: xdcmapper_remove_previous_versions
    - watch:
      - file: xdcmapper_version_file

xdcmapper_groovy_file:
  file.managed:
    - name: {{ xdcontainer.home }}/routerProcessor.groovy
    - source: {{ xdcmapper.tmpl_file_src}}/routerProcessor.groovy
    - user: {{ xdcmapper.user }}
    - group: {{ xdcmapper.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - watch:
      - file: xdcmapper_version_file


xdcmapper_props_file:
  file.managed:
    - name: {{ xdcontainer.config_dir }}/{{ xdcmapper.artifact_name }}.properties
    - source: {{ xdcmapper.artifacts_props_content_url }}
    - source_hash: {{ xdcmapper.artifacts_props_hash }}
    - user: {{ xdcmapper.user }}
    - group: {{ xdcmapper.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - watch:
      - file: xdcmapper_version_file


#Upload artifact to HDFS
xdcmapper_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcmapper.admin_server }}:9393/modules/processor/{{ xdcmapper.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcmapper.jar_loc }}'
    - watch:
      - file: xdcmapper_version_file
