#nexus base conf
{% set artifacts_url = pillar['component_base_url'] %}
{% set view_api = "service/local/artifact/maven/resolve" %}
{% set group = "com.xanaduconsultancy" %}
{% set environment = salt['grains.get']('environment') %}
{% set artifact_name = pillar[environment]['sbpc']['component']  %}
{% set repo_version  = salt['pillar.get'](environment + ':sbpc:repo_version', "LATEST") %}
{% set repository_id  = "snapshots" %}
{% set extension_props = "properties" %}
{% set extension_war = "war" %}
{% set classifier = "application" %}
{% set content_api = "service/local/artifact/maven/content" %}
{% set extension_md5 = "properties.md5" %}


#nexus api
{% set hash = 'sha1=' + salt['cmd.run']( 'curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath \"///sha1/text()\" -' ) %}
{% set version = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///version/text()" -') %}
{% set artifact_id = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///artifactId/text()" -') %}

{%- set artifacts_war_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_war %}

{% set artifacts_war_content_hash_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id %}


{% set artifacts_props_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_props + "&c=" + classifier %}
{% set artifacts_props_hash = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_md5 + "&c=" + classifier  %}



{% set jars_base_url = pillar['certified_platform_base_url'] + "/certified_jars/"  + pillar['certified_platform'] %}
{% set mariadb_jar_package = jars_base_url + "/mariadb-java-client-1.3.7.jar"  %}
{% set mariadb_jar_package_md5 = jars_base_url + "/mariadb-java-client-1.3.7.jar.MD5" %}

{% set mysql_jar_package = jars_base_url + "/mysql-connector-java-5.1.39-bin.jar"  %}
{% set mysql_jar_package_md5 = jars_base_url + "/mysql-connector-java-5.1.39-bin.jar.MD5" %}

#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}


#base conf
{% set name = pillar[environment]['sbpc']['component'] %}
{% set glassfish_name = pillar['glassfish_config']['name'] %}
{% set home = pillar['base_install_dir'] + "/glassfish4"  %}
{% set host = salt['grains.get']('host') %}
{% set logs = pillar['base_install_dir'] + "/logs/" + name %}

{% set basedir =  pillar['base_install_dir'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_sbpc.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar[environment]['sbpc']['component'] %}
{% set config_dir = home + "/conf" %}
{% set healtcheck_port = salt['pillar.get']('base_port', "57080") %}

{% set base_domain = home + "/glassfish/domains/domain1"  %}
{% set glassfish_bin = home + "/glassfish/bin" %}
{% set maria_db = pillar['maria_db'] %}



#conf
{% set p  = salt['pillar.get']('sbpc_config', {}) %}
{%- set user = p.get('user', 'glassfish') %}
{%- set group = p.get('group', 'glassfish') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set pc = p.get('config', {}) %}
{%- set base_port = pc.get('base_port', '0') %}
{%- set name = pc.get('name', '0') %}
{%- set base_port = pc.get('base_port', '0') %}
{%- set password = pc.get('password', '0') %}
{%- set base_domain = pc.get('base_domain', '0') %}
{%- set xmx = pc.get('xmx', '512M') %}
{%- set xms = pc.get('xms', '1044M') %}
{%- set domain_name = pc.get('domain_name', 'sbp') %}
{%- set admin_port = pc.get('admin_port', '57048') %}


{% set base_sbpc_domain = home + "/glassfish/domains/" + domain_name %}



#templates
{% set tmpl_file_src = salt['pillar.get']('environment:sbpc:config:tmpl_file_loc','salt://xanadu-components/xdc1-sbpc/file') %}
