{%- import 'xanadu-components/xdc1-sbpc/settings.sls' as xdc1sbpc with context %}
{%- import 'third-party-components/glassfish/settings.sls' as gf4 with context %}


include:
  - third-party-components.glassfish

xdc1sbpc_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1sbpc.artifacts_url }}

xdc1sbpc_version_file:
  file.managed:
    - name: {{ xdc1sbpc.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1sbpc.artifact_id }}
        version: {{ xdc1sbpc.version }}
        sha1: {{ xdc1sbpc.hash }}
    - require:
      - health_check: xdc1sbpc_nexus_available

xdc1sbpc_asadmin_file:
  file.managed:
    - name: {{ xdc1sbpc.glassfish_bin }}/AS_ADMIN
    - source: {{ xdc1sbpc.tmpl_file_src }}/AS_ADMIN
    - user: {{ xdc1sbpc.user }}
    - group: {{ xdc1sbpc.group }}
    - makedirs: True
    - mode: 644

xdc1sbpc_remove_default_domain1:
  cmd.wait:
    - names:
      - {{ gf4.bin_dir }}/asadmin stop-domain domain1
      - {{ gf4.bin_dir }}/asadmin delete-domain domain1
    - watch:
      - file: glassfish_version_file


xdc1sbpc_stop_sbpc_domain:
  cmd.wait:
    - names:
      - {{ gf4.bin_dir }}/asadmin stop-domain {{ xdc1sbpc.domain_name }}
      - pkill -9 java
    - watch:
      - file: xdc1sbpc_version_file


xdc1sbpc_remove_sbpc_domain:
  cmd.wait:
    - names:
      - {{ gf4.bin_dir }}/asadmin delete-domain {{ xdc1sbpc.domain_name }}
    - watch:
      - file: xdc1sbpc_version_file



xdc1sbpc_create_base_domain:
  cmd.wait:
    - names:
      - {{ gf4.bin_dir }}/asadmin --user {{ xdc1sbpc.domain_name }}  --passwordfile {{ xdc1sbpc.glassfish_bin }}/AS_ADMIN create-domain --savemasterpassword=true  --portbase {{ xdc1sbpc.base_port }} {{ xdc1sbpc.domain_name }}
    - require:
      - health_check: xdc1sbpc_nexus_available
    - watch:
      - file: xdc1sbpc_version_file




xdc1sbpc_copy_mariadb_drivers:
  cmd.wait:
    - name: |
        wget -P {{ xdc1sbpc.base_sbpc_domain }}/lib {{ xdc1sbpc.mariadb_jar_package }}
    - require:
      - health_check: xdc1sbpc_nexus_available
    - watch:
      - file: xdc1sbpc_version_file



xdc1sbpc_copy_mysql_drivers:
  cmd.wait:
    - name: |
        wget -P {{ xdc1sbpc.base_sbpc_domain }}/lib {{ xdc1sbpc.mysql_jar_package }}
    - require:
      - health_check: xdc1sbpc_nexus_available
    - watch:
      - file: xdc1sbpc_version_file


xdc1sbpc_start_domain:
  cmd.wait:
    - name: |
        {{ gf4.bin_dir }}/asadmin --user {{ xdc1sbpc.domain_name }}  start-domain {{ xdc1sbpc.domain_name }}
    - watch:
      - file: xdc1sbpc_version_file


xdc1sbpc_config_file:
  file.managed:
    - name: {{ xdc1sbpc.glassfish_bin }}/sbpc_config.txt
    - source: {{ xdc1sbpc.tmpl_file_src }}/sbpc_config.txt
    - user: {{ xdc1sbpc.user }}
    - group: {{ xdc1sbpc.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - context:
      maria_db: {{ xdc1sbpc.maria_db }}
      base_port: {{ xdc1sbpc.base_port }}
      admin_port: {{ xdc1sbpc.admin_port }}
      password: {{ xdc1sbpc.password }}
      base_domain : {{ xdc1sbpc.base_domain }}
      xmx: {{ xdc1sbpc.xmx }}
      xms: {{ xdc1sbpc.xms }}
      domain_name: {{ xdc1sbpc.domain_name }}
      domains: {{ gf4.domains }}
      bin_dir: {{ gf4.bin_dir }}
      glassfish_bin: {{ xdc1sbpc.glassfish_bin }}
    - require_in:
      - service: glassfish_service


xdc1sbpc_run_config_file:
  cmd.wait:
    - names:
      - {{ gf4.bin_dir }}/asadmin --port {{ xdc1sbpc.admin_port }} multimode --file {{ xdc1sbpc.glassfish_bin }}/sbpc_config.txt
    - require:
      - health_check: xdc1sbpc_nexus_available
    - watch:
      - file: xdc1sbpc_version_file



xdc1sbpc_props:
  file.managed:
    - name: {{ xdc1sbpc.base_sbpc_domain }}/config/sbpc-config.properties
    - source: {{ xdc1sbpc.artifacts_props_content_url }}
    - source_hash: {{ xdc1sbpc.artifacts_props_hash }}
    - user: {{ xdc1sbpc.user }}
    - group: {{ xdc1sbpc.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - context:
    - watch:
      - file: xdc1sbpc_version_file

xdc1dw_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdc1sbpc.base_sbpc_domain }}/{{ xdc1sbpc.artifact_id }}-*
    - watch:
      - file: xdc1sbpc_version_file


xdc1sbpc_install_war:
  file.managed:
    - name:  {{ xdc1sbpc.base_sbpc_domain }}/{{ xdc1sbpc.artifact_id }}-{{ xdc1sbpc.version }}.{{xdc1sbpc.extension_war }}
    - source: {{ xdc1sbpc.artifacts_war_content_url }}
    - source_hash: {{ xdc1sbpc.hash }}
    - user: {{ xdc1sbpc.user }}
    - group: {{ xdc1sbpc.group }}
    - makedirs: True
    - mode: 644
    - watch:
      - file: xdc1sbpc_version_file

xdc1sbpc_deploy_war:
  cmd.wait:
    - name: |
        {{ gf4.bin_dir }}/asadmin --user {{ xdc1sbpc.domain_name }}  --passwordfile {{ xdc1sbpc.glassfish_bin }}/AS_ADMIN   --port {{ xdc1sbpc.admin_port }} deploy --force=true {{ xdc1sbpc.base_sbpc_domain }}/{{ xdc1sbpc.artifact_id }}-{{ xdc1sbpc.version }}.{{xdc1sbpc.extension_war }}
    - watch:
      - file: xdc1sbpc_version_file



successful_xdc1sbpc_deployment:
  health_check.wait_for_url:
    - timeout: 10 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ xdc1sbpc.healtcheck_port }}/sbpc/rest/info
    - match_text: "application-info"
    - require:
      - file: xdc1sbpc_install_war
      - file: xdc1sbpc_props
      - service: glassfish_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ xdc1sbpc.name }} version is {{ xdc1sbpc.version }} and {{ xdc1sbpc.hash }}"


{%- if xdc1sbpc.hipchat_enabled == True %}

xdc1sbpc_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ xdc1sbpc.auth_token }}
    - room_id: {{ xdc1sbpc.room_id }}
    - message: '[{{ xdc1sbpc.environment }}] - Artifact version [{{ xdc1sbpc.version }}] of component [{{ xdc1sbpc.artifact_name }}] was successfully deployed to the [{{ xdc1sbpc.environment }}] environment [{{ xdc1sbpc.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_xdc1sbpc_deployment
    - onchanges:
      - service: glassfish_service

xdc1sbpc_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ xdc1sbpc.auth_token }}
    - room_id: {{ xdc1sbpc.room_id }}
    - message: '[{{ xdc1sbpc.environment }}] - Deployment of artifact version [{{ xdc1sbpc.version }}] for component [{{ xdc1sbpc.artifact_name }}] to the [{{ xdc1sbpc.environment }}] environment [{{ xdc1sbpc.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: xdc1sbpc_hipchat_notifier_deploy_finished

{%- endif %}
