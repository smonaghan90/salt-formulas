{%- import 'xanadu-components/xdc1-mysql-datawriter/settings.sls' as xdc1dw with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer


xdc1dw_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1dw.artifacts_url }}

xdc1dw_version_file:
  file.managed:
    - name: {{ xdc1dw.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1dw.artifact_id }}
        version: {{ xdc1dw.version }}
        sha1: {{ xdc1dw.hash }}
    - require:
      - health_check: xdc1dw_nexus_available

xdc1dw_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdc1dw.basedir }}/{{ xdc1dw.artifact_id }}-*

xdc1dw_get_war_file:
  file.managed:
    - name: {{ xdc1dw.jar_loc }}
    - source: {{ xdc1dw.artifacts_war_content_url }}
    - source_hash: {{ xdc1dw.hash }}
    - require:
      - cmd: xdc1dw_remove_previous_versions

xdc1dw-api_props:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/modules/source/{{ xdc1dw.artifact_id }}/{{ xdc1dw.artifact_id }}.properties
    - source: {{ xdc1dw.artifacts_props_content_url }}
    - source_hash: {{ xdc1dw.artifacts_props_hash }}
    - user: {{ xdc1dw.user }}
    - group: {{ xdc1dw.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - watch:
      - file: xdc1dw_version_file

xdc1dw_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdc1dw.admin_server }}:9393/modules/source/{{ xdc1dw.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdc1dw.jar_loc }}'
    - watch:
      - file: xdc1dw_version_file

{%- if xdc1dw.hipchat_enabled == True %}

xdc1dw_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ xdc1dw.auth_token }}
    - room_id: {{ xdc1dw.room_id }}
    - message: '[{{ xdc1dw.environment }}] - Uploaded version [{{ xdc1dw.version }}] of component [{{ xdc1dw.artifact_name }}] to the [{{ xdc1dw.environment }}] environment [{{ xdc1dw.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - onchanges:
      - file: xdc1dw_version_file

xdc1dw_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ xdc1dw.auth_token }}
    - room_id: {{ xdc1dw.room_id }}
    - message: '[{{ xdc1dw.environment }}] - Uploaded  artifact version [{{ xdc1dw.version }}] for component [{{ xdc1dw.artifact_name }}] to the [{{ xdc1dw.environment }}] environment [{{ xdc1dw.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: xdc1dw_hipchat_notifier_deploy_finished

{%- endif %}




#xdc1dw_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdc1dw.admin_server }}:9393/streams/definitions/{{ xdc1dw.stream_name }} '


#xdc1dw_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdc1dw.admin_server }}:9393/streams/definitions -d name={{ xdc1dw.stream_name }} -d "definition={{ xdc1dw.artifact_id }}" -d deploy=true'
#    - watch:
#      - file: xdc1dw_version_file
