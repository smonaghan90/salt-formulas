{%- import 'xanadu-components/eddie-admin-ui/settings.sls' as eddieui with context %}
{%- import 'third-party-components/nginx/settings.sls' as nx with context %}

include:
  - third-party-components.nginx

eddie-admin-ui_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ eddieui.artifacts_url }}

eddie-admin-ui_version_file:
  file.managed:
    - name: {{ eddieui.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ eddieui.artifact_id }}
        version: {{ eddieui.version }}
        sha1: {{ eddieui.hash }}
    - require:
      - health_check: eddie-admin-ui_nexus_available

eddie-admin-ui_stop:
  cmd.wait:
    - name: service nginx stop
    - onlyif: ps -ef | grep nginx | grep -v grep
    - watch:
      - file: eddie-admin-ui_version_file

eddie-admin-ui_clean:
  cmd.wait:
    - name: rm -rf {{ eddieui.home }}
    - watch:
      - file: eddie-admin-ui_version_file
    - require:
      - cmd: eddie-admin-ui_stop

eddie-admin-ui_install_tar:
  archive.extracted:
    - name: {{ eddieui.basedir }}/{{ eddieui.name }}
    - source: {{ eddieui.artifacts_tar_content_url }}
    - source_hash: {{ eddieui.hash }}
    - archive_format: tar
    - if_missing: {{ eddieui.home }}
    - require:
      - cmd: eddie-admin-ui_clean
    - require_in:
      - service: nginx_service

eddie-admin-ui_permissions:
  file.directory:
    - name: {{ eddieui.basedir }}/{{ eddieui.name }}
    - user: {{ eddieui.user }}
    - group: {{ eddieui.group }}
    - mode: 744
    - recurse:
      - user
      - group
      - mode
    - require:
      - archive: eddie-admin-ui_install_tar
    - require_in:
      - service: nginx_service

eddie_admin_ui_remove_defaults:
  cmd.wait:
    - name: rm -r {{ nx.nginx_conf }}/sites-enabled/*
    - watch:
      - cmd: eddie-admin-ui_stop

eddie_admin_ui_desktop_site_conf:
  file.managed:
    - name: {{ nx.nginx_conf }}/sites-enabled/desktop
    - source: {{ eddieui.tmpl_files}}/desktop
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
      user : {{ nx.user }}
      logs_dir : {{ nx.logs_dir }}
      host_name: {{ eddieui.host_name }}
      admin_host: {{ eddieui.admin_host }}
      admin_port: {{ eddieui.admin_port }}
      desktop_port: {{ eddieui.desktop_port }}
      desktop_assets: {{ eddieui.desktop_assets }}
      ui_host_dict: {{ eddieui.ui_host_dict }}
      ui_hosts: {{ eddieui.ui_hosts }}
    - require:
      - cmd: eddie_admin_ui_remove_defaults
    - require_in:
      - service: nginx_service

eddie_admin_ui_upstreams_conf:
  file.managed:
    - name: {{ nx.nginx_conf }}/conf.d/upstreams.conf
    - source: {{ eddieui.tmpl_files}}/upstreams.conf
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
      user : {{ nx.user }}
      logs_dir : {{ nx.logs_dir }}
      host_name: {{ eddieui.host_name }}
      admin_host: {{ eddieui.admin_host }}
      admin_port: {{ eddieui.admin_port }}
      desktop_port: {{ eddieui.desktop_port }}
      desktop_assets: {{ eddieui.desktop_assets }}
      ui_host_dict: {{ eddieui.ui_host_dict }}
      ui_hosts: {{ eddieui.ui_hosts }}
    - require:
      - cmd: eddie_admin_ui_remove_defaults
    - require_in:
      - service: nginx_service


successful_eddie-admin-ui_deployment:
  health_check.wait_for_url:
    - timeout: 10 #this is in seconds
    - url: http://{{ eddieui.host_name }}:{{ eddieui.desktop_port }}
    - match_text: LOADING
    - require:
      - archive: eddie-admin-ui_install_tar
      - file: eddie-admin-ui_permissions
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ eddieui.name }} version is {{ eddieui.version }} and hash is {{ eddieui.hash }}"

{%- if eddieui.hipchat_enabled == True %}

eddie-admin-ui_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ eddieui.auth_token }}
    - room_id: {{ eddieui.room_id }}
    - message: '[{{ eddieui.environment }}] - Artifact version [{{ eddieui.version }}] of component [{{ eddieui.artifact_name }}] was successfully deployed to the [{{ eddieui.environment }}] environment [{{ eddieui.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_eddie-admin-ui_deployment
    - onchanges:
      - service: nginx_service

eddie-admin-ui_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ eddieui.auth_token }}
    - room_id: {{ eddieui.room_id }}
    - message: '[{{ eddieui.environment }}] - Deployment of artifact version [{{ eddieui.version }}] for component [{{ eddieui.artifact_name }}] to the [{{ eddieui.environment }}] environment [{{ eddieui.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: eddie-admin-ui_hipchat_notifier_deploy_finished

{%- endif %}

{%- if eddieui.slack_enabled == True %}

eddieui_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ eddieui.slack_channel }}'
    - from_name: {{ eddieui.from }}
    - message: '*[{{ eddieui.environment }}]* - Artifact version *[{{ eddieui.version }}]* of component *[{{ eddieui.artifact_name }}]* was successfully deployed to the *[{{ eddieui.environment }}]* environment *[{{ eddieui.host }}]*.'
    - api_key: {{ eddieui.slack_auth_token }}
    - require:
      - health_check: successful_eddie-admin-ui_deployment
    - onchanges:
      - service: nginx_service

eddieui_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ eddieui.slack_channel }}'
    - from_name: {{ eddieui.from }}
    - message: '*[{{ eddieui.environment }}]* - Deployment of artifact version *[{{ eddieui.version }}]* for component *[{{ eddieui.artifact_name }}]* to the *[{{ eddieui.environment }}]* environment *[{{ eddieui.host }}]* failed.'
    - api_key: {{ eddieui.slack_auth_token }}
    - onfail:
      - slack: eddieui_slack_notifier_deploy_finished

{%- endif %}
