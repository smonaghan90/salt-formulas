{%- import 'xanadu-components/espcc-datawriter/settings.sls' as espccdw with context %}
{%- from "xanadu-components/espcc-datawriter/map.jinja" import espccdw_map with context %}

include:
  - third-party-components.springxd
  
espcc-datawriter_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ espccdw.artifacts_url }}
        
espcc-datawriter_version_file:
  file.managed:
    - name: {{ espccdw.sha1_file }}
    - makedirs: True
    - show_diff: True 
    - contents: |
        component: {{ espccdw.artifact_id }}
        version: {{ espccdw.version }}
        sha1: {{ espccdw.hash }}
    - require:
      - health_check: espcc-datawriter_nexus_available

espcc-datawriter_install_jar:
  file.managed:
    - name: {{ espccdw.jar_loc }}
    - source: {{ espccdw.artifacts_jar_content_url }}
    - source_hash: {{ espccdw.hash }}
    - user: {{ espccdw.user }}
    - group: {{ espccdw.group }} 
    - makedirs: True
    - mode: 644
    - require_in:
      - service: xd_admin_service
      - service: xd_container_service
      - service: hsqldb_service
      
espcc-datawriter_log_permissions:
  file.directory:
    - name: {{ espccdw.logs }}
    - user: {{ espccdw.user }}
    - group: {{ espccdw.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require_in:
      - service: xd_admin_service
      - service: xd_container_service
      - service: hsqldb_service
      
espcc-datawriter_xd_data:
  file.managed:
    - name: {{ espccdw.shell_dir }}/xddata.cmd
    - source: {{ espccdw.tmpl_file_src }}/xddata.jinja
    - user: {{ espccdw.user }}
    - group: {{ espccdw.group }}
    - mode: 744
    - template: jinja
    - require:
      - file: espcc-datawriter_install_jar
    
espcc-datawriter_xd_run_shell:
  cmd.wait:
    - name: {{ espccdw.shell_dir }}/xd-shell
    - args: --cmdfile xddata.cmd
    - cwd: {{ espccdw.shell_dir }}
    - stateful: True
    - require:
      - file: espcc-datawriter_install_jar
    - watch:
        - file: espcc-datawriter_version_file
    
espcc-datawriter_xd_run_shell_onlyifchanges:
  cmd.wait:
    - name: echo xd-data.cmd has changed
    - cwd: /
    - watch:
        - cmd: espcc-datawriter_xd_run_shell
         
successful_espcc-datawriter_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ espccdw.healtcheck_port }}/streams/definitions/kafkafeed
    - match_text: status":"<atom:link rel="self"
    - require:
      - file: espcc-datawriter_install_jar
      - service: xd_admin_service
      - service: xd_container_service
      - service: hsqldb_service
      
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ espccdw.name }} version is {{ espccdw.version }} and {{ espccdw.hash }}"
    - require:
      - health_check: successful_espcc-datawriter_deployment
    
    
{%- if espccdw.hipchat_enabled == True %}    

espcc-datawriter_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ espccdw.auth_token }}
    - room_id: {{ espccdw.room_id }}
    - message: '[{{ espccdw.environment }}] - Artifact version [{{ espccdw.version }}] of component [{{ espccdw.artifact_name }}] was successfully deployed to the [{{ espccdw.environment }}] environment [{{ espccdw.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_espcc-datawriter_deployment
    - onchanges:
      - file: espcc-datawriter_version_file

espcc-datawriter_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ espccdw.auth_token }}
    - room_id: {{ espccdw.room_id }}
    - message: '[{{ espccdw.environment }}] - Deployment of artifact version [{{ espccdw.version }}] for component [{{ espccdw.artifact_name }}] to the [{{ espccdw.environment }}] environment [{{ espccdw.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: espcc-datawriter_hipchat_notifier_deploy_finished

{%- endif %}
