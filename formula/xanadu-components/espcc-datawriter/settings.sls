{%- import 'third-party-components/springxd/settings.sls' as xd with context %}

#nexus base conf
{% set artifacts_url = pillar['component_base_url'] %}
{% set view_api = "service/local/artifact/maven/resolve" %}
{% set group = "com.xanaduconsultancy" %}
{% set environment = salt['grains.get']('environment') %}
{% set artifact_name = pillar[environment]['espcc-datawriter']['component']  %}
{% set repo_version  = salt['pillar.get'](environment + ':espcc-datawriter:repo_version', "LATEST") %}

{% set repository_id  = salt['pillar.get']('environment:espcc-datawriter:repository_id', "snapshots") %}
{% set extension = "properties" %}
{% set classifier = environment %}
{% set content_api = "service/local/artifact/maven/content" %}
{% set springxd_home = "/xanadu/spring-xd-1.2.1.RELEASE" %}
{% set cust_dir = salt['pillar.get']('environment:espcc-datawriter:custom_dir', springxd_home + "/xd/custom-modules/source") %}
{% set shell_dir = salt['pillar.get']('environment:espcc-datawriter:shelldir', springxd_home + "/shell/bin") %}

#nexus api
{% set hash = 'sha1=' + salt['cmd.run']( 'curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&c=" + classifier + "\"" + ' | xmllint --xpath \"///sha1/text()\" -' ) %}
{% set version = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&c=" + classifier +  "\"" + ' | xmllint --xpath "///version/text()" -') %}
{% set artifact_id = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&c=" + classifier + "\"" + ' | xmllint --xpath "///artifactId/text()" -') %}

{% set artifacts_jar_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&c=" + classifier %}
{% set artifacts_jar_content_hash_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&c=" + classifier %}


#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#base conf
{% set name = pillar[environment]['espcc-datawriter']['component'] %}
{% set home = pillar['base_install_dir'] + "/" + name %}
{% set jar_loc = springxd_home + "/" + name + ".jar" %}
{% set host = salt['grains.get']('host') %}
{% set logs = pillar['base_install_dir'] + "/logs/" + name %}

{% set basedir =  pillar['base_install_dir'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_espcc-datawriter.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar[environment]['espcc-datawriter']['component'] %}
{% set config_dir = cust_dir + "epscc-datawriter/config" %}
{% set healtcheck_port = salt['pillar.get']('healtcheck_port', "9393") %}

#conf
{% set p  = salt['pillar.get']('users:java_info', {}) %}
{%- set user = p.get('user', 'java') %}
{%- set group = p.get('group', 'java') %}
{%- set tech_group = p.get('tech_group', '_tech') %}

#templates
{% set tmpl_file_src = salt['pillar.get']('environment:espcc-datawriter:config:tmpl_file_loc','salt://xanadu-components/espcc-datawriter/file') %}
