{%- import 'xanadu-components/xdc-im-ysource/settings.sls' as xdcimysource with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcimysource_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcimysource.artifacts_url }}

xdcimysource_version_file:
  file.managed:
    - name: {{ xdcimysource.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcimysource.artifact_id }}
        version: {{ xdcimysource.version }}
        sha1: {{ xdcimysource.hash }}
    - require:
      - health_check: xdcimysource_nexus_available

xdcimysource_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcimysource.basedir }}/{{ xdcimysource.artifact_id }}-*


xdcimysource_get_war_file:
  file.managed:
    - name: {{ xdcimysource.jar_loc }}
    - source: {{ xdcimysource.artifacts_war_content_url }}
    - source_hash: {{ xdcimysource.hash }}
    - require:
      - cmd: xdcimysource_remove_previous_versions

#Upload artifact to HDFS
xdcimysource_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcimysource.admin_server }}:9393/modules/processor/{{ xdcimysource.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcimysource.jar_loc }}'
    - watch:
      - file: xdcimysource_version_file

#xdcimysource_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdcimysource.admin_server }}:9393/streams/definitions/{{ xdcimysource.stream_name }} '


#xdcimysource_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcimysource.admin_server }}:9393/streams/definitions -d name={{ xdcimysource.stream_name }} -d "definition={{ xdcimysource.artifact_id }}" -d deploy=true'
#    - watch:
#      - file: xdcimysource_version_file
