{%- import 'xanadu-components/xdc1-kafka-generator/settings.sls' as xdc1kg with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdc1kg_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1kg.artifacts_url }}

xdc1kg_version_file:
  file.managed:
    - name: {{ xdc1kg.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1kg.artifact_id }}
        version: {{ xdc1kg.version }}
        sha1: {{ xdc1kg.hash }}
    - require:
      - health_check: xdc1kg_nexus_available

xdc1kg_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdc1kg.artifact_id }}-*
    - watch:
      - file: xdc1kg_version_file


xdc1kg_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdc1kg.artifact_id }}-{{ xdc1kg.version }}.jar
    - source: {{ xdc1kg.artifacts_war_content_url }}
    - source_hash: {{ xdc1kg.hash }}
    - user: {{ xdc1kg.user }}
    - group: {{ xdc1kg.group }}
    - mode: 644
    - watch:
      - file: xdc1kg_version_file
    - require:
      - cmd: xdc1kg_remove_previous_versions

xdc1kg_restart_container:
  cmd.wait:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdc1kg_version_file
