{%- import 'xanadu-components/eventmgmt/settings.sls' as eventmgmt with context %}

include:
  - third-party-components.tomcat

eventmgmt_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ eventmgmt.artifacts_url }}

eventmgmt_version_file:
  file.managed:
    - name: {{ eventmgmt.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ eventmgmt.artifact_id }}
        version: {{ eventmgmt.version }}
        sha1: {{ eventmgmt.hash }}
    - require:
      - health_check: eventmgmt_nexus_available

eventmgmt_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: eventmgmt_version_file


eventmgmt-api_clean:
  cmd.wait:
    - name: rm -rf {{ eventmgmt.war_loc }}
    - watch:
      - file: eventmgmt_version_file
    - require:
      - cmd: eventmgmt_stop

eventmgmt_install_war:
  file.managed:
    - name: {{ eventmgmt.war_loc }}
    - source: {{ eventmgmt.artifacts_war_content_url }}
    - source_hash: {{ eventmgmt.hash }}
    - require:
      - cmd: eventmgmt-api_clean
    - require_in:
      - service: tomcat_service


eventmgmt_log_permissions:
  file.directory:
    - name: {{ eventmgmt.logs }}
    - user: {{ eventmgmt.user }}
    - group: {{ eventmgmt.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: eventmgmt_install_war

successful_eventmgmt_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in secondsf
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ eventmgmt.healtcheck_port }}/{{ eventmgmt.artifact_name }}/rest/ping
    - match_text: "pong"
    - require:
      - file: eventmgmt_install_war
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ eventmgmt.name }} version is {{ eventmgmt.version }} and {{ eventmgmt.hash }}"

{%- if eventmgmt.hipchat_enabled == True %}

eventmgmt_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ eventmgmt.auth_token }}
    - room_id: {{ eventmgmt.room_id }}
    - message: '[{{ eventmgmt.environment }}] - Artifact version [{{ eventmgmt.version }}] of component [{{ eventmgmt.artifact_name }}] was successfully deployed to the [{{ eventmgmt.environment }}] environment [{{ eventmgmt.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_eventmgmt_deployment
    - onchanges:
      - service: tomcat_service

eventmgmt_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ eventmgmt.auth_token }}
    - room_id: {{ eventmgmt.room_id }}
    - message: '[{{ eventmgmt.environment }}] - Deployment of artifact version [{{ eventmgmt.version }}] for component [{{ eventmgmt.artifact_name }}] to the [{{ eventmgmt.environment }}] environment [{{ eventmgmt.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: eventmgmt_hipchat_notifier_deploy_finished

{%- endif %}

{%- if eventmgmt.slack_enabled == True %}

eventmgmt_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ eventmgmt.slack_channel }}'
    - from_name: {{ eventmgmt.from }}
    - message: '*[{{ eventmgmt.environment }}]* - Artifact version *[{{ eventmgmt.version }}]* of component *[{{ eventmgmt.artifact_name }}]* was successfully deployed to the *[{{ eventmgmt.environment }}]* environment *[{{ eventmgmt.host }}]*.'
    - api_key: {{ eventmgmt.slack_auth_token }}
    - require:
      - health_check: successful_eventmgmt_deployment
    - onchanges:
      - service: tomcat_service

eventmgmt_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ eventmgmt.slack_channel }}'
    - from_name: {{ eventmgmt.from }}
    - message: '*[{{ eventmgmt.environment }}]* - Deployment of artifact version *[{{ eventmgmt.version }}]* for component *[{{ eventmgmt.artifact_name }}]* to the *[{{ eventmgmt.environment }}]* environment *[{{ eventmgmt.host }}]* failed.'
    - api_key: {{ eventmgmt.slack_auth_token }}
    - onfail:
      - slack: eventmgmt_slack_notifier_deploy_finished

{%- endif %}

