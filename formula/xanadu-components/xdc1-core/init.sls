{%- import 'xanadu-components/xdc1-core/settings.sls' as xdc1core with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdc1core_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1core.artifacts_url }}

xdc1core_version_file:
  file.managed:
    - name: {{ xdc1core.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1core.artifact_id }}
        version: {{ xdc1core.version }}
        sha1: {{ xdc1core.hash }}
    - require:
      - health_check: xdc1core_nexus_available

xdc1core_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdc1core.artifact_id }}-*
    - watch:
      - file: xdc1core_version_file


xdc1core_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdc1core.artifact_id }}-{{ xdc1core.version }}.jar
    - source: {{ xdc1core.artifacts_war_content_url }}
    - source_hash: {{ xdc1core.hash }}
    - user: {{ xdc1core.user }}
    - group: {{ xdc1core.group }}
    - mode: 644
    - watch:
      - file: xdc1core_version_file
    - require:
      - cmd: xdc1core_remove_previous_versions

xdc1core_restart_container:
  cmd.wait:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdc1core_version_file
