{%- import 'xanadu-components/espcc-ui/settings.sls' as espccui with context %}
{%- import 'third-party-components/nginx/settings.sls' as nx with context %}

include:
  - third-party-components.nginx

espcc-ui_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ espccui.artifacts_url }}

espcc-ui_version_file:
  file.managed:
    - name: {{ espccui.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ espccui.artifact_id }}
        version: {{ espccui.version }}
        sha1: {{ espccui.hash }}
    - require:
      - health_check: espcc-ui_nexus_available

espcc-ui_stop:
  cmd.wait:
    - name: service nginx stop
    - onlyif: ps -ef | grep nginx | grep -v grep
    - watch:
      - file: espcc-ui_version_file

espcc-ui_clean:
  cmd.wait:
    - name: rm -rf {{ espccui.home }}
    - watch:
      - file: espcc-ui_version_file
    - require:
      - cmd: espcc-ui_stop

espcc-ui_install_tar:
  archive.extracted:
    - name: {{ espccui.basedir }}/{{ espccui.name }}
    - source: {{ espccui.artifacts_tar_content_url }}
    - source_hash: {{ espccui.hash }}
    - archive_format: tar
    - if_missing: {{ espccui.home }}
    - require:
      - cmd: espcc-ui_clean
    - require_in:
      - service: nginx_service

espcc-ui_permissions:
  file.directory:
    - name: {{ espccui.basedir }}/{{ espccui.name }}
    - user: {{ espccui.user }}
    - group: {{ espccui.group }}
    - mode: 744
    - recurse:
      - user
      - group
      - mode
    - require_in:
      - service: nginx_service

successful_espcc-ui_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ nx.server_port }}/assets/info.json
    - match_text: build-quality
    - require:
      - archive: espcc-ui_install_tar
      - file: espcc-ui_permissions
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ espccui.name }} version is {{ espccui.version }} and hash is {{ espccui.hash }}"

{%- if espccui.hipchat_enabled == True %}

espcc-ui_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ espccui.auth_token }}
    - room_id: {{ espccui.room_id }}
    - message: '[{{ espccui.environment }}] - Artifact version [{{ espccui.version }}] of component [{{ espccui.artifact_name }}] was successfully deployed to the [{{ espccui.environment }}] environment [{{ espccui.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_espcc-ui_deployment
    - onchanges:
      - service: nginx_service

espcc-ui_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ espccui.auth_token }}
    - room_id: {{ espccui.room_id }}
    - message: '[{{ espccui.environment }}] - Deployment of artifact version [{{ espccui.version }}] for component [{{ espccui.artifact_name }}] to the [{{ espccui.environment }}] environment [{{ espccui.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: espcc-ui_hipchat_notifier_deploy_finished

{%- endif %}
