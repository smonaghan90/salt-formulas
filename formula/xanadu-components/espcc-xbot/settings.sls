#nexus base conf
{% set artifacts_url = pillar['component_base_url'] %}
{% set view_api = "service/local/artifact/maven/resolve" %}
{% set group = "com.xanaduconsultancy" %}
{% set environment = salt['grains.get']('environment') %}
{% set artifact_name = pillar[environment]['espcc-xbot']['component']  %}
{% set repo_version  = salt['pillar.get'](environment + 'espcc-xbot:repo_version', "LATEST") %}
{% set repository_id  = salt['pillar.get']('repository_id', "snapshots") %}
{% set extension = "properties" %}
{% set classifier = "application" %}
{% set extension_props = "properties" %}
{% set extension_war = "war" %}
{% set content_api = "service/local/artifact/maven/content" %}



#nexus api
{% set hash = 'sha1=' + salt['cmd.run']( 'curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath \"///sha1/text()\" -' ) %}

{% set version = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///version/text()" -') %}
{% set artifact_id = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///artifactId/text()" -') %}

{% set artifacts_war_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_war %}
{% set artifacts_war_content_hash_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id %}


#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#base conf
{% set name = pillar[environment]['espcc-xbot']['component'] %}
{% set glassfish_home = pillar['glassfish_config']['name'] %}
{% set glassfish_dir = pillar['glassfish_config']['dir'] %}
{% set home = pillar['base_install_dir'] + "/" + glassfish_dir  %}
{% set domain_dir = home + '/glassfish/domains/domain1'  %}
{% set domain_config = domain_dir + '/config'  %}
{% set war_loc = domain_dir + '/autodeploy/' + name + '.war' %}

{% set fqdn =  salt['grains.get']('fqdn')  %}



{% set host = salt['grains.get']('host') %}
{% set logs = pillar['base_install_dir'] + "/logs/" + name %}
{% set base_port = pillar['xbot_config']['domain']['base_port']%}
#Domain information port etc

{% set base_port = pillar['xbot_config']['domain']['base_port']%}
{% set check_port = pillar['xbot_config']['domain']['check_port']%}
{% set max_perm_size = pillar['xbot_config']['domain']['max_perm_size']%}
{% set perm_size = pillar['xbot_config']['domain']['perm_size']%}
{% set xmx = pillar['xbot_config']['domain']['xmx']%}


{% set basedir =  pillar['base_install_dir'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_espcc-xbot.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar[environment]['espcc-xbot']['component'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_espcc-xbot.txt' %}

{% set sbpc_url = pillar['xbot_config']['sbpc']['sbpc_url']  %}



#conf
{% set p  = salt['pillar.get']('glassfish_config', {}) %}
{%- set user = p.get('user', 'glassfish') %}
{%- set group = p.get('group', 'glassfish') %}
{%- set tech_group = p.get('tech_group', '_tech') %}

#templates
{% set tmpl_file_src = salt['pillar.get']('environment:espcc-xbot:config:tmpl_file_loc','salt://xanadu-components/espcc-xbot/file') %}
