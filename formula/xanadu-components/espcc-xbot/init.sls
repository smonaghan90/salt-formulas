{%- import 'xanadu-components/espcc-xbot/settings.sls' as xb with context %}
{%- import 'third-party-components/glassfish/settings.sls' as gf with context %}

include:
  - third-party-components.glassfish

espcc-xbot_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xb.artifacts_url }}

espcc-xbot_version_file:
  file.managed:
    - name: {{ xb.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xb.artifact_id }}
        version: {{ xb.version }}
        sha1: {{ xb.hash }}
    - require:
      - health_check: espcc-xbot_nexus_available

espcc-xbot_stop:
  cmd.wait:
    - name: service glassfish stop
    - onlyif: ps -ef | grep glassfish | grep -v grep
    - watch:
      - file: espcc-xbot_version_file

espcc-xbot_install_war:
  file.managed:
    - name: {{ xb.war_loc }}
    - source: {{ xb.artifacts_war_content_url }}
    - source_hash: {{ xb.hash }}
    - require:
      - file: espcc-xbot_version_file
    - require_in:
      - service: glassfish_service

espcc-xbot_glassfish_file:
  file.managed:
    - name: /etc/init.d/glassfish
    - source: salt://xanadu-components/espcc-xbot/files/glassfish.init.d
    - mode: 775
    - template: jinja
    - context:
      glassfish_home: {{ gf.home }}
      glassfish_user: {{ gf.user }}
      base_port:  {{ xb.base_port }}
    - require:
      - file: espcc-xbot_version_file
    - require_in:
      - service: glassfish_service


espcc-xbot_domain:
  file.managed:
    - name:  {{ xb.domain_config }}/domain.xml
    - source: salt://xanadu-components/espcc-xbot/files/domain.xml
    - user: {{ gf.user }}
    - group: {{ gf.group }}
    - mode: 644
    - template: jinja
    - context:
      logs_dir: {{ xb.logs_dir }}
      domain_dir: {{ xb.domain_dir }}
      base_port:  {{ xb.base_port }}
      host_name:  {{ xb.host }}
      max_perm_size: {{ xb.max_perm_size }}
      perm_size: {{ xb.perm_size }}
      xmx: {{ xb.xmx }}
    - require:
      - file: espcc-xbot_version_file
    - require_in:
      - service: glassfish_service

espcc-xbot_sql:
  file.recurse:
    - name:  {{ xb.domain_config }}
    - source: salt://xanadu-components/espcc-xbot/files/sql
    - user: {{ gf.user }}
    - group: {{ gf.group }}
    - template: jinja
    - context:
      host_name: {{ xb.host }}
    - require:
      - file: espcc-xbot_version_file
    - require_in:
      - service: glassfish_service


espcc-xbot_properties:
  file.managed:
    - name:  {{ xb.domain_config }}/xbot.properties
    - source: salt://xanadu-components/espcc-xbot/files/xbot.properties
    - user: {{ gf.user }}
    - group: {{ gf.group }}
    - template: jinja
    - context:
      host_name: {{ xb.host }}
      sbpc_url: {{ xb.sbpc_url }}
    - require:
      - file: espcc-xbot_version_file
    - require_in:
      - service: glassfish_service

espcc-xbot_logs:
  file.managed:
    - name:  {{ xb.domain_config }}/logback.xml
    - source: salt://xanadu-components/espcc-xbot/files/logback.xml
    - user: {{ gf.user }}
    - group: {{ gf.group }}
    - template: jinja
    - context:
      logs_dir: {{ xb.logs_dir }}
    - require:
      - file: espcc-xbot_version_file
    - require_in:
      - service: glassfish_service


espcc-xbot_log_permissions:
  file.directory:
    - name: {{ xb.logs }}
    - user: {{ gf.user }}
    - group: {{ gf.group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: espcc-xbot_install_war
    - require_in:
      - service: glassfish_service

successful_espcc-xbot_deployment:
  health_check.wait_for_url:
    - timeout: 400 #this is in seconds
    - url: 'http://{{xb.fqdn}}:{{ xb.check_port }}/xbot'
    - match_text: "X-BOT Administrator Login"
    - require:
      - file: espcc-xbot_install_war
      - file: espcc-xbot_glassfish_file
      - file: espcc-xbot_domain


{%- if xb.hipchat_enabled == True %}
espcc-xbot_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ xb.auth_token }}
    - room_id: {{ xb.room_id }}
    - message: '[{{ xb.environment }}] - Artifact version [{{ xb.version }}] of component [{{ xb.artifact_name }}] was successfully deployed to the [{{ xb.environment }}] environment [{{ xb.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_espcc-xbot_deployment
    - onchanges:
      - service:  glassfish_service

espcc-xbot_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ xb.auth_token }}
    - room_id: {{ xb.room_id }}
    - message: '[{{ xb.environment }}] - Deployment of artifact version [{{ xb.version }}] for component [{{ xb.artifact_name }}] to the [{{ xb.environment }}] environment [{{ xb.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: espcc-xbot_hipchat_notifier_deploy_finished
{%- endif %}
