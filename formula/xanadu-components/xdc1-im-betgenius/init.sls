{%- import 'xanadu-components/xdc1-im-betgenius/settings.sls' as xdc1imbg with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer


xdc1imbg_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1imbg.artifacts_url }}

xdc1imbg_version_file:
  file.managed:
    - name: {{ xdc1imbg.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1imbg.artifact_id }}
        version: {{ xdc1imbg.version }}
        sha1: {{ xdc1imbg.hash }}
    - require:
      - health_check: xdc1imbg_nexus_available

xdc1imbg_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdc1imbg.basedir }}/{{ xdc1imbg.artifact_id }}-*

xdc1imbg_get_war_file:
  file.managed:
    - name: {{ xdc1imbg.jar_loc }}
    - source: {{ xdc1imbg.artifacts_war_content_url }}
    - source_hash: {{ xdc1imbg.hash }}
    - require:
      - cmd: xdc1imbg_remove_previous_versions

xdc1imbg-api_props:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/modules/source/{{ xdc1imbg.artifact_id }}/{{ xdc1imbg.artifact_id }}.properties
    - source: {{ xdc1imbg.artifacts_props_content_url }}
    - source_hash: {{ xdc1imbg.artifacts_props_hash }}
    - user: {{ xdc1imbg.user }}
    - group: {{ xdc1imbg.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - watch:
      - file: xdc1imbg_version_file

xdc1imbg_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdc1imbg.admin_server }}:9393/modules/source/{{ xdc1imbg.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdc1imbg.jar_loc }}'
    - watch:
      - file: xdc1imbg_version_file

#xdc1imbg_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdc1imbg.admin_server }}:9393/streams/definitions/{{ xdc1imbg.stream_name }} '


#xdc1imbg_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdc1imbg.admin_server }}:9393/streams/definitions -d name={{ xdc1imbg.stream_name }} -d "definition={{ xdc1imbg.artifact_id }}" -d deploy=true'
#    - watch:
#      - file: xdc1imbg_version_file
