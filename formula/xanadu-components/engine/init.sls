{%- import 'xanadu-components/engine/settings.sls' as engine with context %}

include:
  - third-party-components.tomcat

engine_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ engine.artifacts_url }}

engine_version_file:
  file.managed:
    - name: {{ engine.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ engine.artifact_id }}
        version: {{ engine.version }}
        sha1: {{ engine.hash }}
    - require:
      - health_check: engine_nexus_available

engine_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: engine_version_file


engine-api_clean:
  cmd.wait:
    - name: rm -rf {{ engine.war_loc }}
    - watch:
      - file: engine_version_file
    - require:
      - cmd: engine_stop

engine_install_war:
  file.managed:
    - name: {{ engine.war_loc }}
    - source: {{ engine.artifacts_war_content_url }}
    - source_hash: {{ engine.hash }}
    - require:
      - cmd: engine-api_clean
    - require_in:
      - service: tomcat_service


engine_log_permissions:
  file.directory:
    - name: {{ engine.logs }}
    - user: {{ engine.user }}
    - group: {{ engine.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: engine_install_war

successful_engine_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in secondsf
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ engine.healtcheck_port }}/{{ engine.artifact_name }}/rest/ping
    - match_text: "pong"
    - require:
      - file: engine_install_war
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ engine.name }} version is {{ engine.version }} and {{ engine.hash }}"

{%- if engine.hipchat_enabled == True %}

engine_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ engine.auth_token }}
    - room_id: {{ engine.room_id }}
    - message: '[{{ engine.environment }}] - Artifact version [{{ engine.version }}] of component [{{ engine.artifact_name }}] was successfully deployed to the [{{ engine.environment }}] environment [{{ engine.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_engine_deployment
    - onchanges:
      - service: tomcat_service

engine_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ engine.auth_token }}
    - room_id: {{ engine.room_id }}
    - message: '[{{ engine.environment }}] - Deployment of artifact version [{{ engine.version }}] for component [{{ engine.artifact_name }}] to the [{{ engine.environment }}] environment [{{ engine.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: engine_hipchat_notifier_deploy_finished

{%- endif %}

{%- if engine.slack_enabled == True %}

engine_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ engine.slack_channel }}'
    - from_name: {{ engine.from }}
    - message: '*[{{ engine.environment }}]* - Artifact version *[{{ engine.version }}]* of component *[{{ engine.artifact_name }}]* was successfully deployed to the *[{{ engine.environment }}]* environment *[{{ engine.host }}]*.'
    - api_key: {{ engine.slack_auth_token }}
    - require:
      - health_check: successful_engine_deployment
    - onchanges:
      - service: tomcat_service

engine_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ engine.slack_channel }}'
    - from_name: {{ engine.from }}
    - message: '*[{{ engine.environment }}]* - Deployment of artifact version *[{{ engine.version }}]* for component *[{{ engine.artifact_name }}]* to the *[{{ engine.environment }}]* environment *[{{ engine.host }}]* failed.'
    - api_key: {{ engine.slack_auth_token }}
    - onfail:
      - slack: engine_slack_notifier_deploy_finished

{%- endif %}
