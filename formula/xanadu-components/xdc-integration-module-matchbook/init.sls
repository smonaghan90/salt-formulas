{%- import 'xanadu-components/xdc-integration-module-matchbook/settings.sls' as xdcimmb with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcimmb_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcimmb.artifacts_url }}

xdcimmb_version_file:
  file.managed:
    - name: {{ xdcimmb.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcimmb.artifact_id }}
        version: {{ xdcimmb.version }}
        sha1: {{ xdcimmb.hash }}
    - require:
      - health_check: xdcimmb_nexus_available

xdcimmb_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcimmb.basedir }}/{{ xdcimmb.artifact_id }}-*

xdcimmb_get_war_file:
  file.managed:
    - name: {{ xdcimmb.jar_loc }}
    - source: {{ xdcimmb.artifacts_war_content_url }}
    - source_hash: {{ xdcimmb.hash }}
    - require:
      - cmd: xdcimmb_remove_previous_versions

#Upload artifact to HDFS
xdcimmb_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcimmb.admin_server }}:9393/modules/processor/{{ xdcimmb.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcimmb.jar_loc }}'
    - watch:
      - file: xdcimmb_version_file

#xdcimmb_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdcimmb.admin_server }}:9393/streams/definitions/{{ xdcimmb.stream_name }} '


#xdcimmb_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcimmb.admin_server }}:9393/streams/definitions -d name={{ xdcimmb.stream_name }} -d "definition={{ xdcimmb.artifact_id }}" -d deploy=true'
#    - watch:
#      - file: xdcimmb_version_file
