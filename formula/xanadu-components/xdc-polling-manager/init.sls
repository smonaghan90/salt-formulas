{%- import 'xanadu-components/xdc-polling-manager/settings.sls' as xdcpm with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcpm_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcpm.artifacts_url }}

xdcpm_version_file:
  file.managed:
    - name: {{ xdcpm.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcpm.artifact_id }}
        version: {{ xdcpm.version }}
        sha1: {{ xdcpm.hash }}
    - require:
      - health_check: xdcpm_nexus_available

xdcpm_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcpm.basedir }}/{{ xdcpm.artifact_id }}-*


xdcpm_get_war_file:
  file.managed:
    - name: {{ xdcpm.jar_loc }}
    - source: {{ xdcpm.artifacts_war_content_url }}
    - source_hash: {{ xdcpm.hash }}
    - require:
      - cmd: xdcpm_remove_previous_versions

xdc-polling-manager-api_props:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/{{ xdcpm.artifact_id }}.properties
    - source: {{ xdcpm.artifacts_props_content_url }}
    - source_hash: {{ xdcpm.artifacts_props_hash }}
    - user: {{ xdcpm.user }}
    - group: {{ xdcpm.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - watch:
      - file: xdcpm_version_file

xdc-polling-manager-quartz-file:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/quartz.properties
    - source: {{ xdcpm.tmpl_file_src}}/quartz.properties
    - user: {{ xdcpm.user }}
    - group: {{ xdcpm.group }}
    - makedirs: True
    - mode: 644
    - template: jinja



xdc-polling-manager-rules-xsd:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/{{ xdcpm.artifact_id }}-rules/rules.xsd
    - source: {{ xdcpm.tmpl_file_src}}/rules.xsd
    - user: {{ xdcpm.user }}
    - group: {{ xdcpm.group }}
    - makedirs: True
    - mode: 644
    - template: jinja


#Upload artifact + properties to HDFS
xdcpm_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcpm.admin_server }}:9393/modules/source/{{ xdcpm.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcpm.jar_loc }}'
    - watch:
      - file: xdcpm_version_file

#xdcpm_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdcpm.admin_server }}:9393/streams/definitions/{{ xdcpm.stream_name }} '


#xdcpm_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcpm.admin_server }}:9393/streams/definitions -d name={{ xdcpm.stream_name }} -d "definition= http | xdc-json-to-object | {{ xdcpm.artifact_id }} | log" -d deploy=true'
#    - watch:
#      - file: xdcpm_version_file
