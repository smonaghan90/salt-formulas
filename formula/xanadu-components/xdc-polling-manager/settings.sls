#Get Jars from nexus based on version set in pillar
{% set artifacts_url = pillar['component_base_url'] %}
{% set view_api = "service/local/artifact/maven/resolve" %}
{% set group = "com.xanaduconsultancy" %}
{% set environment = salt['grains.get']('environment') %}
{% set artifact_name = pillar[environment]['xdc-polling-manager']['component']  %}
{% set repo_version  = salt['pillar.get'](environment + ':xdc-polling-manager:repo_version', "LATEST") %}
{% set repository_id  = salt['pillar.get']('repository_id', "snapshots") %}
{% set extension_props = "properties" %}
{% set extension_war = "jar" %}
{% set classifier = "application" %}
{% set content_api = "service/local/artifact/maven/content" %}
{% set extension_md5 = "properties.md5" %}


#nexus api
{% set hash = 'sha1=' + salt['cmd.run']( 'curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath \"///sha1/text()\" -' ) %}
{% set version = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///version/text()" -') %}
{% set artifact_id = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///artifactId/text()" -') %}

{% set artifacts_war_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_war %}
{% set artifacts_war_content_hash_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id %}
{% set artifacts_props_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_props + "&c=" + classifier %}
{% set artifacts_props_hash = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_md5 + "&c=" + classifier  %}


{% set stream_name = 'xdc-polling-manager' %}

{% set basedir =  pillar['base_install_dir'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_xdc-polling-manager.txt' %}
{% set jar_loc = basedir + "/" + artifact_id + '-'+ version +".jar" %}
{% set hdfs_master = pillar['hdfs_master'] %}

{% set admin_server = pillar['admin_server'] %}

#Get config
{% set p  = salt['pillar.get']('xdc-polling-manager', {}) %}
{%- set user = p.get('user', 'java') %}
{%- set group = p.get('group', 'java') %}
{%- set tech_group = p.get('tech_group', '_tech') %}


{% set rules_path = p.get('properties:rules_path', {}) %}
{% set rules_schema_path = p.get('properties:rules_schema_path', '') %}
{% set quartz_property_file = p.get('properties:quartz_property_file', '') %}
{% set auto_load_rules = p.get('properties:auto_load_rules', '') %}
{% set rules_file_exstension = p.get('properties:rules_file_exstension', '') %}
{% set default_account_username = p.get('properties:default_account_username', '') %}
{% set account_check_interval = p.get('properties:account_check_interval', '') %}
{% set connectors_to_login_to = p.get('properties:connectors_to_login_to', '') %}




#templates
{% set tmpl_file_src = salt['pillar.get']('environment:xdc-polling-manager:config:tmpl_file_loc','salt://xanadu-components/xdc-polling-manager/file') %}
