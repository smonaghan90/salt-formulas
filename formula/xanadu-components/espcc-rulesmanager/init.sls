{%- import 'xanadu-components/espcc-rulesmanager/settings.sls' as espccrm with context %}
{%- from "xanadu-components/espcc-rulesmanager/map.jinja" import espccrm_map with context %}

include:
  - third-party-components.java

espcc-rulesmanager_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ espccrm.artifacts_url }}

espcc-rulesmanager_version_file:
  file.managed:
    - name: {{ espccrm.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ espccrm.artifact_id }}
        version: {{ espccrm.version }}
        sha1: {{ espccrm.hash }}
    - require:
      - health_check: espcc-rulesmanager_nexus_available

espcc-rulesmanager_stop:
  cmd.wait:
    - name: service espcc-rulesmanager stop
    - onlyif: ps -ef | grep espcc-rulesmanager | grep -v grep
    - watch:
      - file: espcc-rulesmanager_version_file
    - require:
      - file: {{ espccrm_map.service_script }}

espcc-rulesmanager_clean:
  cmd.wait:
    - name: rm -rf {{ espccrm.home }}
    - watch:
      - file: espcc-rulesmanager_version_file
    - require:
      - cmd: espcc-rulesmanager_stop

espcc-rulesmanager_props:
  file.managed:
    - name: {{ espccrm.config_dir }}/application-{{ espccrm.environment }}.properties
    - source: {{ espccrm.artifacts_props_content_url }}
    - user: {{ espccrm.user }}
    - group: {{ espccrm.group }}
    - makedirs: True
    - mode: 644
    - template: jinja

espcc-rulesmanager_install_jar:
  file.managed:
    - name: {{ espccrm.jar_loc }}
    - source: {{ espccrm.artifacts_jar_content_url }}
    - source_hash: {{ espccrm.hash }}
    - user: {{ espccrm.user }}
    - group: {{ espccrm.group }}
    - makedirs: True
    - mode: 644
    - require:
      - cmd: espcc-rulesmanager_stop

espcc-rulesmanager_log_permissions:
  file.directory:
    - name: {{ espccrm.logs }}
    - user: {{ espccrm.user }}
    - group: {{ espccrm.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: espcc-rulesmanager_install_jar


{%- if espccrm_map.service_script %}
{{ espccrm_map.service_script }}:
  file.managed:
    - source: {{ espccrm.tmpl_file_src }}/{{ espccrm_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ espccrm_map.service_script_mode }}
    - template: jinja
    - context:
       jar_loc: {{ espccrm.jar_loc }}
       config_dir: {{ espccrm.config_dir }}
       environment: {{ espccrm.environment }}
       user: {{ espccrm.user }}
       name: {{ espccrm.name }}

espccrm_service:
  service.running:
    - name: espcc-rulesmanager
    - sig: java -Dspring
    - require:
      - file: espcc-rulesmanager_install_jar
      - file: {{ espccrm_map.service_script }}
    - watch:
      - file: espcc-rulesmanager_props
      - file: espcc-rulesmanager_install_jar
{%- endif %}

successful_espcc-rulesmanager_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ espccrm.healtcheck_port }}/manage-rulesmanager/status
    - match_text: status":"UP"
    - require:
      - file: espcc-rulesmanager_install_jar
      - file: espcc-rulesmanager_props
      - service: espccrm_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ espccrm.artifact_name }} version is {{ espccrm.version }} and hash is {{ espccrm.hash }}"

{%- if espccrm.hipchat_enabled == True %}

espcc-rulesmanager_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ espccrm.auth_token }}
    - room_id: {{ espccrm.room_id }}
    - message: '[{{ espccrm.environment }}] - Artifact version [{{ espccrm.version }}] of component [{{ espccrm.artifact_name }}] was successfully deployed to the [{{ espccrm.environment }}] environment [{{ espccrm.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_espcc-rulesmanager_deployment
    - onchanges:
      - service: espccrm_service

espcc-rulesmanager_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ espccrm.auth_token }}
    - room_id: {{ espccrm.room_id }}
    - message: '[{{ espccrm.environment }}] - Deployment of artifact version [{{ espccrm.version }}] for component [{{ espccrm.artifact_name }}] to the [{{ espccrm.environment }}] environment [{{ espccrm.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: espcc-rulesmanager_hipchat_notifier_deploy_finished

{%- endif %}
