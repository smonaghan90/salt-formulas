{%- import 'xanadu-components/monitor/settings.sls' as monitor with context %}

include:
  - third-party-components.tomcat

monitor_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ monitor.artifacts_url }}

monitor_version_file:
  file.managed:
    - name: {{ monitor.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ monitor.artifact_id }}
        version: {{ monitor.version }}
        sha1: {{ monitor.hash }}
    - require:
      - health_check: monitor_nexus_available

monitor_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: monitor_version_file


monitor-api_clean:
  cmd.wait:
    - name: rm -rf {{ monitor.war_loc }}
    - watch:
      - file: monitor_version_file
    - require:
      - cmd: monitor_stop

monitor_install_war:
  file.managed:
    - name: {{ monitor.war_loc }}
    - source: {{ monitor.artifacts_war_content_url }}
    - source_hash: {{ monitor.hash }}
    - require:
      - cmd: monitor-api_clean
    - require_in:
      - service: tomcat_service


monitor_log_permissions:
  file.directory:
    - name: {{ monitor.logs }}
    - user: {{ monitor.user }}
    - group: {{ monitor.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: monitor_install_war

successful_monitor_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in secondsf
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ monitor.healtcheck_port }}/{{ monitor.artifact_name }}/rest/ping
    - match_text: "pong"
    - require:
      - file: monitor_install_war
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ monitor.name }} version is {{ monitor.version }} and {{ monitor.hash }}"

{%- if monitor.hipchat_enabled == True %}

monitor_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ monitor.auth_token }}
    - room_id: {{ monitor.room_id }}
    - message: '[{{ monitor.environment }}] - Artifact version [{{ monitor.version }}] of component [{{ monitor.artifact_name }}] was successfully deployed to the [{{ monitor.environment }}] environment [{{ monitor.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_monitor_deployment
    - onchanges:
      - service: tomcat_service

monitor_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ monitor.auth_token }}
    - room_id: {{ monitor.room_id }}
    - message: '[{{ monitor.environment }}] - Deployment of artifact version [{{ monitor.version }}] for component [{{ monitor.artifact_name }}] to the [{{ monitor.environment }}] environment [{{ monitor.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: monitor_hipchat_notifier_deploy_finished

{%- endif %}
