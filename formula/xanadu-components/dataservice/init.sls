{%- import 'xanadu-components/dataservice/settings.sls' as dataservice with context %}

include:
  - third-party-components.tomcat

dataservice_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ dataservice.artifacts_url }}

dataservice_version_file:
  file.managed:
    - name: {{ dataservice.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ dataservice.artifact_id }}
        version: {{ dataservice.version }}
        sha1: {{ dataservice.hash }}
    - require:
      - health_check: dataservice_nexus_available

dataservice_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: dataservice_version_file


dataservice-api_clean:
  cmd.wait:
    - name: rm -rf {{ dataservice.war_loc }}
    - watch:
      - file: dataservice_version_file
    - require:
      - cmd: dataservice_stop

dataservice_install_war:
  file.managed:
    - name: {{ dataservice.war_loc }}
    - source: {{ dataservice.artifacts_war_content_url }}
    - source_hash: {{ dataservice.hash }}
    - require:
      - cmd: dataservice-api_clean
    - require_in:
      - service: tomcat_service


dataservice_log_permissions:
  file.directory:
    - name: {{ dataservice.logs }}
    - user: {{ dataservice.user }}
    - group: {{ dataservice.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: dataservice_install_war

successful_dataservice_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in secondsf
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ dataservice.healtcheck_port }}/{{ dataservice.artifact_name }}/rest/ping
    - match_text: "pong"
    - require:
      - file: dataservice_install_war
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ dataservice.name }} version is {{ dataservice.version }} and {{ dataservice.hash }}"

{%- if dataservice.hipchat_enabled == True %}

dataservice_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ dataservice.auth_token }}
    - room_id: {{ dataservice.room_id }}
    - message: '[{{ dataservice.environment }}] - Artifact version [{{ dataservice.version }}] of component [{{ dataservice.artifact_name }}] was successfully deployed to the [{{ dataservice.environment }}] environment [{{ dataservice.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_dataservice_deployment
    - onchanges:
      - service: tomcat_service

dataservice_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ dataservice.auth_token }}
    - room_id: {{ dataservice.room_id }}
    - message: '[{{ dataservice.environment }}] - Deployment of artifact version [{{ dataservice.version }}] for component [{{ dataservice.artifact_name }}] to the [{{ dataservice.environment }}] environment [{{ dataservice.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: dataservice_hipchat_notifier_deploy_finished

{%- endif %}

{%- if dataservice.slack_enabled == True %}

dataservice_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ dataservice.slack_channel }}'
    - from_name: {{ dataservice.from }}
    - message: '*[{{ dataservice.environment }}]* - Artifact version *[{{ dataservice.version }}]* of component *[{{ dataservice.artifact_name }}]* was successfully deployed to the *[{{ dataservice.environment }}]* environment *[{{ dataservice.host }}]*.'
    - api_key: {{ dataservice.slack_auth_token }}
    - require:
      - health_check: successful_dataservice_deployment
    - onchanges:
      - service: tomcat_service

dataservice_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ dataservice.slack_channel }}'
    - from_name: {{ dataservice.from }}
    - message: '*[{{ dataservice.environment }}]* - Deployment of artifact version *[{{ dataservice.version }}]* for component *[{{ dataservice.artifact_name }}]* to the *[{{ dataservice.environment }}]* environment *[{{ dataservice.host }}]* failed.'
    - api_key: {{ dataservice.slack_auth_token }}
    - onfail:
      - slack: dataservice_slack_notifier_deploy_finished

{%- endif %}
