{%- import 'xanadu-components/xdc-positions-processor/settings.sls' as xdcposprocessor with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcposprocessor_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcposprocessor.artifacts_url }}

xdcposprocessor_version_file:
  file.managed:
    - name: {{ xdcposprocessor.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcposprocessor.artifact_id }}
        version: {{ xdcposprocessor.version }}
        sha1: {{ xdcposprocessor.hash }}
    - require:
      - health_check: xdcposprocessor_nexus_available

xdcposprocessor_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcposprocessor.basedir }}/{{ xdcposprocessor.artifact_id }}-*

xdcposprocessor_get_war_file:
  file.managed:
    - name: {{ xdcposprocessor.jar_loc }}
    - source: {{ xdcposprocessor.artifacts_war_content_url }}
    - source_hash: {{ xdcposprocessor.hash }}
    - require:
      - cmd: xdcposprocessor_remove_previous_versions

#Upload artifact to HDFS
xdcposprocessor_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcposprocessor.admin_server }}:9393/modules/processor/{{ xdcposprocessor.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcposprocessor.jar_loc }}'
    - watch:
      - file: xdcposprocessor_version_file

#xdcposprocessor_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdcposprocessor.admin_server }}:9393/streams/definitions/{{ xdcposprocessor.stream_name }} '


#xdcposprocessor_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcposprocessor.admin_server }}:9393/streams/definitions -d name={{ xdcposprocessor.stream_name }} -d "definition= http  --port=9002 | xdc-json-to-object | xdc-positions-processor | xdc-object-to-json |file --name=outfile" -d deploy=true'
#    - watch:
#      - file: xdcposprocessor_version_file
