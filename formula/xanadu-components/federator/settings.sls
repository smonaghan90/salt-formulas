#nexus base conf
{% set artifacts_url = pillar['component_base_url'] %}
{% set view_api = "service/local/artifact/maven/resolve" %}
{% set group = "com.mb" %}
{% set config = "salt" %}
{% set webapps = "webapps" %}
{% set environment = salt['grains.get']('environment') %}
{% set artifact_name = pillar[environment]['federator']['component']  %}
{% set repo_version  = salt['pillar.get'](environment + ':federator:repo_version', "LATEST") %}
{% set config_version  = salt['pillar.get'](environment + ':federator:config_version', "LATEST") %}
{% set repository_id  = salt['pillar.get']('repository_id', "snapshots") %}
{% set extension = "properties" %}
{% set classifier = "application" %}
{% set extension_props = "json" %}
{% set extension_war = "war" %}
{% set content_api = "service/local/artifact/maven/content" %}


#nexus api
{% set hash = 'sha1=' + salt['cmd.run']( 'curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath \"///sha1/text()\" -' ) %}

{% set version = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///version/text()" -') %}
{% set artifact_id = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///artifactId/text()" -') %}

{% set artifacts_war_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_war %}
{% set artifacts_war_content_hash_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id %}
{% set artifacts_props_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + config + "&v=" +  config_version + "&r=" + repository_id + "&e=" + extension_props  %}



#Check the
{% if salt['grains.get']('function') %}
{% set function = salt['grains.get']('function')%}
{% else %}
{% set function = 'primary' %}
{% endif %}

#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#slack api
{% set slack_enabled  = salt['pillar.get']('slack:enabled', "False") %}
{% set slack_auth_token = salt['pillar.get']('slack:auth_token', "xxxxxxx") %}
{% set slack_channel = salt['pillar.get']('slack:channel', "dummychannel") %}
{% set from = salt['pillar.get']('slack:from', "SaltStack") %}

#base conf
{% set name = pillar[environment]['federator']['component'] %}
{% set tomcat_name = pillar['tomcat_config']['name'] %}
{% set home = pillar['base_install_dir'] + "/" + tomcat_name %}
{% set war_loc = home + "/webapps"  %}
{% set webapp_loc = war_loc + "/" + name  %}
{% set host = salt['grains.get']('host') %}
{% set logs = pillar['base_install_dir'] + "/logs/" + name %}

{% set basedir =  pillar['base_install_dir'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_federator.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar[environment]['federator']['component'] %}
{% set config_dir = home + "/" + webapps + "/" +  artifact_name + "/WEB-INF/classes"   %}
{% set healtcheck_port = salt['pillar.get']('healtcheck_port', "8080") %}




#conf
{% set p  = salt['pillar.get']('tomcat_config', {}) %}
{%- set user = p.get('user', 'tomcat') %}
{%- set group = p.get('group', 'tomcat') %}
{%- set tech_group = p.get('tech_group', '_tech') %}

{% set pn = salt['pillar.get']('settings', {}) %}
{%- set fluentd_port = p.get('fluentd_port', "24224") %}
{%- set fluentd_host = p.get('fluentd_host', "mdb02s1stg.cix.xcl.ie") %}

{% set udpGroup = pillar['tomcat_config']['udpGroup'] %}

#templates
{% set tmpl_file_src = salt['pillar.get']('environment:federator:config:tmpl_file_loc','salt://xanadu-components/federator/file') %}
