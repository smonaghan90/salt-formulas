export CATALINA_HOME="{{ tomcat_home }}"
export CATALINA_OPTS="-server {{ tomcat_mem }} -XX:+HeapDumpOnOutOfMemoryError -Dfile.encoding=UTF-8 -verbose:gc -Xloggc:{{ logs_dir }}/gc.log -XX:+PrintGCTimeStamps -XX:+PrintGCDetails -XX:+PrintTenuringDistribution -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=8086 -Dcom.sun.management.jmxremote.rmi.port=8086 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false -Djava.awt.headless=true -Dfederator.mode={{ function }} -DudpGroup={{ udpGroup }}"
export PATH=$CATALINA_HOME/bin:$PATH
export LD_LIBRARY_PATH="{{ tomcat_home }}/lib:$LD_LIBRARY_PATH"
