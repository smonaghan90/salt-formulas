{%- import 'xanadu-components/federator/settings.sls' as federator with context %}
{%- import 'third-party-components/tomcat/settings.sls' as tc with context %}


include:
  - third-party-components.tomcat

federator_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ federator.artifacts_url }}

federator_version_file:
  file.managed:
    - name: {{ federator.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ federator.artifact_id }}
        version: {{ federator.version }}
        sha1: {{ federator.hash }}
    - require:
      - health_check: federator_nexus_available

federator_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: federator_version_file
      - file: federator_catalina_sh
      - file: federator_setenv_sh


federator-api_clean:
  cmd.wait:
    - name: rm -rf {{ federator.webapp_loc }}
    - watch:
      - file: federator_version_file
    - require:
      - cmd: federator_stop



federator_extract:
  archive.extracted:
    - name: {{ federator.webapp_loc }}
    - source: {{ federator.artifacts_war_content_url }}
    - source_hash: {{ federator.hash }}
    - archive_format: zip
    - if_missing: {{ federator.webapp_loc }}
    - require:
      - cmd:  federator-api_clean
    - watch:
      - file: federator_version_file
    - require_in:
      - service: tomcat_service




federator_setenv_sh:
  file.managed:
    - name: {{ tc.bin_dir }}/setenv.sh
    - source: {{ federator.tmpl_file_src }}/setenv.sh
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - mode: 744
    - template: jinja
    - context:
      logs_dir: {{ tc.logs_dir }}
      tomcat_home: {{ tc.home }}
      tomcat_mem: {{ tc.tomcat_mem }}
      function: {{ federator.function }}
      fluentd_host: {{ federator.fluentd_host }}
      fluentd_port: {{ federator.fluentd_port }}
      udpGroup: {{ tc.udpGroup }}

federator_catalina_sh:
  file.managed:
    - name: {{ tc.bin_dir }}/catalina.sh
    - source: {{ federator.tmpl_file_src }}/catalina.sh
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - mode: 744
    - template: jinja
    - context:
      logs_dir: {{ tc.logs_dir }}
      environment: {{ federator.environment }}


federator_props:
  file.managed:
    - name: {{ federator.config_dir }}/application-{{ federator.environment }}.json
    - source: {{ federator.artifacts_props_content_url }}
    - user: {{ federator.user }}
    - group: {{ federator.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - require:
      - archive: federator_extract
    - require_in:
      - service: tomcat_service


federator_log_permissions:
  file.directory:
    - name: {{ federator.logs }}
    - user: {{ federator.user }}
    - group: {{ federator.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - archive: federator_extract


successful_federator_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ federator.healtcheck_port }}/{{ federator.artifact_name }}/rest/ping
    - match_text: "pong"
    - require:
      - archive: federator_extract
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ federator.name }} version is {{ federator.version }} and {{ federator.hash }}"

{%- if federator.hipchat_enabled == True %}

federator_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ federator.auth_token }}
    - room_id: {{ federator.room_id }}
    - message: '[{{ federator.environment }}] - Artifact version [{{ federator.version }}] of component [{{ federator.artifact_name }}] was successfully deployed to the [{{ federator.environment }}] environment [{{ federator.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_federator_deployment
    - onchanges:
      - service: tomcat_service

federator_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ federator.auth_token }}
    - room_id: {{ federator.room_id }}
    - message: '[{{ federator.environment }}] - Deployment of artifact version [{{ federator.version }}] for component [{{ federator.artifact_name }}] to the [{{ federator.environment }}] environment [{{ federator.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: federator_hipchat_notifier_deploy_finished

{%- endif %}

{%- if federator.slack_enabled == True %}

federator_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ federator.slack_channel }}'
    - from_name: {{ federator.from }}
    - message: '*[{{ federator.environment }}]* - Artifact version *[{{ federator.version }}]* of component *[{{ federator.artifact_name }}]* was successfully deployed to the *[{{ federator.environment }}]* environment *[{{ federator.host }}]*.'
    - api_key: {{ federator.slack_auth_token }}
    - require:
      - health_check: successful_federator_deployment
    - onchanges:
      - service: tomcat_service

federator_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ federator.slack_channel }}'
    - from_name: {{ federator.from }}
    - message: '*[{{ federator.environment }}]* - Deployment of artifact version *[{{ federator.version }}]* for component *[{{ federator.artifact_name }}]* to the *[{{ federator.environment }}]* environment *[{{ federator.host }}]* failed.'
    - api_key: {{ federator.slack_auth_token }}
    - onfail:
      - slack: federator_slack_notifier_deploy_finished

{%- endif %}
