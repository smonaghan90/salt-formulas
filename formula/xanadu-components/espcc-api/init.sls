{%- import 'xanadu-components/espcc-api/settings.sls' as espccapi with context %}
{%- from "xanadu-components/espcc-api/map.jinja" import espccapi_map with context %}

include:
  - third-party-components.java

espcc-api_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ espccapi.artifacts_url }}

espcc-api_version_file:
  file.managed:
    - name: {{ espccapi.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ espccapi.artifact_id }}
        version: {{ espccapi.version }}
        sha1: {{ espccapi.hash }}
    - require:
      - health_check: espcc-api_nexus_available

espcc-api_stop:
  cmd.wait:
    - name: service espcc-api stop
    - onlyif: ps -ef | grep espcc-api | grep -v grep
    - watch:
      - file: espcc-api_version_file
    - require:
      - file: {{ espccapi_map.service_script }}

espcc-api_clean:
  cmd.wait:
    - name: rm -rf {{ espccapi.home }}
    - watch:
      - file: espcc-api_version_file
    - require:
      - cmd: espcc-api_stop

espcc-api_props:
  file.managed:
    - name: {{ espccapi.config_dir }}/application-{{ espccapi.environment }}.properties
    - source: {{ espccapi.artifacts_props_content_url }}
    - user: {{ espccapi.user }}
    - group: {{ espccapi.group }}
    - makedirs: True
    - mode: 644
    - template: jinja

espcc-api_install_jar:
  file.managed:
    - name: {{ espccapi.jar_loc }}
    - source: {{ espccapi.artifacts_jar_content_url }}
    - source_hash: {{ espccapi.hash }}
    - user: {{ espccapi.user }}
    - group: {{ espccapi.group }}
    - makedirs: True
    - mode: 644
    - require:
      - cmd: espcc-api_stop

espcc-api_log_permissions:
  file.directory:
    - name: {{ espccapi.logs }}
    - user: {{ espccapi.user }}
    - group: {{ espccapi.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: espcc-api_install_jar

{%- if espccapi_map.service_script %}
{{ espccapi_map.service_script }}:
  file.managed:
    - source: {{ espccapi.tmpl_file_src }}/{{ espccapi_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ espccapi_map.service_script_mode }}
    - template: jinja
    - context:
       jar_loc: {{ espccapi.jar_loc }}
       config_dir: {{ espccapi.config_dir }}
       environment: {{ espccapi.environment }}
       user: {{ espccapi.user }}
       name: {{ espccapi.name }}

espccapi_service:
  service.running:
    - name: espcc-api
    - sig: java -Dspring
    - require:
      - file: espcc-api_install_jar
      - file: {{ espccapi_map.service_script }}
    - watch:
      - file: espcc-api_props
      - file: espcc-api_install_jar
{%- endif %}

successful_espcc-espccapi_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ espccapi.healtcheck_port }}/manage-api/status
    - match_text: status":"UP"
    - require:
      - file: espcc-api_install_jar
      - file: espcc-api_props
      - service: espccapi_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ espccapi.name }} version is {{ espccapi.version }} and {{ espccapi.hash }}"

{%- if espccapi.hipchat_enabled == True %}

espcc-espccapi_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ espccapi.auth_token }}
    - room_id: {{ espccapi.room_id }}
    - message: '[{{ espccapi.environment }}] - Artifact version [{{ espccapi.version }}] of component [{{ espccapi.artifact_name }}] was successfully deployed to the [{{ espccapi.environment }}] environment [{{ espccapi.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_espcc-espccapi_deployment
    - onchanges:
      - service: espccapi_service

espcc-espccapi_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ espccapi.auth_token }}
    - room_id: {{ espccapi.room_id }}
    - message: '[{{ espccapi.environment }}] - Deployment of artifact version [{{ espccapi.version }}] for component [{{ espccapi.artifact_name }}] to the [{{ espccapi.environment }}] environment [{{ espccapi.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: espcc-espccapi_hipchat_notifier_deploy_finished

{%- endif %}
