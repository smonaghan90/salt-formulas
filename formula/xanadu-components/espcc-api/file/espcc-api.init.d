SERVICE_NAME={{ name }}
PATH_TO_JAR="nohup java -Dspring.config.location={{ config_dir }}/application-{{ environment }}.properties -jar {{ jar_loc }}"
ESPCC_API_USER={{ user }}
RETVAL=0

case $1 in
  start)
        # Start daemon.
        echo "Starting $SERVICE_NAME";
        su -c "$PATH_TO_JAR > /dev/null 2>&1 &" $ESPCC_API_USER
        ;;
  stop)
        # Stop daemons.
        echo "Shutting down $SERVICE_NAME";
        pid=`ps ax | grep -i 'java -Dspring' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          kill -9 $pid
        else
          echo "$SERVICE_NAME was not Running"
        fi
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'java -Dspring' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "$SERVICE_NAME is Running as PID: $pid"
          RETVAL=0
        else
          echo "$SERVICE_NAME is not Running"
          RETVAL=3
        fi

        echo "Return Code: $RETVAL"
                return $RETVAL
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac
exit $RETVAL
