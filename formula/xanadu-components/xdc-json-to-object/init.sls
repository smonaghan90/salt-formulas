{%- import 'xanadu-components/xdc-json-to-object/settings.sls' as xdcjto with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcjto_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcjto.artifacts_url }}

xdcjto_version_file:
  file.managed:
    - name: {{ xdcjto.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcjto.artifact_id }}
        version: {{ xdcjto.version }}
        sha1: {{ xdcjto.hash }}
    - require:
      - health_check: xdcjto_nexus_available

xdcjto_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcjto.basedir }}/{{ xdcjto.artifact_id }}-*


xdcjto_get_war_file:
  file.managed:
    - name: {{ xdcjto.jar_loc }}
    - source: {{ xdcjto.artifacts_war_content_url }}
    - source_hash: {{ xdcjto.hash }}
    - require:
      - cmd: xdcjto_remove_previous_versions

#Upload artifact to HDFS
xdcjto_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcjto.admin_server }}:9393/modules/processor/{{ xdcjto.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcjto.jar_loc }}'
    - watch:
      - file: xdcjto_version_file
