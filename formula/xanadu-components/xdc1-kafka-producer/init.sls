{%- import 'xanadu-components/xdc1-kafka-producer/settings.sls' as xdc1kp with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdc1kp_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1kp.artifacts_url }}

xdc1kp_version_file:
  file.managed:
    - name: {{ xdc1kp.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1kp.artifact_id }}
        version: {{ xdc1kp.version }}
        sha1: {{ xdc1kp.hash }}
    - require:
      - health_check: xdc1kp_nexus_available

xdc1kp_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdc1kp.artifact_id }}-*
    - watch:
      - file: xdc1kp_version_file


xdc1kp_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdc1kp.artifact_id }}-{{ xdc1kp.version }}.jar
    - source: {{ xdc1kp.artifacts_war_content_url }}
    - source_hash: {{ xdc1kp.hash }}
    - user: {{ xdc1kp.user }}
    - group: {{ xdc1kp.group }}
    - mode: 644
    - watch:
      - file: xdc1kp_version_file
    - require:
      - cmd: xdc1kp_remove_previous_versions

xdc1kp_restart_container:
  cmd.wait:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdc1kp_version_file
