{%- import 'xanadu-components/xdc-error-handler/settings.sls' as xdceh with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdceh_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdceh.artifacts_url }}

xdceh_version_file:
  file.managed:
    - name: {{ xdceh.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdceh.artifact_id }}
        version: {{ xdceh.version }}
        sha1: {{ xdceh.hash }}
    - require:
      - health_check: xdceh_nexus_available

xdceh_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdceh.artifact_id }}-*
    - watch:
      - file: xdceh_version_file


xdceh_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdceh.artifact_id }}-{{ xdceh.version }}.jar
    - source: {{ xdceh.artifacts_war_content_url }}
    - source_hash: {{ xdceh.hash }}
    - user: {{ xdceh.user }}
    - group: {{ xdceh.group }}
    - mode: 644
    - watch:
      - file: xdceh_version_file
    - require:
      - cmd: xdceh_remove_previous_versions

xdceh_restart_container:
  cmd.run:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdccore_version_file
