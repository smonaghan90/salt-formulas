{%- import 'xanadu-components/xdc1-utilities/settings.sls' as xdc1util with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdc1util_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1util.artifacts_url }}

xdc1util_version_file:
  file.managed:
    - name: {{ xdc1util.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1util.artifact_id }}
        version: {{ xdc1util.version }}
        sha1: {{ xdc1util.hash }}
    - require:
      - health_check: xdc1util_nexus_available

xdc1util_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdc1util.artifact_id }}-*
    - watch:
      - file: xdc1util_version_file


xdc1util_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdc1util.artifact_id }}-{{ xdc1util.version }}.jar
    - source: {{ xdc1util.artifacts_war_content_url }}
    - source_hash: {{ xdc1util.hash }}
    - user: {{ xdc1util.user }}
    - group: {{ xdc1util.group }}
    - mode: 644
    - watch:
      - file: xdc1util_version_file
    - require:
      - cmd: xdc1util_remove_previous_versions

xdc1util_restart_container:
  cmd.run:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdc1util_version_file
