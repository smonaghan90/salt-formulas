{%- import 'xanadu-components/xdc-mapper-api/settings.sls' as xdcmapperapi with context %}
{%- from "xanadu-components/xdc-mapper-api/map.jinja" import xdcmapperapi_map with context %}

include:
  - third-party-components.java

xdc-mapper-api_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcmapperapi.artifacts_url }}

xdc-mapper-api_version_file:
  file.managed:
    - name: {{ xdcmapperapi.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcmapperapi.artifact_id }}
        version: {{ xdcmapperapi.version }}
        sha1: {{ xdcmapperapi.hash }}
    - require:
      - health_check: xdc-mapper-api_nexus_available

xdc-mapper-api_stop:
  cmd.wait:
    - name: service xdc-mapper-api stop
    - onlyif: ps -ef | grep xdc-mapper-api | grep -v grep
    - watch:
      - file: xdc-mapper-api_version_file
    - require:
      - file: {{ xdcmapperapi_map.service_script }}

xdc-mapper-api_clean:
  cmd.wait:
    - name: rm -rf {{ xdcmapperapi.home }}
    - watch:
      - file: xdc-mapper-api_version_file
    - require:
      - cmd: xdc-mapper-api_stop

xdc-mapper-api_props:
  file.managed:
    - name: {{ xdcmapperapi.config_dir }}/application-{{ xdcmapperapi.environment }}.properties
    - source: {{ xdcmapperapi.artifacts_props_content_url }}
    - source_hash: {{ xdcmapperapi.artifacts_props_hash }}
    - user: {{ xdcmapperapi.user }}
    - group: {{ xdcmapperapi.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - require:
      - cmd: xdc-mapper-api_stop

xdc-mapper-api_install_jar:
  file.managed:
    - name: {{ xdcmapperapi.jar_loc }}
    - source: {{ xdcmapperapi.artifacts_jar_content_url }}
    - source_hash: {{ xdcmapperapi.hash }}
    - user: {{ xdcmapperapi.user }}
    - group: {{ xdcmapperapi.group }}
    - makedirs: True
    - mode: 644
    - require:
      - cmd: xdc-mapper-api_stop

xdc-mapper-api_log_permissions:
  file.directory:
    - name: {{ xdcmapperapi.logs }}
    - user: {{ xdcmapperapi.user }}
    - group: {{ xdcmapperapi.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: xdc-mapper-api_install_jar

{%- if xdcmapperapi_map.service_script %}
{{ xdcmapperapi_map.service_script }}:
  file.managed:
    - source: {{ xdcmapperapi.tmpl_file_src }}/{{ xdcmapperapi_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ xdcmapperapi_map.service_script_mode }}
    - template: jinja
    - context:
       jar_loc: {{ xdcmapperapi.jar_loc }}
       config_dir: {{ xdcmapperapi.config_dir }}
       environment: {{ xdcmapperapi.environment }}
       user: {{ xdcmapperapi.user }}
       name: {{ xdcmapperapi.name }}

xdcmapperapi_service:
  service.running:
    - name: xdc-mapper-api
    - sig: 'xdc-mapper-api'
    - require:
      - file: xdc-mapper-api_install_jar
      - file: {{ xdcmapperapi_map.service_script }}
    - watch:
      - file: xdc-mapper-api_props
      - file: xdc-mapper-api_install_jar
      - file: xdc-mapper-api_version_file
{%- endif %}

successful_xdcmapperapi_deployment:
  health_check.wait_for_url:
    - timeout: 130 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ xdcmapperapi.healtcheck_port }}/status
    - match_text: 'Running'
    - require:
      - file: xdc-mapper-api_install_jar
      - file: xdc-mapper-api_props
      - service: xdcmapperapi_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ xdcmapperapi.name }} version is {{ xdcmapperapi.version }} and {{ xdcmapperapi.hash }}"

{%- if xdcmapperapi.hipchat_enabled == True %}

xdcmapperapi_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ xdcmapperapi.auth_token }}
    - room_id: {{ xdcmapperapi.room_id }}
    - message: '[{{ xdcmapperapi.environment }}] - Artifact version [{{ xdcmapperapi.version }}] of component [{{ xdcmapperapi.artifact_name }}] was successfully deployed to the [{{ xdcmapperapi.environment }}] environment [{{ xdcmapperapi.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_xdcmapperapi_deployment
    - onchanges:
      - service: xdcmapperapi_service

xdcmapperapi_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ xdcmapperapi.auth_token }}
    - room_id: {{ xdcmapperapi.room_id }}
    - message: '[{{ xdcmapperapi.environment }}] - Deployment of artifact version [{{ xdcmapperapi.version }}] for component [{{ xdcmapperapi.artifact_name }}] to the [{{ xdcmapperapi.environment }}] environment [{{ xdcmapperapi.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: xdcmapperapi_hipchat_notifier_deploy_finished

{%- endif %}
