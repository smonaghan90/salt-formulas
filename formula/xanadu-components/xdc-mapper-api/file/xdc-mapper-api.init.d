SERVICE_NAME={{ name }}
PATH_TO_JAR="nohup java -Dspring.config.location={{ config_dir }}/application-{{ environment }}.properties -jar {{ jar_loc }}"
MAPPER_API_USER={{ user }}
RETVAL=0

case $1 in
  start)
        # Start daemon.
        echo "Starting $SERVICE_NAME";
        su -c "$PATH_TO_JAR >  /dev/null 2>&1 &"
        ;;
  stop)
        # Stop daemons.
        echo "Shutting down $SERVICE_NAME";
        pid=`ps aux | grep -i "xdc-mapper" | grep -v grep | grep -v sh | awk {'print $2'}`
        echo $pid
        if ps -p $pid > /dev/null 2>&1
        then
          for x in $pid; do echo "Killing $x"; kill -9 $x  ;done
        else
          echo "Process was not actually running"
        fi
        ;;
  restart)
        $0 stop
        sleep 4
        $0 start
        ;;
  status)
        pstree -p
        pid=$(ps aux | grep -i "xdc-mapper"| grep -v grep | grep -v sh | awk {'print $2'})
        if [ -n "$pid" ]
          then
          echo "$SERVICE_NAME is Running as PID: $pid"
          RETVAL=0
        else
          echo "$SERVICE_NAME is not Running"
          RETVAL=3
        fi

        echo "Return Code: $RETVAL"
                return $RETVAL
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac
exit $RETVAL
