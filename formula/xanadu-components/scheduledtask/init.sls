{%- import 'xanadu-components/scheduledtask/settings.sls' as schdtsk with context %}
{%- import 'third-party-components/tomcat/settings.sls' as tc with context %}

include:
  - third-party-components.tomcat

scheduledtask_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ schdtsk.artifacts_url }}

scheduledtask_version_file:
  file.managed:
    - name: {{ schdtsk.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ schdtsk.artifact_id }}
        version: {{ schdtsk.version }}
        sha1: {{ schdtsk.hash }}
    - require:
      - health_check: scheduledtask_nexus_available

scheduledtask_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - watch:
      - file: scheduledtask_version_file
      - file: scheduledtask_deploy_cfg

scheduledtask_clean:
  cmd.wait:
    - name: rm -rf {{ schdtsk.war_dir }}
    - watch:
      - file: scheduledtask_version_file
      - file: scheduledtask_deploy_cfg
    - require:
      - cmd: scheduledtask_stop
    - onlyif: 
      - cmd: test -f {{ schdtsk.war_loc }}

scheduledtask_install_war:
  file.managed:
    - name: {{ schdtsk.war_loc }}
    - source: {{ schdtsk.artifacts_war_content_url }}
    - source_hash: {{ schdtsk.artifacts_war_content_hash_url }}
    - user: {{ schdtsk.user }}
    - group: {{ schdtsk.group }}
    - mode: 744
    - require:
      - cmd: scheduledtask_clean
      - file: scheduledtask_deploy_cfg
    - require_in:
      - service: tomcat_service

scheduledtask_deploy_cfg:
  file.managed:
    - name: {{ schdtsk.conf }}/deploy.cfg
    - source: {{ schdtsk.tmpl_file_src }}/deploy.cfg
    - user: {{ schdtsk.user }}
    - group: {{ schdtsk.group }}
    - mode: 644
    - template: jinja
    - context:
      environment: {{ schdtsk.environment }}
      tomcat_home: {{ tc.home }}
    - require_in:
      - service: tomcat_service

scheduledtask_log_permissions:
  file.directory:
    - name: {{ schdtsk.logs }}
    - user: {{ schdtsk.user }}
    - group: {{ schdtsk.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require_in:
      - service: tomcat_service

scheduledtask_deploy_script:
  file.managed:
    - name: {{ schdtsk.bin }}/deploy.pl
    - source: {{ schdtsk.tmpl_file_src }}/deploy.pl
    - user: {{ schdtsk.user }}
    - group: {{ schdtsk.group }}
    - mode: 744
    - template: jinja
    - require:
      - file: scheduledtask_install_war

scheduledtask_run_deploy_script:
  cmd.wait:
    - name: {{ schdtsk.bin }}/deploy.pl {{ schdtsk.environment }}
    - watch:
      - file: scheduledtask_deploy_cfg
      - file: scheduledtask_version_file
    - require:
      - file: scheduledtask_deploy_script
    - require_in:
      - service: tomcat_service

successful_scheduledtask_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ schdtsk.healtcheck_port }}/{{ schdtsk.artifact_name }}
    - match_text: "MatchBook-Admin<"
    - username: {{ tc.manager_username }}
    - password: {{ tc.manager_password}}
    - require:
      - file: scheduledtask_install_war
      - service: tomcat_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ schdtsk.name }} version is {{ schdtsk.version }} and {{ schdtsk.hash }}"

{%- if schdtsk.hipchat_enabled == True %}

scheduledtask_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ schdtsk.auth_token }}
    - room_id: {{ schdtsk.room_id }}
    - message: '[{{ schdtsk.environment }}] - Artifact version [{{ schdtsk.version }}] of component [{{ schdtsk.artifact_name }}] was successfully deployed to the [{{ schdtsk.environment }}] environment [{{ schdtsk.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_scheduledtask_deployment
    - onchanges:
      - service: tomcat_service

scheduledtask_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ schdtsk.auth_token }}
    - room_id: {{ schdtsk.room_id }}
    - message: '[{{ schdtsk.environment }}] - Deployment of artifact version [{{ schdtsk.version }}] for component [{{ schdtsk.artifact_name }}] to the [{{ schdtsk.environment }}] environment [{{ schdtsk.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: scheduledtask_hipchat_notifier_deploy_finished

{%- endif %}

{%- if schdtsk.slack_enabled == True %}

scheduledtask_slack_notifier_deploy_finished:
  slack.post_message:
    - channel: '#{{ schdtsk.slack_channel }}'
    - from_name: {{ schdtsk.from }}
    - message: '*[{{ schdtsk.environment }}]* - Artifact version *[{{ schdtsk.version }}]* of component *[{{ schdtsk.artifact_name }}]* was successfully deployed to the *[{{ schdtsk.environment }}]* environment *[{{ schdtsk.host }}]*.'
    - api_key: {{ schdtsk.slack_auth_token }}
    - require:
      - health_check: successful_scheduledtask_deployment
    - onchanges:
      - service: tomcat_service

scheduledtask_slack_notifier_deploy_failed:
  slack.post_message:
    - channel: '#{{ schdtsk.slack_channel }}'
    - from_name: {{ schdtsk.from }}
    - message: '*[{{ schdtsk.environment }}]* - Deployment of artifact version *[{{ schdtsk.version }}]* for component *[{{ schdtsk.artifact_name }}]* to the *[{{ schdtsk.environment }}]* environment *[{{ schdtsk.host }}]* failed.'
    - api_key: {{ schdtsk.slack_auth_token }}
    - onfail:
      - slack: scheduledtask_slack_notifier_deploy_finished

{%- endif %}
