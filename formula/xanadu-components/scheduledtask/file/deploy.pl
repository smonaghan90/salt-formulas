#! /usr/bin/perl -w

use strict;
use warnings;
use Carp;
use Data::Dumper;
use File::Glob qw/:glob/;
use File::Find;
use YAML qw/Dump LoadFile/;

my $target = shift;

# read the config and trap for errors
my $filename = $0;
$filename =~ s/(\.pl)?$/.cfg/;
$filename =~ s|/bin/([^/]+).cfg$|/conf/$1.cfg| unless -f $filename;
$filename =~ s|^bin/([^/]+).cfg$|conf/$1.cfg| unless -f $filename;
$filename =~ s|^\./([^/]+).cfg$|../conf/$1.cfg| unless -f $filename;
$filename =~ s|/bin/([^/]+).cfg$|/etc/$1.cfg| unless -f $filename;
$filename =~ s|^bin/([^/]+).cfg$|etc/$1.cfg| unless -f $filename;
$filename =~ s|^\./([^/]+).cfg$|../etc/$1.cfg| unless -f $filename;
my $config      = LoadFile $filename;
my $enableDebug = $config->{enableDebug};
print STDERR Dump( { filename => $filename, config => $config } ) if $enableDebug;

die "The 'enableConfig:' option is not set to '1' in $filename.\nExting program ...\n"
  unless $config->{enableConfig};

my $source      = $config->{source};
my $targetType  = $config->{targetType};
my @replaceList = @{ $config->{replace} };
print STDERR Dump( { replaceList => \@replaceList, source => $source } ) if $enableDebug;

die "The config file does not have a '$target'." unless exists $targetType->{$target};

my $data = $targetType->{$target};

# Setup and explode the new war
my $apps      = $source->{apps};
my $name      = $source->{name};
my $base      = $source->{base};
my $separator = $source->{separator};
my $oldcwd    = chdir $base;

# Find a war to deploy
my @wars = bsd_glob $source->{file};
die "ERROR: There are no files to deploy! [$source->{file}]" if $#wars < 0;
my $war     = $wars[ $source->{choose} ];
my $warpath = $base . $separator . $apps . $separator . $war;
print STDERR Dump( { wars => \@wars, selected => $war, warpath => $warpath } ) if $enableDebug;

# Check that we are not overwriting anything and expand the war
die "ERROR: $base/$apps/$name exists." if ( -e "$base/$apps/$name" );
chdir "$apps";
mkdir $name;
chdir $name;
system $source->{expand} . " " . $warpath;
chdir $base;

# find the target files
my @fileList = ();

find(
    {
        wanted => sub {
            foreach my $replace (@replaceList) {
                my $name = $_;
                push @fileList, $_ if $name =~ m/$replace/;
            }
        },
        no_chdir => 1
    },
    "$apps"
);

my $map = {};
foreach my $key1 ( keys %$data ) {
    foreach my $key2 ( keys %{ $data->{$key1} } ) {
        $map->{"<<$key1:$key2>>"} = $data->{$key1}->{$key2};
        $map->{"<<$key1:$key2>>"} = " " if ! defined $data->{$key1}->{$key2};
    }
}

# replace the contents
my $replaceFiles = {};
foreach my $file (@fileList) {
    my $contents = "";
    {
        local $/ = undef;
        open FILE, $file or die "Couldn't open file: $!";
        binmode FILE;
        $contents = <FILE>;
        close FILE;
    }

    if ( $contents =~ m/(<<[^:>]+:[^>]+>>)/s ) {
        foreach my $key ( keys %$map ) {
            $replaceFiles->{$file}->{$key} = $map->{$key} if $contents =~ m/$key/;
            $contents =~ s/$key/$map->{$key}/gs;
        }
        print STDERR Dump({ $file => $replaceFiles->{$file}}) if $enableDebug;
        die "There is no value for $1 in $file\n" if ( $contents =~ m/(<<[^:>]+:[^>]+>>)/s ) ;
        open FILE, ">", $file;
        print FILE $contents;
        close FILE;
    }
}

# vim: si ts=8 sw=4
