#nexus base conf
{% set artifacts_url = pillar['component_base_url'] %}
{% set view_api = "service/local/artifact/maven/resolve" %}
{% set group = "com.xanaduconsultancy" %}
{% set environment = salt['grains.get']('environment') %}
{% set parent_artifact_name = pillar[environment]['matchbook']['component']  %}
{% set artifact_name = pillar[environment]['scheduledtask']['component']  %}
{% set repo_version  = salt['pillar.get'](environment + ':scheduledtask:repo_version', "LATEST") %}
{% set repository_id  = salt['pillar.get']('repository_id', "snapshots") %}
{% set extension_props = "properties" %}
{% set extension_war = "war" %}
{% set classifier = "application" %}
# Using workaround api for classic
{% set content_api = "service/local/repositories/" + repository_id + "/content" %}
{% set content_group = "/com/xanaduconsultancy" %}

#nexus api
{% set hash = 'sha1=' + salt['cmd.run']( 'curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + parent_artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath \"///sha1/text()\" -' ) %}
{% set version = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + parent_artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///version/text()" -') %}
{% set artifact_id = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + parent_artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///artifactId/text()" -') %}

{% set artifacts_war_content_url = artifacts_url + "/" + content_api + content_group + "/" + parent_artifact_name  + "/" +  version + "/" + artifact_name + "-" + version + ".war" %}
{% set artifacts_war_content_hash_url = artifacts_url + "/" + content_api + content_group + "/" + parent_artifact_name + "/"  + version + "/" + artifact_name + "-" + version + ".war.md5" %}

#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#slack api
{% set slack_enabled  = salt['pillar.get']('slack:enabled', "False") %}
{% set slack_auth_token = salt['pillar.get']('slack:auth_token', "xxxxxxx") %}
{% set slack_channel = salt['pillar.get']('slack:channel', "dummychannel") %}
{% set from = salt['pillar.get']('slack:from', "SaltStack") %}

#base conf
{% set name = pillar[environment]['scheduledtask']['component'] %}
{% set tomcat_name = pillar['tomcat_config']['name'] %}
{% set home = pillar['base_install_dir'] + "/" + tomcat_name %}
{% set conf = home + "/conf"%}
{% set bin = home + "/bin" %}
{% set war_dir = home + "/webapps/" + name + "*"%}
{% set war_loc = home + "/webapps/" + name + ".war" %}
{% set host = salt['grains.get']('host') %}
{% set logs = pillar['base_install_dir'] + "/logs/" + name %}

{% set basedir =  pillar['base_install_dir'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_scheduledtask.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar[environment]['scheduledtask']['component'] %}
{% set config_dir = home + "/conf" %}
{% set healtcheck_port = salt['pillar.get']('healtcheck_port', "8080") %}

#conf
{% set p  = salt['pillar.get']('tomcat_config', {}) %}
{%- set user = p.get('user', 'tomcat') %}
{%- set group = p.get('group', 'tomcat') %}
{%- set tech_group = p.get('tech_group', '_tech') %}

#templates
{% set tmpl_file_src = salt['pillar.get']('environment:scheduledtask:config:tmpl_file_loc','salt://xanadu-components/scheduledtask/file') %}
