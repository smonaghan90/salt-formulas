{%- import 'xanadu-components/xdc-im-sis-source/settings.sls' as xdcimsiss with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcimsiss_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcimsiss.artifacts_url }}

xdcimsiss_version_file:
  file.managed:
    - name: {{ xdcimsiss.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcimsiss.artifact_id }}
        version: {{ xdcimsiss.version }}
        sha1: {{ xdcimsiss.hash }}
    - require:
      - health_check: xdcimsiss_nexus_available

xdcimsiss_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcimsiss.basedir }}/{{ xdcimsiss.artifact_id }}-*


xdcimsiss_get_war_file:
  file.managed:
    - name: {{ xdcimsiss.jar_loc }}
    - source: {{ xdcimsiss.artifacts_war_content_url }}
    - source_hash: {{ xdcimsiss.hash }}
    - require:
      - cmd: xdcimsiss_remove_previous_versions

#Upload artifact to HDFS'''
xdcimsiss_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcimsiss.admin_server }}:9393/modules/processor/{{ xdcimsiss.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcimsiss.jar_loc }}'
    - watch:
      - file: xdcimsiss_version_file

#xdcimsiss_remove_stream:
  #cmd.run:
  #  - name:  'curl -s -X DELETE {{ xdcimsiss.admin_server }}:9393/streams/definitions/{{ xdcimsiss.stream_name }} '


#xdcimsiss_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcimsiss.admin_server }}:9393/streams/definitions -d name={{ xdcimsiss.stream_name }} -d "definition={{ xdcimsiss.artifact_id }}" -d deploy=true'
#    - watch:
#      - file: xdcimsiss_version_file
