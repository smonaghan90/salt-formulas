{%- import 'xanadu-components/xdc1-error-handler/settings.sls' as xdc1eh with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdc1eh_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1eh.artifacts_url }}

xdc1eh_version_file:
  file.managed:
    - name: {{ xdc1eh.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1eh.artifact_id }}
        version: {{ xdc1eh.version }}
        sha1: {{ xdc1eh.hash }}
    - require:
      - health_check: xdc1eh_nexus_available

xdc1eh_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdc1eh.artifact_id }}-*
    - watch:
      - file: xdc1eh_version_file


xdc1eh_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdc1eh.artifact_id }}-{{ xdc1eh.version }}.jar
    - source: {{ xdc1eh.artifacts_war_content_url }}
    - source_hash: {{ xdc1eh.hash }}
    - user: {{ xdc1eh.user }}
    - group: {{ xdc1eh.group }}
    - mode: 644
    - watch:
      - file: xdc1eh_version_file
    - require:
      - cmd: xdc1eh_remove_previous_versions

xdc1eh_restart_container:
  cmd.wait:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdc1eh_version_file
