{%- import 'xanadu-components/xdc1-integration-module-smartodds/settings.sls' as xdc1ims with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer


xdc1ims_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1ims.artifacts_url }}

xdc1ims_version_file:
  file.managed:
    - name: {{ xdc1ims.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1ims.artifact_id }}
        version: {{ xdc1ims.version }}
        sha1: {{ xdc1ims.hash }}
    - require:
      - health_check: xdc1ims_nexus_available

xdc1ims_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdc1ims.basedir }}/{{ xdc1ims.artifact_id }}-*

xdc1ims_get_war_file:
  file.managed:
    - name: {{ xdc1ims.jar_loc }}
    - source: {{ xdc1ims.artifacts_war_content_url }}
    - source_hash: {{ xdc1ims.hash }}
    - require:
      - cmd: xdc1ims_remove_previous_versions

xdc1ims-api_props:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/modules/source/{{ xdc1ims.artifact_id }}/{{ xdc1ims.artifact_id }}.properties
    - source: {{ xdc1ims.artifacts_props_content_url }}
    - source_hash: {{ xdc1ims.artifacts_props_hash }}
    - user: {{ xdc1ims.user }}
    - group: {{ xdc1ims.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - watch:
      - file: xdc1ims_version_file

xdc1ims-keystore:
  file.managed:
    - name: {{ xdcontainer.home }}/xd/config/modules/source/{{ xdc1ims.artifact_id }}/xanadukeystore.jks
    - source: {{ xdc1ims.tmpl_file_src }}/keystore.jks
    - user: {{ xdc1ims.user }}
    - group: {{ xdc1ims.group }}
    - makedirs: True
    - mode: 644
    - watch:
      - file: xdc1ims_version_file

xdc1ims_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdc1ims.admin_server }}:9393/modules/source/{{ xdc1ims.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdc1ims.jar_loc }}'
    - watch:
      - file: xdc1ims_version_file


{%- if xdc1ims.hipchat_enabled == True %}

xdc1ims_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ xdc1ims.auth_token }}
    - room_id: {{ xdc1ims.room_id }}
    - message: '[{{ xdc1ims.environment }}] - Uploaded version [{{ xdc1ims.version }}] of component [{{ xdc1ims.artifact_name }}] to the [{{ xdc1ims.environment }}] environment [{{ xdc1ims.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - onchanges:
      - file: xdc1ims_version_file

xdc1ims_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ xdc1ims.auth_token }}
    - room_id: {{ xdc1ims.room_id }}
    - message: '[{{ xdc1ims.environment }}] - Uploaded  artifact version [{{ xdc1ims.version }}] for component [{{ xdc1ims.artifact_name }}] to the [{{ xdc1ims.environment }}] environment [{{ xdc1ims.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: xdc1ims_hipchat_notifier_deploy_finished

{%- endif %}


#xdc1ims_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdc1ims.admin_server }}:9393/streams/definitions/{{ xdc1ims.stream_name }} '


#xdc1ims_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdc1ims.admin_server }}:9393/streams/definitions -d name={{ xdc1ims.stream_name }} -d "definition={{ xdc1ims.artifact_id }}" -d deploy=true'
#    - watch:
#      - file: xdc1ims_version_file
