{%- import 'xanadu-components/mercury-api/settings.sls' as mercuryapi with context %}
{%- import 'third-party-components/glassfish/settings.sls' as gf3 with context %}


include:
  - third-party-components.glassfish

mercuryapi_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ mercuryapi.artifacts_url }}

mercuryapi_version_file:
  file.managed:
    - name: {{ mercuryapi.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ mercuryapi.artifact_id }}
        version: {{ mercuryapi.version }}
        sha1: {{ mercuryapi.hash }}
    - require:
      - health_check: mercuryapi_nexus_available

mercuryapi_asadmin_file:
  file.managed:
    - name: {{ mercuryapi.glassfish_bin }}/AS_ADMIN
    - source: {{ mercuryapi.tmpl_file_src }}/AS_ADMIN
    - user: {{ mercuryapi.user }}
    - group: {{ mercuryapi.group }}
    - makedirs: True
    - mode: 644

mercuryapi_remove_default_domain1:
  cmd.wait:
    - names:
      - {{ gf3.bin_dir }}/asadmin stop-domain domain1
      - {{ gf3.bin_dir }}/asadmin delete-domain domain1
    - watch:
      - file: glassfish_version_file


mercuryapi_stop_mercury_domain:
  cmd.wait:
    - names:
      - {{ gf3.bin_dir }}/asadmin stop-domain {{ mercuryapi.domain_name }}
      - pkill -9 java
    - watch:
      - file: mercuryapi_version_file


mercuryapi_remove_mercury_domain:
  cmd.wait:
    - names:
      - {{ gf3.bin_dir }}/asadmin delete-domain {{ mercuryapi.domain_name }}
    - watch:
      - file: mercuryapi_version_file



mercuryapi_create_base_domain:
  cmd.wait:
    - names:
      - {{ gf3.bin_dir }}/asadmin --user {{ mercuryapi.domain_name }}  --passwordfile {{ mercuryapi.glassfish_bin }}/AS_ADMIN create-domain --savemasterpassword=true  --portbase {{ mercuryapi.base_port }} {{ mercuryapi.domain_name }}
    - require:
      - health_check: mercuryapi_nexus_available
    - watch:
      - file: mercuryapi_version_file




mercuryapi_copy_mariadb_drivers:
  cmd.wait:
    - name: |
        wget -P {{ mercuryapi.base_mercury_domain }}/lib {{ mercuryapi.mariadb_jar_package }}
    - require:
      - health_check: mercuryapi_nexus_available
    - watch:
      - file: mercuryapi_version_file



mercuryapi_copy_mysql_drivers:
  cmd.wait:
    - name: |
        wget -P {{ mercuryapi.base_mercury_domain }}/lib {{ mercuryapi.mysql_jar_package }}
    - require:
      - health_check: mercuryapi_nexus_available
    - watch:
      - file: mercuryapi_version_file


mercuryapi_start_domain:
  cmd.wait:
    - name: |
        {{ gf3.bin_dir }}/asadmin --user {{ mercuryapi.domain_name }}  start-domain {{ mercuryapi.domain_name }}
    - watch:
      - file: mercuryapi_version_file


mercuryapi_config_file:
  file.managed:
    - name: {{ mercuryapi.glassfish_bin }}/mercury-gf-config.txt
    - source: {{ mercuryapi.tmpl_file_src }}/mercury-gf-config.txt
    - user: {{ mercuryapi.user }}
    - group: {{ mercuryapi.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - context:
      maria_db: {{ mercuryapi.maria_db }}
      base_port: {{ mercuryapi.base_port }}
      admin_port: {{ mercuryapi.admin_port }}
      password: {{ mercuryapi.password }}
      base_domain : {{ mercuryapi.base_domain }}
      xmx: {{ mercuryapi.xmx }}
      xms: {{ mercuryapi.xms }}
      domain_name: {{ mercuryapi.domain_name }}
      domains: {{ gf3.domains }}
      bin_dir: {{ gf3.bin_dir }}
      glassfish_bin: {{ mercuryapi.glassfish_bin }}
    - require_in:
      - service: glassfish_service


mercuryapi_run_config_file:
  cmd.wait:
    - names:
      - {{ gf3.bin_dir }}/asadmin --port {{ mercuryapi.admin_port }} multimode --file {{ mercuryapi.glassfish_bin }}/mercury-gf-config.txt
    - require:
      - health_check: mercuryapi_nexus_available
    - watch:
      - file: mercuryapi_version_file



mercuryapi_props:
  file.managed:
    - name: {{ mercuryapi.base_mercury_domain }}/config/mercury-config.properties
    - source: {{ mercuryapi.artifacts_props_content_url }}
    - source_hash: {{ mercuryapi.artifacts_props_hash }}
    - user: {{ mercuryapi.user }}
    - group: {{ mercuryapi.group }}
    - makedirs: True
    - mode: 644
    - template: jinja
    - context:
    - watch:
      - file: mercuryapi_version_file

xdc1dw_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ mercuryapi.base_mercury_domain }}/{{ mercuryapi.artifact_id }}-*
    - watch:
      - file: mercuryapi_version_file


mercuryapi_install_war:
  file.managed:
    - name:  {{ mercuryapi.base_mercury_domain }}/{{ mercuryapi.artifact_id }}-{{ mercuryapi.version }}.{{mercuryapi.extension_war }}
    - source: {{ mercuryapi.artifacts_war_content_url }}
    - source_hash: {{ mercuryapi.hash }}
    - user: {{ mercuryapi.user }}
    - group: {{ mercuryapi.group }}
    - makedirs: True
    - mode: 644
    - watch:
      - file: mercuryapi_version_file

mercuryapi_deploy_war:
  cmd.wait:
    - name: |
        {{ gf3.bin_dir }}/asadmin --user {{ mercuryapi.domain_name }}  --passwordfile {{ mercuryapi.glassfish_bin }}/AS_ADMIN   --port {{ mercuryapi.admin_port }} deploy --force=true {{ mercuryapi.base_mercury_domain }}/{{ mercuryapi.artifact_id }}-{{ mercuryapi.version }}.{{mercuryapi.extension_war }}
    - watch:
      - file: mercuryapi_version_file



successful_mercuryapi_deployment:
  health_check.wait_for_url:
    - timeout: 10 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ mercuryapi.healtcheck_port }}/mercury/api/status
    - match_text: "application-info"
    - require:
      - file: mercuryapi_install_war
      - file: mercuryapi_props
      - service: glassfish_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ mercuryapi.name }} version is {{ mercuryapi.version }} and {{ mercuryapi.hash }}"


{%- if mercuryapi.hipchat_enabled == True %}

mercuryapi_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ mercuryapi.auth_token }}
    - room_id: {{ mercuryapi.room_id }}
    - message: '[{{ mercuryapi.environment }}] - Artifact version [{{ mercuryapi.version }}] of component [{{ mercuryapi.artifact_name }}] was successfully deployed to the [{{ mercuryapi.environment }}] environment [{{ mercuryapi.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_mercuryapi_deployment
    - onchanges:
      - service: glassfish_service

mercuryapi_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ mercuryapi.auth_token }}
    - room_id: {{ mercuryapi.room_id }}
    - message: '[{{ mercuryapi.environment }}] - Deployment of artifact version [{{ mercuryapi.version }}] for component [{{ mercuryapi.artifact_name }}] to the [{{ mercuryapi.environment }}] environment [{{ mercuryapi.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: mercuryapi_hipchat_notifier_deploy_finished

{%- endif %}
