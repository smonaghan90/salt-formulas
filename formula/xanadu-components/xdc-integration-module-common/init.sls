{%- import 'xanadu-components/xdc-integration-module-common/settings.sls' as xdcimc with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcimc_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcimc.artifacts_url }}

xdcimc_version_file:
  file.managed:
    - name: {{ xdcimc.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcimc.artifact_id }}
        version: {{ xdcimc.version }}
        sha1: {{ xdcimc.hash }}
    - require:
      - health_check: xdcimc_nexus_available

xdcimc_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcimc.basedir }}/{{ xdcimc.artifact_id }}-*


xdcimc_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdcimc.artifact_id }}-{{ xdcimc.version }}.jar
    - source: {{ xdcimc.artifacts_war_content_url }}
    - source_hash: {{ xdcimc.hash }}
    - user: {{ xdcimc.user }}
    - group: {{ xdcimc.group }}
    - mode: 644
    - watch:
      - file: xdcimc_version_file
    - require:
      - cmd: xdcimc_remove_previous_versions
