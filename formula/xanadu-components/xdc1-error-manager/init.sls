{%- import 'xanadu-components/xdc1-error-handler/settings.sls' as xdc1em with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdc1em_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdc1em.artifacts_url }}

xdc1em_version_file:
  file.managed:
    - name: {{ xdc1em.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdc1em.artifact_id }}
        version: {{ xdc1em.version }}
        sha1: {{ xdc1em.hash }}
    - require:
      - health_check: xdc1em_nexus_available

xdc1em_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdc1em.artifact_id }}-*
    - watch:
      - file: xdc1em_version_file


xdc1em_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdc1em.artifact_id }}-{{ xdc1em.version }}.jar
    - source: {{ xdc1em.artifacts_war_content_url }}
    - source_hash: {{ xdc1em.hash }}
    - user: {{ xdc1em.user }}
    - group: {{ xdc1em.group }}
    - mode: 644
    - watch:
      - file: xdc1em_version_file
    - require:
      - cmd: xdc1em_remove_previous_versions

xdc1em_restart_container:
  cmd.wait:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdc1em_version_file
