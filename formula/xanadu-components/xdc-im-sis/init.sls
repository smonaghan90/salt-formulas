{%- import 'xanadu-components/xdc-im-sis/settings.sls' as xdcimsis with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcimsis_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcimsis.artifacts_url }}

xdcimsis_version_file:
  file.managed:
    - name: {{ xdcimsis.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcimsis.artifact_id }}
        version: {{ xdcimsis.version }}
        sha1: {{ xdcimsis.hash }}
    - require:
      - health_check: xdcimsis_nexus_available

xdcimsis_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcimsis.basedir }}/{{ xdcimsis.artifact_id }}-*


xdcimsis_get_war_file:
  file.managed:
    - name: {{ xdcimsis.jar_loc }}
    - source: {{ xdcimsis.artifacts_war_content_url }}
    - source_hash: {{ xdcimsis.hash }}
    - require:
      - cmd: xdcimsis_remove_previous_versions

#Upload artifact to HDFS
xdcimsis_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcimsis.admin_server }}:9393/modules/processor/{{ xdcimsis.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcimsis.jar_loc }}'
    - watch:
      - file: xdcimsis_version_file

xdcimsis_remove_stream:
  cmd.run:
    - name:  'curl -s -X DELETE {{ xdcimsis.admin_server }}:9393/streams/definitions/{{ xdcimsis.stream_name }} '


xdcimsis_create_stream:
  cmd.run:
    - name: ' curl -s -X POST {{ xdcimsis.admin_server }}:9393/streams/definitions -d name={{ xdcimsis.stream_name }} -d "definition={{ xdcimsis.artifact_id }}" -d deploy=true'
    - watch:
      - file: xdcimsis_version_file
