#nexus base conf
{% set artifacts_url = pillar['component_base_url'] %}
{% set view_api = "service/local/artifact/maven/resolve" %}
{% set group = "com.xanaduconsultancy" %}
{% set environment = salt['grains.get']('environment') %}
{% set artifact_name = pillar[environment]['xdc-legacy-api']['component']  %}
{% set repo_version  = salt['pillar.get'](environment + ':xdc-legacy-api:repo_version', "LATEST") %}
{% set repository_id  = salt['pillar.get']('repository_id', "snapshots") %}
{% set extension = "properties" %}
{% set classifier = "application" %}
{% set extension_md5 = "properties.md5" %}
{% set content_api = "service/local/artifact/maven/content" %}


#nexus api
{% set hash = 'sha1=' + salt['cmd.run']( 'curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "\"" + ' | xmllint --xpath \"///sha1/text()\" -' ) %}
{% set version = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "\"" + ' | xmllint --xpath "///version/text()" -') %}
{% set artifact_id = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "\"" + ' | xmllint --xpath "///artifactId/text()" -') %}

{% set artifacts_jar_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id %}
{% set artifacts_jar_content_hash_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id %}
{% set artifacts_props_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension + "&c=" + classifier %}
{% set artifacts_props_hash = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_md5 + "&c=" + classifier  %}

#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#base conf
{% set name = pillar[environment]['xdc-legacy-api']['component'] %}
{% set home = pillar['base_install_dir'] + "/" + name %}
{% set jar_loc = home + "/" + name + ".jar" %}
{% set environment = salt['grains.get']('environment') %}
{% set host = salt['grains.get']('host') %}
{% set logs = pillar['base_install_dir'] + "/logs/" + name %}

{% set basedir =  pillar['base_install_dir'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_xdc-legacy-api.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar[environment]['xdc-legacy-api']['component'] %}
{% set config_dir = basedir + "/" + pillar[environment]['xdc-legacy-api']['component'] + "/props" %}
{% set healtcheck_port = salt['pillar.get']('xdc-legacy-api:api:port', "9001") %}

#conf
{% set p  = salt['pillar.get']('java', {}) %}
{%- set user = p.get('user', 'java') %}
{%- set group = p.get('group', 'java') %}
{%- set tech_group = p.get('tech_group', '_tech') %}

#templates
{% set tmpl_file_src = salt['pillar.get']('environment:xdc-legacy-api:config:tmpl_file_loc','salt://xanadu-components/xdc-legacy-api/file') %}
