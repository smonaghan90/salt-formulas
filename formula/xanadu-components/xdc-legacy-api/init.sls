{%- import 'xanadu-components/xdc-legacy-api/settings.sls' as xdclegacyapi with context %}
{%- from "xanadu-components/xdc-legacy-api/map.jinja" import xdclegacyapi_map with context %}

include:
  - third-party-components.java

xdc-legacy-api_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdclegacyapi.artifacts_url }}

xdc-legacy-api_version_file:
  file.managed:
    - name: {{ xdclegacyapi.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdclegacyapi.artifact_id }}
        version: {{ xdclegacyapi.version }}
        sha1: {{ xdclegacyapi.hash }}
    - require:
      - health_check: xdc-legacy-api_nexus_available

xdc-legacy-api_stop:
  cmd.wait:
    - name: service xdc-legacy-api stop
    - onlyif: ps -ef | grep xdc-legacy-api | grep -v grep
    - watch:
      - file: xdc-legacy-api_version_file
    - require:
      - file: {{ xdclegacyapi_map.service_script }}

xdc-legacy-api_clean:
  cmd.wait:
    - name: rm -rf {{ xdclegacyapi.home }}
    - watch:
      - file: xdc-legacy-api_version_file
    - require:
      - cmd: xdc-legacy-api_stop

xdc-legacy-api_props:
  file.managed:
    - name: {{ xdclegacyapi.config_dir }}/application-{{ xdclegacyapi.environment }}.properties
    - source: {{ xdclegacyapi.artifacts_props_content_url }}
    - source_hash: {{ xdclegacyapi.artifacts_props_hash }}
    - user: {{ xdclegacyapi.user }}
    - group: {{ xdclegacyapi.group }}
    - makedirs: True
    - mode: 644
    - template: jinja

xdc-legacy-api_install_jar:
  file.managed:
    - name: {{ xdclegacyapi.jar_loc }}
    - source: {{ xdclegacyapi.artifacts_jar_content_url }}
    - source_hash: {{ xdclegacyapi.hash }}
    - user: {{ xdclegacyapi.user }}
    - group: {{ xdclegacyapi.group }}
    - makedirs: True
    - mode: 644
    - require:
      - cmd: xdc-legacy-api_stop

xdc-legacy-api_log_permissions:
  file.directory:
    - name: {{ xdclegacyapi.logs }}
    - user: {{ xdclegacyapi.user }}
    - group: {{ xdclegacyapi.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - file: xdc-legacy-api_install_jar

{%- if xdclegacyapi_map.service_script %}
{{ xdclegacyapi_map.service_script }}:
  file.managed:
    - source: {{ xdclegacyapi.tmpl_file_src }}/{{ xdclegacyapi_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ xdclegacyapi_map.service_script_mode }}
    - template: jinja
    - context:
       jar_loc: {{ xdclegacyapi.jar_loc }}
       config_dir: {{ xdclegacyapi.config_dir }}
       environment: {{ xdclegacyapi.environment }}
       user: {{ xdclegacyapi.user }}
       name: {{ xdclegacyapi.name }}

xdclegacyapi_service:
  service.running:
    - name: xdc-legacy-api
    - sig: 'xdc-legacy-api'
    - require:
      - file: xdc-legacy-api_install_jar
      - file: {{ xdclegacyapi_map.service_script }}
    - watch:
      - file: xdc-legacy-api_props
      - file: xdc-legacy-api_install_jar
      - file: xdc-legacy-api_version_file
{%- endif %}

successful_xdclegacyapi_deployment:
  health_check.wait_for_url:
    - timeout: 10 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ xdclegacyapi.healtcheck_port }}/status
    - match_text: 'Running'
    - require:
      - file: xdc-legacy-api_install_jar
      - file: xdc-legacy-api_props
      - service: xdclegacyapi_service
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "{{ xdclegacyapi.name }} version is {{ xdclegacyapi.version }} and {{ xdclegacyapi.hash }}"

{%- if xdclegacyapi.hipchat_enabled == True %}

xdclegacyapi_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ xdclegacyapi.auth_token }}
    - room_id: {{ xdclegacyapi.room_id }}
    - message: '[{{ xdclegacyapi.environment }}] - Artifact version [{{ xdclegacyapi.version }}] of component [{{ xdclegacyapi.artifact_name }}] was successfully deployed to the [{{ xdclegacyapi.environment }}] environment [{{ xdclegacyapi.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_espcc-xdclegacyapi_deployment
    - onchanges:
      - service: xdclegacyapi_service

xdclegacyapi_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ xdclegacyapi.auth_token }}
    - room_id: {{ xdclegacyapi.room_id }}
    - message: '[{{ xdclegacyapi.environment }}] - Deployment of artifact version [{{ xdclegacyapi.version }}] for component [{{ xdclegacyapi.artifact_name }}] to the [{{ xdclegacyapi.environment }}] environment [{{ xdclegacyapi.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: espcc-xdclegacyapi_hipchat_notifier_deploy_finished

{%- endif %}
