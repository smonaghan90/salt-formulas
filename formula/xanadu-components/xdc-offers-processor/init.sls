{%- import 'xanadu-components/xdc-offers-processor/settings.sls' as xdcoprocessor with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcoprocessor_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcoprocessor.artifacts_url }}

xdcoprocessor_version_file:
  file.managed:
    - name: {{ xdcoprocessor.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcoprocessor.artifact_id }}
        version: {{ xdcoprocessor.version }}
        sha1: {{ xdcoprocessor.hash }}
    - require:
      - health_check: xdcoprocessor_nexus_available

xdcoprocessor_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcoprocessor.basedir }}/{{ xdcoprocessor.artifact_id }}-*

xdcoprocessor_get_war_file:
  file.managed:
    - name: {{ xdcoprocessor.jar_loc }}
    - source: {{ xdcoprocessor.artifacts_war_content_url }}
    - source_hash: {{ xdcoprocessor.hash }}
    - require:
      - cmd: xdcoprocessor_remove_previous_versions

#Upload artifact to HDFS
xdcoprocessor_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcoprocessor.admin_server }}:9393/modules/processor/{{ xdcoprocessor.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcoprocessor.jar_loc }}'
    - watch:
      - file: xdcoprocessor_version_file

#xdcoprocessor_remove_stream:
#  cmd.run:
#    - name:  'curl -s -X DELETE {{ xdcoprocessor.admin_server }}:9393/streams/definitions/{{ xdcoprocessor.stream_name }}'


#xdcoprocessor_create_stream:
#  cmd.run:
#    - name: ' curl -s -X POST {{ xdcoprocessor.admin_server }}:9393/streams/definitions -d name={{ xdcoprocessor.stream_name }} -d "definition= http  --port=9001 | xdc-json-to-object | xdc-offers-processor | xdc-object-to-json |file --name=outfile" -d deploy=true'
#    - watch:
#      - file: xdcoprocessor_version_file
