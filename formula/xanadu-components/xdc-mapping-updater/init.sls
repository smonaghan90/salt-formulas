{%- import 'xanadu-components/xdc-mapping-updater/settings.sls' as xdcmu with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcmu_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcmu.artifacts_url }}

xdcmu_version_file:
  file.managed:
    - name: {{ xdcmu.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcmu.artifact_id }}
        version: {{ xdcmu.version }}
        sha1: {{ xdcmu.hash }}
    - require:
      - health_check: xdcmu_nexus_available

xdcmu_remove_previous_versions:
  cmd.run:
    - name: rm -rf {{ xdcmu.basedir }}/{{ xdcmu.artifact_id }}-*


xdcmu_get_war_file:
  file.managed:
    - name: {{ xdcmu.jar_loc }}
    - source: {{ xdcmu.artifacts_war_content_url }}
    - source_hash: {{ xdcmu.hash }}
    - require:
      - cmd: xdcmu_remove_previous_versions

#Upload artifact to HDFS
xdcmu_upload_module:
  cmd.run:
    - name: 'curl -s -X POST {{ xdcmu.admin_server }}:9393/modules/processor/{{ xdcmu.artifact_id }}?force=true -H "Content-Type: application/octet-stream" --data-binary @{{ xdcmu.jar_loc }}'
    - watch:
      - file: xdcmu_version_file
