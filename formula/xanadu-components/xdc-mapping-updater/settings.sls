#Get Jars from nexus based on version set in pillar
{% set artifacts_url = pillar['component_base_url'] %}
{% set view_api = "service/local/artifact/maven/resolve" %}
{% set group = "com.xanaduconsultancy" %}
{% set environment = salt['grains.get']('environment') %}
{% set artifact_name = pillar[environment]['xdc-mapping-updater']['component']  %}
{% set repo_version  = salt['pillar.get'](environment + ':xdc-mapping-updater:repo_version', "LATEST") %}
{% set repository_id  = salt['pillar.get']('repository_id', "snapshots") %}
{% set extension_props = "properties" %}
{% set extension_war = "jar" %}
{% set classifier = "application" %}
{% set content_api = "service/local/artifact/maven/content" %}

#nexus api


{% set hash = 'sha1=' + salt['cmd.run']( 'curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath \"///sha1/text()\" -' ) %}
{% set version = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///version/text()" -') %}
{% set artifact_id = salt['cmd.run']('curl -s ' + "\"" + artifacts_url + "/" + view_api + '?g=' + group + '&a=' + artifact_name + '&v=' + repo_version + '&r=' + repository_id + "&e=" + extension_war + "\"" + ' | xmllint --xpath "///artifactId/text()" -') %}

{% set artifacts_war_content_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id + "&e=" + extension_war %}
{% set artifacts_war_content_hash_url = artifacts_url + "/" + content_api + "?g=" + group + "&a=" + artifact_name + "&v=" +  repo_version + "&r=" + repository_id %}

{% set stream_name = artifact_id + '-1' %}

{% set basedir =  pillar['base_install_dir'] %}
{% set sha1_file = pillar['install_packages_dir'] + '/xc_version_xdc-mapping-updater.txt' %}
{% set jar_loc = basedir + "/" + artifact_id + '-'+ version +".jar" %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set hdfs_master = pillar['hdfs_master'] %}

{% set admin_server = pillar['admin_server'] %}


#conf
{% set p  = salt['pillar.get']('xdc-mapping-updater', {}) %}
{%- set user = p.get('user', 'java') %}
{%- set group = p.get('group', 'java') %}
{%- set tech_group = p.get('tech_group', '_tech') %}

#templates
{% set tmpl_file_src = salt['pillar.get']('environment:xdc-mapping-updater:config:tmpl_file_loc','salt://xanadu-components/xdc-mapping-updater/file') %}
