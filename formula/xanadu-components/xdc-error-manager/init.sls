{%- import 'xanadu-components/xdc-error-manager/settings.sls' as xdcem with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}

include:
  - third-party-components.springxdcontainer

xdcem_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcem.artifacts_url }}

xdcem_version_file:
  file.managed:
    - name: {{ xdcem.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdcem.artifact_id }}
        version: {{ xdcem.version }}
        sha1: {{ xdcem.hash }}
    - require:
      - health_check: xdcem_nexus_available

xdcem_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdcem.artifact_id }}-*
    - watch:
      - file: xdcem_version_file


xdcem_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdcem.artifact_id }}-{{ xdcem.version }}.jar
    - source: {{ xdcem.artifacts_war_content_url }}
    - source_hash: {{ xdcem.hash }}
    - user: {{ xdcem.user }}
    - group: {{ xdcem.group }}
    - mode: 644
    - watch:
      - file: xdcem_version_file
    - require:
      - cmd: xdcem_remove_previous_versions

xdcem_restart_container:
  cmd.run:
    - name: '/etc/init.d/xd-container restart'
  watch:
    - file: xdccore_version_file
