{%- import 'third-party-components/nginx/settings.sls' as nx with context %}

nginx_pkg:
  pkgrepo.managed:
    - name: {{ nx.debian_packages_url }}
    - require_in:
      - pkg: nginx_install

nginx_install:
  pkg.installed:
    - name: nginx
    - install_recommends: True
    - skip_verify: True

nginx_conf:
  file.managed:
    - name: {{ nx.nginx_conf }}/nginx.conf
    - source: {{ nx.tmpl_src }}/nginx.conf
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
       user : {{ nx.user }}
       logs_dir : {{ nx.logs_dir }}

{% if salt['grains.get']('customer_code') == 'MBK' %}
{%- set flag = True %}

mb_remove_defaults:
  cmd.run:
    - name: rm -r  {{ nx.nginx_conf }}/sites-available/*


mb_desktop_site_conf:
  file.managed:
    - name: {{ nx.nginx_conf }}/sites-available/desktop
    - source: {{ nx.tmpl_files}}/desktop
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
       user : {{ nx.user }}
       logs_dir : {{ nx.logs_dir }}
       host_name: {{ nx.host_name }}
       desktop_port: {{ nx.desktop_port }}
       desktop_assets: {{ nx.desktop_assets }}


mb_mobile_site_enabled_conf:
  file.managed:
    - name: {{ nx.nginx_conf }}/sites-enabled/mobile
    - source: {{ nx.tmpl_files}}/mobile
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
       user : {{ nx.user }}
       logs_dir : {{ nx.logs_dir }}
       host_name: {{ nx.host_name }}
       mobile_port: {{ nx.mobile_port }}
       mobile_assets: {{ nx.mobile_assets }}

mb_desktop_site_enabled_conf:
  file.managed:
    - name: {{ nx.nginx_conf }}/sites-enabled/desktop
    - source: {{ nx.tmpl_files}}/desktop
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
       user : {{ nx.user }}
       logs_dir : {{ nx.logs_dir }}
       host_name: {{ nx.host_name }}
       desktop_port: {{ nx.desktop_port }}
       desktop_assets: {{ nx.desktop_assets }}


mb_mobile_site_conf:
  file.managed:
    - name: {{ nx.nginx_conf }}/sites-available/mobile
    - source: {{ nx.tmpl_files}}/mobile
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
       user : {{ nx.user }}
       logs_dir : {{ nx.logs_dir }}
       host_name: {{ nx.host_name }}
       mobile_port: {{ nx.mobile_port }}
       mobile_assets: {{ nx.mobile_assets }}
{% else %}
{%- set flag = False %}
sites_available_conf:
  file.managed:
    - name: {{ nx.nginx_conf }}/sites-available/default
    - source: {{ nx.tmpl_src }}/sites-available/default
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
        upstream_name : {{ nx.upstream_name }}
        server_port : {{ nx.server_port }}
        upstream_server : {{ nx.upstream_server }}
        server_asset_location: {{ nx.server_asset_location }}
{% endif %}
nginx_log_permissions:
  file.directory:
    - name: {{ nx.logs_dir }}
    - user: root
    - group: {{ nx.tech_group }}
    - makedirs: True
    - dir_mode: 774

nginx_service:
  service.running:
    - name: nginx
    - enable: True
{% if flag %}
    - watch:
      - pkgrepo: nginx_pkg
      - file: mb_desktop_site_conf
      - file: mb_mobile_site_conf
    - require:
      - file: nginx_log_permissions

{% else %}
    - watch:
      - pkgrepo: nginx_pkg
      - file: nginx_conf
      - file: sites_available_conf
    - require:
      - file: nginx_log_permissions
{% endif %}
version:
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "Nginx version is {{ nx.nginx_version }}"
