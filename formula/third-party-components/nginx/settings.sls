#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set debian_packages_url = 'deb ' + artifacts_base_url + '/debian ./' %}



#base conf
{% set nginx_conf = salt['pillar.get']('conf', '/etc/nginx') %}
{% set upstream_name = salt['pillar.get']('config:upstream:name', 'localhost')  %}
{% set upstream_server = salt['pillar.get']('config:upstream:server', 'localhost:8080')  %}
{% set server_port = salt['pillar.get']('config:server:port', '50080')  %}
{% set server_asset_location = salt['pillar.get']('config:server:asset_location', '/usr/share/nginx/html')  %}
{% set logs_dir = pillar['base_log_dir'] + "/nginx" %}
{% set nginx_version = salt['cmd.run']( 'nginx -v') %}

{% set environment = salt['grains.get']('environment')%}
#Custom for matchbook nginx
{% set p  = salt['pillar.get']('nginx_config', {}) %}
{%- set desktop_port = p.get('desktop_port' , '50080' )  %}
{%- set mobile_port = p.get ('mobile_port', '50081') %}
{%- set mobile_assets = p.get('mobile_assets', '/xanadu/mb-mobile') %}
{%- set desktop_assets = p.get('desktop_assets' , '/xanadu/mb-desktop') %}
{%- set host_name = p.get('host_name' , '') %}




#conf
{% set p  = salt['pillar.get']('nginx', {}) %}
{%- set user = p.get('user', 'nginx') %}
{%- set group = p.get('group', 'nginx') %}
{%- set tech_group = p.get('tech_group', '_tech') %}


#templates
{% set tmpl_src = salt['pillar.get']('nginx:config:tmpl_loc','salt://third-party-components/nginx/file') %}
{% set tmpl_files = salt['pillar.get']('nginx_config:tmpl_files','salt://' + environment + '/nginx/files/sites-available') %}
