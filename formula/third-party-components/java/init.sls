{%- import 'third-party-components/java/settings.sls' as jdk with context %}
{%- from "third-party-components/java/map.jinja" import jdk_map with context %}

java_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ jdk.install_package_md5 }}

java_version_file:
  file.managed:
    - name: {{ jdk.md5_file }}
    - makedirs: True
    - show_diff: True
    - mode: 644
    - contents: |
        version: {{ jdk.name }}
        hash: {{ jdk.md5_hash }}
    - require:
      - health_check: java_md5_available
  cmd.wait:
    - name: rm -rf {{ jdk.home }}
    - watch:
      - file: java_version_file

java_home:
  archive.extracted:
    - name: {{ jdk.basedir }}
    - source: {{ jdk.install_package }}
    - source_hash: {{ jdk.install_package_md5 }}
    - archive_format: tar
    - if_missing: {{ jdk.home }}
    - require:
      - health_check: java_md5_available

{%- if jdk_map.profile_localtion %}
set_java_home_env_variable:
  file.managed:
    - name: {{ jdk_map.profile_localtion }}/java-env.sh
    - source: {{ jdk.tmpl_src }}/java-env.sh
    - template: jinja
    - mode: 644
    - require:
      - archive: java_home
{%- endif %}

java_permissions:
  file.directory:
    - name: {{ jdk.home }}
    - user: {{ jdk.user }}
    - group: {{ jdk.group }}
    - recurse:
      - user
      - group
    - require:
      - archive: java_home

java_cacerts_file:
  file.managed:
    - name: {{ jdk.security_home }}/cacerts
    - source: {{ jdk.tmpl_src}}/cacerts
    - makedirs: True
    - show_diff: True
    - mode: 644


java_link:
  file:
    - symlink
    - name: /usr/bin/java
    - target: {{ jdk.home }}/bin/java
    - makedirs: True
    - user: root
    - group: root
    - require:
      - archive: java_home

version:
  get_platform_versions.versions:
    - info_list:
      - dummy
    - alone: True
    - ver_string: "Java version is {{ jdk.version }}"
