{%- import 'third-party-components/java/settings.sls' as jdk with context %}

export JAVA_HOME={{ jdk.home }}
export PATH=$JAVA_HOME/bin:$PATH
