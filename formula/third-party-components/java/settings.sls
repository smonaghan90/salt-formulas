#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}


## Check to see how many xanadu-components have been configured for deployment on the target minion


## Check to see how many xanadu-components have been configured for deployment on the target minion
##Set java version by default jdk 8

{% if salt['grains.get']('java_version') %}
{% set java_version =  salt['grains.get']('java_version') %}
{% else  %}
{% set java_version = pillar['java_config']['name'] %}
{%- endif %}



{% set install_package = artifacts_base_url + "/" + java_version + '.tar.gz' %}
{% set install_package_md5 = artifacts_base_url + "/" + java_version  + '.tar.gz.MD5' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}



#base conf
{% set install_dir = pillar['base_install_dir'] %}
{% set home = install_dir + "/" + java_version %}
{% set security_home = home + "/jre/lib/security" %}
{% set name = java_version %}
{% set basedir =  pillar['base_install_dir'] %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_java.txt' %}
{% set version = salt['cmd.run']( 'cat ' + pillar['install_packages_dir'] + '/xc_version_java.txt') %}

#conf
{% set p  = salt['pillar.get']('java_config', {}) %}
{% set pc = p.get('config', {}) %}
{%- set user = p.get('user', 'java') %}
{%- set group = p.get('group', 'java') %}
{%- set tech_group = p.get('tech_group', '_tech') %}


#remote debugging
{%- set remote_debug_enabled = pc.get('remote_debug_enabled', 'False') %}
{%- set remote_debugging_string = pc.get('remote_debug_string', '-Xdebug -agentlib:jdwp=transport=dt_socket,address=9999,server=y,suspend=n') %}

#templates
{% set tmpl_src = salt['pillar.get']('java_config:config:tmpl_loc','salt://third-party-components/java/file') %}
