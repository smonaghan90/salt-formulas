#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set install_package = artifacts_base_url + "/" + pillar['glassfish_config']['name'] + '.zip' %}
{% set install_package_md5 = artifacts_base_url + "/" + pillar['glassfish_config']['name']  + '.zip.MD5' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}

#base conf
{% set name = pillar['glassfish_config']['name'] %}
{% set basedir =  pillar['base_install_dir'] %}
{%- set customer_code = salt['grains.get']('customer_code') %}

{% if  customer_code == 'mercury' %}
{% set home = salt['pillar.get']('glassfish:config:home',basedir + "/" + 'glassfish3') %}
{% else  %}
{% set home = salt['pillar.get']('glassfish:config:home',basedir + "/" + 'glassfish4') %}
{% endif %}


{% set md5_file = pillar['install_packages_dir'] + '/xc_version_glassfish.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar['glassfish_config']['name'] %}
{% set domain_dir = salt['pillar.get']('glassfish:config:domain_dir',home + "/" + 'glassfish/domains/domain1') %}
{% set domains = salt['pillar.get']('glassfish:config:domain_dir',home + "/" + 'glassfish/domains') %}
{% set config_dir = salt['pillar.get']('glassfish:config:config_dir',domain_dir + "/" + 'config') %}
{% set bin_dir = salt['pillar.get']('glassfish:config:bindir',home + "/" + 'glassfish/bin') %}


#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#conf
{% set p  = salt['pillar.get']('glassfish', {}) %}
{%- set user = p.get('user', 'glassfish') %}
{%- set group = p.get('group', 'glassfish') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set pc = p.get('config', {}) %}

#templates
{% set tmpl_src = salt['pillar.get']('glassfish:config:tmpl_loc','salt://third-party-components/glassfish/file') %}
