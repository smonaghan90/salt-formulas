{%- import 'third-party-components/glassfish/settings.sls' as gf with context %}

export GLASSFISH_HOME={{ gf.home }}
export PATH=$GLASSFISH_HOME/bin:$PATH
