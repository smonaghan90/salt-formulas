#!/bin/bash
GLASSFISH_HOME={{ glassfish_home }}
GLASSFISH_USER={{ glassfish_user }}
RETVAL=0

    case "$1" in
	start)
    	echo "Starting GlassFish";
    	sudo -u $GLASSFISH_USER $GLASSFISH_HOME/glassfish/bin/asadmin start-domain >/dev/null
    	;;
	stop)
    	echo "Stopping GlassFish";
    	sudo -u $GLASSFISH_USER $GLASSFISH_HOME/glassfish/bin/asadmin stop-domain >/dev/null
    	;;
	restart)
    	sudo -u $GLASSFISH_USER $GLASSFISH_HOME/glassfish/bin/asadmin restart-domain >/dev/null
    	;;
    status)
        pid=`ps ax | grep -i 'glassfish.bootstrap.ASMain ' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "Glassfish is Running as PID: $pid"
          RETVAL=0
        else
          echo "Glassfish is not Running"
          RETVAL=3
        fi
        ;;
  	\*)
        echo "usage: $0 (start|stop|restart|status|help)"
esac

exit $RETVAL
