{%- import 'third-party-components/glassfish/settings.sls' as gf with context %}
{%- from "third-party-components/glassfish/map.jinja" import gf_map with context %}


include:
  - third-party-components.java

glassfish_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ gf.install_package_md5 }}

glassfish_version_file:
  file.managed:
    - name: {{ gf.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ gf.name }}
        hash: {{ gf.md5_hash }}
    - require:
      - health_check: glassfish_md5_available
  cmd.wait:
    - name: rm -rf {{ gf.home }}
    - watch:
      - file: glassfish_version_file

glassfish_home:
  archive.extracted:
    - name: {{ gf.basedir }}
    - source: {{ gf.install_package }}
    - source_hash: {{ gf.install_package_md5 }}
    - archive_format: zip
    - if_missing: {{ gf.home }}
    - require:
      - health_check: glassfish_md5_available



{%- if gf_map.profile_localtion %}
set_glassfish_home_env_variable:
  file.managed:
    - name: {{ gf_map.profile_localtion }}/glassfish-env.sh
    - source: {{ gf.tmpl_src }}/glassfish-env.sh
    - template: jinja
    - require:
      - archive: glassfish_home
{%- endif %}


{%- if gf_map.service_script %}
{{ gf_map.service_script }}:
  file.managed:
    - source: {{ gf.tmpl_src }}/{{ gf_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ gf_map.service_script_mode }}
    - template: jinja
    - context:
      glassfish_home: {{ gf.home }}
      glassfish_user: {{ gf.user }}

{% endif %}


glassfish_permissions:
  file.directory:
    - name: {{ gf.home }}
    - user: {{ gf.user }}
    - group: {{ gf.group }}
    - recurse:
      - user
      - group
    - require:
      - file: set_glassfish_home_env_variable

glassfish_bin_permissions:
  file.directory:
    - name: {{ gf.bin_dir }}
    - user: {{ gf.user }}
    - group: {{ gf.group }}
    - mode: 755
    - file_mode: 744
    - recurse:
      - user
      - group
      - mode
    - require:
      - file: glassfish_permissions

glassfish_service:
  service.running:
    - name: glassfish
    - sig: glassfish.bootstrap.ASMain
    - require:
      - file: glassfish_permissions
    - watch:
      - file: set_glassfish_home_env_variable
