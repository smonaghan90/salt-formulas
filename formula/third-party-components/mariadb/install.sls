{%- from "third-party-components/mariadb/map.jinja" import mariadb with context %}
{%- import 'third-party-components/mariadb/settings.sls' as mariadb_settings with context %}


mariadb_settings_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ mariadb_settings.install_package_md5 }}

mariadb_version_file:
  file.managed:
    - name: {{ mariadb_settings.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ mariadb_settings.mariadb_version }}
        hash: {{ mariadb_settings.md5_hash }}
    - require:
      - health_check: mariadb_settings_md5_available

install-mariadb:
  pkg.installed:
    - name: {{ mariadb.server }}

{% if mariadb_settings.os == 'Ubuntu' %}
get-mariadb-repo:
  pkgrepo.managed:
    - humanname: MariaDB Repo
    - name: {{ mariadb_settings.debian_packages_url }}
    - require_in:
      - pkg: install-mariadb
{% endif %}
