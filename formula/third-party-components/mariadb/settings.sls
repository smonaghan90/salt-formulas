#Basic mariadb formula

#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set artifact_name = 'mariadb' %}
{% set mariadb_version = 'mariadb-server-core-10.0_10.0.26+maria-1-trusty_amd64' %}
{% set debian_packages_url = 'deb ' + artifacts_base_url + '/debian ./' %}
{% set install_package_md5 = artifacts_base_url +'/debian/'+ mariadb_version +'.deb.MD5' %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_mariadb.txt' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}


{% set os = salt['grains.get']('os', None) %}
{% set os_family = salt['grains.get']('os_family', None) %}

#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#templates
{% set tmpl_src = salt['pillar.get']('mariadb:config:tmpl_loc','salt://third-party-components/mariadb/files') %}
