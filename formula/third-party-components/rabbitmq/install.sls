{%- import 'third-party-components/rabbitmq/settings.sls' as rmq with context %}
{%- from "third-party-components/rabbitmq/map.jinja" import rmq_map with context %}

{% set module_list = salt['sys.list_modules']() %}


include:
  - third-party-components.java

{% if 'rabbitmqadmin' in module_list %}


include:
  - third-party-components.rabbitmq.config_bindings
  - third-party-components.rabbitmq.config_queue
  - third-party-components.rabbitmq.config_exchange
{% endif %}

rmq_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ rmq.install_package_md5 }}

rabbit_version_file:
  file.managed:
    - name: {{ rmq.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ rmq.rabbitmq_version }}
        hash: {{ rmq.md5_hash }}
    - require:
      - health_check: rmq_md5_available

rabbitmq_home:
  file.directory:
    - name: {{ rmq.home }}
    - user: {{ rmq.user }}
    - group: {{ rmq.group }}
    - makedirs: True
    - dir_mode: 755

{% if grains['osrelease'] == '14.04' %}

rabbitmq_pkg:
  pkgrepo.managed:
    - name: {{ rmq.debian_packages_url }}
    - require_in:
      - pkg: rabbitmq_install

rabbitmq_install:
  pkg.installed:
    - name: {{ rmq_map.pkg }}
    - install_recommends: True
    - skip_verify: True

{% elif grains['osrelease'] != '14.04' %}

rabbitmq_install:
  pkg.installed:
    - name: rabbitmq-server

{% endif %}


{% if 1 == salt['cmd.retcode']('test -f /etc/rabbitmq/rabbitmq-env.conf') %}

rabbit_stew:
  service.dead:
    - name: {{ rmq_map.service }}
    - watch:
      - pkg: rabbitmq_install

rabbit_stew_definitelycooked:
  cmd.wait:
    - name: pkill epmd
    - onlyif: ps ax | grep -i epmd | grep -v grep
    - watch:
      - service: rabbit_stew

rabbitmq.erlang_cookie:
  file.managed:
    - name: {{ rmq_map.cookie_file }}
    - makedirs: True
    - mode: 400
    - user: rabbitmq
    - group: rabbitmq
    - contents: {{ rmq.cluster_cookie }}
    - require:
      - service: rabbit_stew

{% else %}

remove_prev_conf:
  file.absent:
    - name : {{ rmq.rabbit_etc}}/rabbit-env.conf

{% endif %}


rabbit_env_conf:
  file.append:
    - name:  {{ rmq.rabbit_etc}}/rabbitmq-env.conf
    - text: RABBITMQ_NODENAME={{ rmq.rabbit_node_user}}@{{ rmq.host }}
    - require:
      - pkg: rabbitmq_install

erlang-base-hipe_install:
  pkg.installed:
    - name: erlang-base-hipe
    - require:
      - pkg: rabbitmq_install

rabbitmq_binary_tool_env:
  file.symlink:
    - makedirs: True
    - name: /usr/local/bin/rabbitmq-env
    - target: /usr/lib/rabbitmq/bin/rabbitmq-env
    - require:
      - pkg: rabbitmq_install

rabbitmq_binary_tool_plugins:
  file.symlink:
    - makedirs: True
    - name: /usr/local/bin/rabbitmq-plugins
    - target: /usr/lib/rabbitmq/bin/rabbitmq-plugins
    - require:
      - pkg: rabbitmq_install
      - file: rabbitmq_binary_tool_env

{%- if rmq.guest_user_enabled == True %}
rabbitmq_guest_conf:
  file.managed:
    - name: /etc/rabbitmq/rabbitmq.config
    - source: {{ rmq.tmpl_src }}/rabbitmq.config
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
      dummy: you are
{% else %}
rabbitmq_guest_conf:
  file.absent:
    - name: /etc/rabbitmq/rabbitmq.config
{%- endif %}

{%- if rmq.ssl_enabled == True %}

rabbitmq_ssl_cacert:
  file.managed:
    - name: {{ rmq.ssl_path }}/keys/cacert.pem
    - source: {{ rmq.tmpl_src }}/ssl/cacert.pem
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - watch:
      - file: rabbit_version_file


rabbitmq_ssl_cert:
  file.managed:
    - name: {{ rmq.ssl_path }}/keys/cert.pem
    - source: {{ rmq.tmpl_src }}/ssl/cert.pem
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - watch:
      - file: rabbit_version_file


rabbitmq_ssl_key:
  file.managed:
    - name: {{ rmq.ssl_path }}/keys/key.pem
    - source: {{ rmq.tmpl_src }}/ssl/key.pem
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
      ssl_path: {{ rmq.ssl_path }}
      ssl_port: {{ rmq.ssl_port }}
    - watch:
      - file: rabbit_version_file

rabbitmq_ssl_config:
  file.managed:
    - name: /etc/rabbitmq/rabbitmq.config
    - source: {{ rmq.tmpl_src }}/ssl/rabbitmq.config
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - backup: minion
    - template: jinja
    - context:
      ssl_path: {{ rmq.ssl_path }}
      ssl_port: {{ rmq.ssl_port }}
    - watch:
      - file: rabbit_version_file

{%- endif %}


rabbitmq_service:
  service.running:
    - name: {{ rmq_map.service }}
    - enable: True
    - watch:
      - pkg: rabbitmq_install
      - file: /etc/rabbitmq/rabbitmq.config

successful_rabbitmq_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ rmq.healtcheck_port }}
    - match_text: "RabbitMQ Management"
    - require:
      - sls: third-party-components.rabbitmq.config
      - service: rabbitmq_service
  get_platform_versions.versions:
    - info_list:
        - dummy
    - alone: True
    - ver_string: "{{ rmq.artifact_name }} version is {{ rmq.version }}"
    - watch:
      - file: rabbit_version_file

{%- if rmq.hipchat_enabled == True %}

rabbitmq_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ rmq.auth_token }}
    - room_id: {{ rmq.room_id }}
    - message: '[{{ rmq.environment }}] - Artifact version [{{ rmq.version }}] of component [{{ rmq.artifact_name }}] was successfully deployed to the [{{ rmq.environment }}] environment [{{ rmq.host }}].'
    - html: True
    - message_color: green
    - notify: True
    - require:
      - health_check: successful_rabbitmq_deployment
    - onchanges:
      - service: rabbitmq_service

rabbitmq_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ rmq.auth_token }}
    - room_id: {{ rmq.room_id }}
    - message: '[{{ rmq.environment }}] - Deployment of artifact version [{{ rmq.version }}] for component [{{ rmq.artifact_name }}] to the [{{ rmq.environment }}] environment [{{ rmq.host }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: rabbitmq_hipchat_notifier_deploy_finished

{%- endif %}
