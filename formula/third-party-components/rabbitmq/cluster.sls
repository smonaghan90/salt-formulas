{%- import 'third-party-components/rabbitmq/settings.sls' as rmq with context %}
{%- from "third-party-components/rabbitmq/map.jinja" import rmq_map with context %}

{%- if rmq.cluster_enabled %}
{%- if salt['cmd.run']('cat '+rmq_map.cookie_file) != (''+rmq.cluster_cookie) %}

{% set cluster_join_host = salt['pillar.get']('rabbitmq_config:cluster:host', 'x.x.x.x') %}

{% if cluster_join_host != grains['host'] %}
 
rabbitmq_stop_app:
  cmd.run:
    - name: rabbitmqctl stop_app
    - require: 
      - service: rabbitmq_service
    
rabbit@{{ cluster_join_host }}:
  rabbitmq_cluster.join:
    - user: {{ rmq.rabbit_node_user }}
    - host: {{ cluster_join_host }}
    - require:
      - cmd: rabbitmq_stop_app
    - require_in:
      - cmd: rabbitmq_start_app

rabbitmq_start_app:
  cmd.run:
    - name: rabbitmqctl start_app
    - require: 
      - service: rabbitmq_service
{% endif %}

{% endif %}
{% endif %}