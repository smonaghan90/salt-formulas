#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set artifact_name = 'rabbitmq' %}
{% set rabbitmq_version = 'rabbitmq-server_3.5.4-1_all' %}
{% set debian_packages_url = 'deb ' + artifacts_base_url + '/debian ./' %}
{% set install_package_md5 = artifacts_base_url +'/debian/'+ rabbitmq_version +'.deb.MD5' %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_rabbitmq.txt' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}


#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#slack api
{% set slack_enabled  = salt['pillar.get']('slack:enabled', "False") %}
{% set slack_auth_token = salt['pillar.get']('slack:auth_token', "xxxxxxx") %}
{% set slack_channel = salt['pillar.get']('slack:channel', "dummychannel") %}
{% set from = salt['pillar.get']('slack:from', "SaltStack") %}

{% set guest_user_enabled = salt['pillar.get']('rabbitmq_config:rabbitmq_ac_config:guest_enabled', 'True') %}
{% set ssl_enabled = salt['pillar.get']('rabbitmq_config:ssl:enabled', 'True') %}
{% set ssl_path = salt['pillar.get']('rabbitmq_config:ssl:path', '') %}
{% set ssl_port = salt['pillar.get']('rabbitmq_config:ssl:port', '5672') %}

#base conf
{% set environment = salt['grains.get']('environment') %}
{% set version = pillar['rabbitmq_config']['version'] %}
{% set guest_user_enabled = salt['pillar.get']('rabbitmq_config:rabbitmq_ac_config:guest_enabled', 'True') %}
{% set host = salt['grains.get']('host') %}
{% set hostname = salt['grains.get']('fqdn') %}

{% set healtcheck_port = salt['pillar.get']('rabbitmq:healtcheck_port', "15672") %}
{% set home = "/home/rabbitmq" %}
{% set rabbit_etc = "/etc/rabbitmq"%}
{% set etc = "/etc"%}


#RABBIT cluster
{% set cluster_enabled =   salt['pillar.get']('rabbitmq_config:cluster:enabled','False') %}
{% set cluster_cookie  =   salt['pillar.get']('rabbitmq_config:cluster:cookie','TESTCOOOKIE') %}
{% set rabbitmq_master =   salt['pillar.get']('rabbitmq_config:rabbitmq_master','default') %}
{% set rabbit_node_user =   salt['pillar.get']('rabbitmq_config:cluster:user','rabbit') %}


#conf
{% set p  = salt['pillar.get']('rabbitmq', {}) %}
{%- set user = p.get('user', 'rabbitmq') %}
{%- set group = p.get('group', 'rabbitmq') %}
{%- set tech_group = p.get('tech_group', '_tech') %}

#file
{% set tmpl_src = salt['pillar.get']('rabbitmq:tmpl_loc','salt://third-party-components/rabbitmq/file') %}
