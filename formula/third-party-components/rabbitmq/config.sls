{%- import 'third-party-components/rabbitmq/settings.sls' as rmq with context %}

/etc/default/rabbitmq-server:
  file.managed:
    - user: root
    - group: root

rabbit_sysctl:
  file.append:
    - name:  {{ rmq.etc}}/sysctl.conf
    - source: {{ rmq.tmpl_src }}/sysctl.conf
    - require_in:
      - service: rabbitmq_service

rabbit_apply_ctl_changes:
  cmd.wait:
    - name: sysctl -p {{ rmq.etc}}/sysctl.conf
    - watch:
      - file:  rabbit_sysctl
    - require_in:
      - service: rabbitmq_service

# Increase the allowed maximum number of open files
rabbitmq_ulimit:
  file.blockreplace:
    - name: /etc/default/rabbitmq-server
    - marker_start: "# START salt managed zone"
    - marker_end: "# END salt managed zone"
    - content: |
        ulimit -n {{ salt['pillar.get']('rabbitmq_config:ulimit:size', "1024") }}
    - append_if_not_found: True
    - backup: '.bak'
    - show_changes: True
    - require_in:
      - service: rabbitmq_service


{% for name, plugin in salt["pillar.get"]("rabbitmq_config:plugin", {}).iteritems() %}
{{ name }}:
  rabbitmq_plugin:
    {% for value in plugin %}
    - {{ value }}
    {% endfor %}
    - runas: root
    - require:
      - pkg: rabbitmq-server
      - file: rabbitmq_binary_tool_plugins
    - watch_in:
      - service: rabbitmq_service
{% endfor %}

{% for name, policy in salt["pillar.get"]("rabbitmq_config:policy", {}).iteritems() %}
{{ name }}:
  rabbitmq_policy.present:
    {% for value in policy %}
    - {{ value }}
    {% endfor %}
    - require:
      - service: rabbitmq_service
{% endfor %}

# need to create users and then vhosts

{% for name, user in salt["pillar.get"]("rabbitmq_config:user", {}).iteritems() %}
rabbitmq_user_{{ name }}:
  rabbitmq_user.present:
    - name: {{ name }}
    {% for value in user %}
    - {{ value }}
    {% endfor %}
    - require:
      - service: rabbitmq_service
{% endfor %}

{% for name, policy in salt["pillar.get"]("rabbitmq_config:vhost", {}).iteritems() %}
rabbitmq_vhost_{{ name }}:
  rabbitmq_vhost.present:
    - name: {{ name }}
    {% for value in policy %}
    - {{ value }}
    {% endfor %}
    - require:
      - service: rabbitmq_service
{% endfor %}
