#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set debian_packages_url = 'deb ' + artifacts_base_url + '/debian ./' %}



#base conf
{% set fluentd_conf = salt['pillar.get']('conf', '/etc/td-agent') %}
{% set logs_dir = pillar['base_log_dir'] + "/fluentd" %}
{% set fluentd_version = salt['cmd.run']( 'td-agent -v') %}

{% set environment = salt['grains.get']('environment')%}
#Custom for matchbook fluentd
{% set p  = salt['pillar.get']('fluentd_config', {}) %}
{%- set desktop_port = p.get('desktop_port' , '50080' )  %}
{%- set host_name = p.get('host_name' , '') %}




#conf
{% set p  = salt['pillar.get']('fluentd', {}) %}
{%- set user = p.get('user', 'fluentd') %}
{%- set group = p.get('group', 'fluentd') %}
{%- set tech_group = p.get('tech_group', '_tech') %}


#templates
{% set tmpl_src = salt['pillar.get']('fluentd:config:tmpl_loc','salt://third-party-components/fluentd/file') %}
{% set tmpl_files = salt['pillar.get']('fluentd_config:tmpl_files','salt://' + environment + '/fluentd/files/sites-available') %}
