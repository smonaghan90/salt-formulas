td-agent-install:
  cmd.run:
    - name: curl -L https://toolbelt.treasuredata.com/sh/install-debian-jessie-td-agent2.sh | sh
    - cwd: /
    - stateful: False