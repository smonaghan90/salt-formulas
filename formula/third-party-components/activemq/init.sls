{%- import 'third-party-components/activemq/settings.sls' as amq with context %}
{%- from "third-party-components/activemq/map.jinja" import amq_map with context %}


include:
  - third-party-components.java

activemq_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ amq.install_package_md5 }}

activemq_version_file:
  file.managed:
    - name: {{ amq.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ amq.name }}
        hash: {{ amq.md5_hash }}
    - require:
      - health_check: activemq_md5_available


activemq_home:
  cmd.wait:
    - name: |
        wget -NqO- {{ amq.install_package }} | tar xvz -C {{ amq.basedir }}
    - require:
      - health_check: activemq_md5_available
    - watch:
      - file: activemq_version_file

activemq_xml:
  file.managed:
    - name: {{ amq.config_dir }}/activemq.xml
    - source: {{ amq.tmpl_src }}/activemq.xml
    - user: {{ amq.user }}
    - group: {{ amq.group }}
    - mode: 644
    - template: jinja
    - require:
      - cmd: activemq_home
    - context:
      bind_address: {{ amq.bind_address }}
      activemq_port: {{ amq.activemq_port }}
      activemq_stomp_port: {{ amq.activemq_stomp_port }}
      openwire_port: {{ amq.openwire_port }}
      amqp_port: {{ amq.amqp_port }}
      mqtt_port: {{ amq.mqtt_port }}
      ws_port: {{ amq.ws_port }}
      broker_name: {{ amq.broker_name }}
      broker_id: {{ amq.broker_id }}


activemq_executable:
  file.managed:
    - name: {{ amq.bin_dir }}/activemq
    - source: {{ amq.tmpl_src }}/activemq
    - user: {{ amq.user }}
    - group: {{ amq.group }}
    - mode: 644
    - template: jinja
    - require:
      - cmd: activemq_home
    - context:
      xms: {{ amq.xms }}
      xmx: {{ amq.xmx }}




{%- if amq_map.service_script %}
{{ amq_map.service_script }}:
  file.managed:
    - source: {{ amq.tmpl_src }}/{{ amq_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ amq_map.service_script_mode }}
    - template: jinja
    - context:
      activemq_home: {{ amq.home }}
      activemq_user: {{ amq.user }}

{% endif %}

activemq_permissions:
  file.directory:
    - name: {{ amq.home }}
    - user: {{ amq.user }}
    - group: {{ amq.group }}
    - recurse:
      - user
      - group

activemq_bin_permissions:
  file.directory:
    - name: {{ amq.bin_dir }}
    - user: {{ amq.user }}
    - group: {{ amq.group }}
    - mode: 755
    - file_mode: 744
    - recurse:
      - user
      - group
      - mode
    - require:
      - file: activemq_permissions

activemq_service:
  service.running:
    - name: activemq
    - sig: 'apache.activemq.transport.nio'
    - require:
      - file: activemq_permissions
    - watch:
      - file: activemq_xml
