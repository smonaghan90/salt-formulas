#!/bin/bash
ACTIVEMQ_HOME={{ activemq_home }}
ACTIVEMQ_USER={{ activemq_user }}
RETVAL=0

    case "$1" in
	start)
    	echo "Starting ACTIVEMQ";
    	sudo -u $ACTIVEMQ_USER $ACTIVEMQ_HOME/bin/activemq start >/dev/null
    	;;
	stop)
    	echo "Stopping ACTIVEMQ";
    	sudo -u $ACTIVEMQ_USER $ACTIVEMQ_HOME/bin/activemq stop >/dev/null
    	;;
	restart)
    	sudo -u $ACTIVEMQ_USER $ACTIVEMQ_HOME/bin/activemq restart >/dev/null
    	;;
    status)
        pid=`ps ax | grep -i 'apache.activemq.transport.nio' | grep -v sh | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "ACTIVEMQ is Running as PID: $pid"
          RETVAL=0
        else
          echo "ACTIVEMQ is not Running"
          RETVAL=3
        fi
        ;;
  	\*)
        echo "usage: $0 (start|stop|restart|status|help)"
esac

exit $RETVAL
