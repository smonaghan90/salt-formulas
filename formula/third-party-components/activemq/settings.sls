#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set install_package = artifacts_base_url + "/" + pillar['activemq_config']['name'] + '-bin.tar.gz' %}
{% set install_package_md5 = artifacts_base_url + "/" + pillar['activemq_config']['name']  + '-bin.tar.gz.MD5' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}

#base conf
{% set name = pillar['activemq_config']['name'] %}
{% set basedir =  pillar['base_install_dir'] %}
{% set home = salt['pillar.get']('activemq_config:home',basedir + "/" + name ) %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_activemq.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar['activemq_config']['name'] %}
{% set config_dir = salt['pillar.get']('activemq_config:config_dir',home + "/" + 'conf') %}
{% set bin_dir = salt['pillar.get']('activemq_config:bindir',home + '/bin') %}


{% set bind_address = salt['pillar.get']('activemq_config:bind_address','') %}
{% set activemq_port = salt['pillar.get']('activemq_config:activemq_port','') %}
{% set activemq_stomp_port = salt['pillar.get']('activemq_config:activemq_stomp_port','') %}


{% set openwire_port = salt['pillar.get']('activemq_config:openwire_port','') %}
{% set amqp_port = salt['pillar.get']('activemq_config:amqp_port','') %}
{% set mqtt_port = salt['pillar.get']('activemq_config:mqtt_port','') %}
{% set ws_port = salt['pillar.get']('activemq_config:ws_port','') %}


{% set memory_usage_limit = salt['pillar.get']('activemq_config:memory_usage_limit','800 mb') %}
{% set store_usage_limit = salt['pillar.get']('activemq_config:store_usage_limit','15 gb') %}

{% set xms = salt['pillar.get']('activemq_config:xms','1G') %}
{% set xmx = salt['pillar.get']('activemq_config:xmx','1G') %}

{% set broker_name = salt['pillar.get']('activemq_config:broker_name','1G') %}
{% set broker_id = salt['pillar.get']('activemq_config:broker_id','1G') %}

#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

#conf
{% set p  = salt['pillar.get']('amq', {}) %}
{%- set user = p.get('user', 'amq') %}
{%- set group = p.get('group', 'amq') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set pc = p.get('config', {}) %}

#templates
{% set tmpl_src = salt['pillar.get']('activemq_config:tmpl_loc','salt://third-party-components/activemq/file') %}
