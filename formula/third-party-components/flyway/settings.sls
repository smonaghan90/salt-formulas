#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set install_package = artifacts_base_url + "/" + pillar['flyway_config']['name'] + '.tar.gz' %}
{% set install_package_md5 = artifacts_base_url + "/" + pillar['flyway_config']['name']  + '.tar.gz.MD5' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}

#base conf
{% set name = pillar['flyway_config']['name'] %}
{% set basedir =  pillar['base_install_dir'] %}
{% set home = salt['pillar.get']('flyway:config:home',basedir + "/" + 'flyway-4.0.3') %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_glassfish.txt' %}


#conf
{% set p  = salt['pillar.get']('flyway', {}) %}
{%- set user = p.get('user', 'flyway') %}
{%- set group = p.get('group', 'flyway') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set pc = p.get('config', {}) %}

#templates
{% set tmpl_src = salt['pillar.get']('flyway:config:tmpl_loc','salt://third-party-components/flyway/file') %}
