{%- import 'third-party-components/flyway/settings.sls' as flyway with context %}
{%- import 'xanadu-components/xdc1-mariadb-config/settings.sls' as mariadb with context %}

include:
  - third-party-components.java

flyway_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ flyway.install_package_md5 }}

flyway_version_file:
  file.managed:
    - name: {{ flyway.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ flyway.name }}
        hash: {{ flyway.md5_hash }}
    - require:
      - health_check: flyway_md5_available

flyway_home:
  archive.extracted:
    - name: {{ flyway.basedir }}
    - source: {{ flyway.install_package }}
    - source_hash: {{ flyway.install_package_md5 }}
    - archive_format: tar
    - if_missing: {{ flyway.home }}
    - require:
      - health_check: flyway_md5_available

flyway_config_file:
  file.managed:
    - name:  {{ flyway.basedir }}/{{ flyway.name}}/conf/flyway.conf
    - source: {{ flyway.tmpl_src }}/flyway.conf
    - makedirs: True
    - show_diff: True
    - template: jinja
    - context:
      home: {{ flyway.home }}
      db_user: {{ mariadb.db_user }}
      db_password: {{ mariadb.db_password }}
      db_name: {{ mariadb.db_name }}
      db_port: {{ mariadb.db_port }}
    - watch:
      - file: flyway_version_file


flyway_tar_remove:
  cmd.run:
    - name: rm -rf {{ flyway.name }}*.gz
