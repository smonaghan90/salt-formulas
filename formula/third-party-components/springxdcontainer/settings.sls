#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set install_package = artifacts_base_url + "/" + pillar['springxdcontainer_config']['name'] + '.zip'  %}
{% set install_package_md5 = artifacts_base_url + "/" + pillar['springxdcontainer_config']['name'] + '.zip.MD5' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}
{%- set customer_code = salt['grains.get']('customer_code') %}




{% set jars_base_url = pillar['certified_platform_base_url'] + "/certified_jars/"  + pillar['certified_platform'] %}
{% set jar_package = jars_base_url + "/certified_jars.tar.gz"  %}
{% set jar_package_md5 = jars_base_url + "/certified_jars.tgz.MD5" %}


{%- set md5_jar_hash = salt['xanadu_utils.get_md5'](jar_package_md5) %}

#base conf
{% set install_dir = pillar['base_install_dir'] %}
{% set name = pillar['springxdcontainer_config']['name'] %}
{% set basedir =  pillar['base_install_dir'] %}
{% set home = basedir + "/" + pillar['springxdcontainer_config']['name']  %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_springxd.txt' %}
{% set md5_jar_file = pillar['install_packages_dir'] + '/xc_version_jarxd.txt' %}
{% set hsqldb_bin = basedir + "/" + pillar['springxdcontainer_config']['name'] + "/hsqldb/bin" %}
{% set xd_bin = basedir + "/" + pillar['springxdcontainer_config']['name'] + "/xd/bin" %}
{% set number_of_admin_servers =  pillar['springxdcontainer_config']['number_of_admin_servers']  %}
{% set adminServers =  pillar['springxdcontainer_config']['adminServers']  %}
{% set adminMemory =  pillar['springxdcontainer_config']['adminMemory']  %}
{% set containers =  pillar['springxdcontainer_config']['containers']  %}
{% set containerMemory =  pillar['springxdcontainer_config']['containerMemory']  %}

{% set xmx =  pillar['springxdcontainer_config']['xmx']  %}
{% set xms =  pillar['springxdcontainer_config']['xms']  %}
{% set log_level =  pillar['springxdcontainer_config']['log_level']  %}

{% set port =  pillar['springxdcontainer_config']['port']  %}
{% set xd_lib = basedir + "/" + pillar['springxdcontainer_config']['name'] + "/xd/lib/" %}


#HDFS path
{% set environment = salt['grains.get']('environment') %}
{% set hdfs_master = pillar['hdfs_master']  %}
{% set hdfs_module_path = pillar['hdfs_module_path']  %}
{% set maria_db = pillar['maria_db'] %}

#conf
{% set p  = salt['pillar.get']('users:java_info', {}) %}
{%- set user = p.get('user', 'java') %}
{%- set group = p.get('group', '_tech') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar['springxdcontainer_config']['name'] %}
{% set config_dir = basedir + "/" + pillar['springxdcontainer_config']['name'] + "/xd/config" %}


#Fetch kafka cluster
{% set pk  = salt['pillar.get']('kafka_config', {}) %}
{% set cluster = pillar['kafka_config']['cluster']['nodes']  %}

{% set p  = salt['pillar.get']('springxdcontainer_config', {}) %}


{% set rp  = salt['pillar.get']('redis_config', {}) %}
{% set pcc = rp.get('cluster', {}) %}
{%- set master = pcc.get('master', '127.0.0.1') %}
{%- set sentinels = pcc.get('sentinels', '127.0.0.1') %}

#templates
{% set tmpl_src = salt['pillar.get']('springxdcontainer:config:tmpl_loc','salt://third-party-components/springxdcontainer/file') %}
