{%- import 'xanadu-components/xdc-core/settings.sls' as xdccore with context %}
{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}



xdccore_nexus_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdccore.artifacts_url }}

xdccore_version_file:
  file.managed:
    - name: {{ xdccore.sha1_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        component: {{ xdccore.artifact_id }}
        version: {{ xdccore.version }}
        sha1: {{ xdccore.hash }}
    - require:
      - health_check: xdccore_nexus_available

xdccore_remove_previous_versions:
  cmd.wait:
    - name: rm -rf {{ xdcontainer.xd_lib }}{{ xdccore.artifact_id }}-*
    - watch:
      - file: xdccore_version_file


xdccore_install_war:
  file.managed:
    - name: {{ xdcontainer.xd_lib }}{{ xdccore.artifact_id }}-{{ xdccore.version }}.jar
    - source: {{ xdccore.artifacts_war_content_url }}
    - source_hash: {{ xdccore.hash }}
    - user: {{ xdccore.user }}
    - group: {{ xdccore.group }}
    - mode: 644
    - watch:
      - file: xdccore_version_file
    - require:
      - cmd: xdccore_remove_previous_versions
