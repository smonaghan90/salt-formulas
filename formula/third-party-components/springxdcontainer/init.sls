{%- import 'third-party-components/springxdcontainer/settings.sls' as xdcontainer with context %}
{%- import 'third-party-components/kafka/settings.sls' as kfa with context %}
{%- from "third-party-components/springxdcontainer/map.jinja" import xd_map with context %}


include:
  - third-party-components.java
  {% if xdcontainer.customer_code != 'xdc1' %}
  - xanadu-components.xdc-core
  - xanadu-components.xdc-error-handler
  - xanadu-components.xdc-error-manager
  - xanadu-components.xdc-integration-module-common
  {% endif %}

xd_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcontainer.install_package_md5 }}


xd_jar_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xdcontainer.jar_package_md5 }}

springxdcontainer_jar_version_file:
  file.managed:
    - name: {{ xdcontainer.md5_jar_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ xdcontainer.name }}
        hash: {{ xdcontainer.md5_jar_hash }}
    - require:
      - health_check: xd_jar_md5_available


springxdcontainer_version_file_version_file:
  file.managed:
    - name: {{ xdcontainer.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ xdcontainer.name }}
        hash: {{ xdcontainer.md5_hash }}
    - require:
      - health_check: xd_md5_available


xd_home:
  archive.extracted:
    - name: {{ xdcontainer.basedir }}
    - source: {{ xdcontainer.install_package }}
    - source_hash: {{ xdcontainer.install_package_md5 }}
    - archive_format: zip
    - if_missing: {{ xdcontainer.home }}
    - require:
      - health_check: xd_md5_available
    - watch:
      - file: springxdcontainer_version_file_version_file


xd_download_extra_jars:
  cmd.wait:
    - names:
        - wget -NqO- {{ xdcontainer.jar_package }} | tar xvz -C {{ xdcontainer.xd_lib }}
        - service xd-container restart
    - require:
      - health_check: xd_jar_md5_available
    - watch:
      - file: springxdcontainer_jar_version_file

xd_container_file:
  file.managed:
    - source: {{ xdcontainer.tmpl_src }}/xd-container
    - name: {{ xdcontainer.xd_bin }}/xd-container
    - group: {{ xdcontainer.group }}
    - template: jinja
    - context:
      xmx: {{ xdcontainer.xmx }}
      xms: {{ xdcontainer.xms }}
      environment: {{ xdcontainer.environment}}
    - require:
      - archive: xd_home


xd_permissions:
  file.directory:
    - name: {{ xdcontainer.home }}
    - user: {{ xdcontainer.user }}
    - group: {{ xdcontainer.group }}
    - makedirs: True
    - recurse:
      - user
      - group
    - require:
      - archive: xd_home

xd_script_permissions:
  file.directory:
    - name: {{ xdcontainer.xd_bin }}
    - user: {{ xdcontainer.user }}
    - group: {{ xdcontainer.group }}
    - makedirs: True
    - mode: 755
    - recurse:
      - user
      - group
      - mode
    - require:
        - archive: xd_home
#Spring XD hsqldb_bin
{%- if xd_map.hsql_service_script %}
{{ xd_map.hsql_service_script }}:
  file.managed:
    - source: {{ xdcontainer.tmpl_src }}/{{ xd_map.hsql_service_script_source }}
    - user: root
    - group: root
    - mode: {{ xd_map.hsql_service_script_mode }}
    - template: jinja
    - context:
      xd_home: {{ xdcontainer.home }}
      xd_user: {{ xdcontainer.user }}
      logs_dir: {{ xdcontainer.logs_dir }}

#Set [permission on bin files]
{{ xdcontainer.hsqldb_bin }}:
  file.directory:
    - user: {{ xdcontainer.user }}
    - group: {{ xdcontainer.group }}
    - mode: 744
    - makedirs: True
    - recurse:
      - user
      - group
      - mode
    - require:
        - archive: xd_home
{%- endif %}

#Springxd admin servers
xd_container_setup_servers:
  file.managed:
    - source: {{ xdcontainer.tmpl_src }}/xdc1-server.yml
    - name:  {{ xdcontainer.config_dir}}/servers.yml
    - user: {{ xdcontainer.user }}
    - group: {{ xdcontainer.group }}
    - mode: 644
    - template: jinja
    - context:
      master: {{ xdcontainer.master }}
      sentinels: {{ xdcontainer.sentinels }}
      kafka_cluster: {{ xdcontainer.cluster }}
      zookeeper_str: {{ kfa.zookeepers }}
      number_of_admin_servers: {{ xdcontainer.number_of_admin_servers }}
      adminServers: {{ xdcontainer.adminServers }}
      adminMemory: {{ xdcontainer.adminMemory }}
      containers: {{ xdcontainer.containers }}
      containerMemory: {{ xdcontainer.containerMemory }}
      hdfs_master: {{ xdcontainer.hdfs_master }}
      hdfs_module_path: {{ xdcontainer.hdfs_module_path }}
      environment: {{ xdcontainer.environment}}
      maria_db: {{ xdcontainer.maria_db }}
      port: {{ xdcontainer.port }}
    - require:
        - archive: xd_home
    - watch:
      - file: springxdcontainer_version_file_version_file


xd_container_logback_properties:
  file.managed:
    - source: {{ xdcontainer.tmpl_src }}/xd-container-logback.groovy
    - name:  {{ xdcontainer.config_dir}}/xd-container-logback.groovy
    - user: {{ xdcontainer.user }}
    - group: {{ xdcontainer.group }}
    - mode: 644
    - template: jinja
    - context:
      log_level: {{ xdcontainer.log_level }}
    - watch:
      - file: springxdcontainer_version_file_version_file




#xd_admin service
{%- if xd_map.xd_container_service_script %}
{{ xd_map.xd_container_service_script }}:
  file.managed:
    - source: {{ xdcontainer.tmpl_src }}/{{ xd_map.xd_container_service_script_source }}
    - user: root
    - group: root
    - mode: {{ xd_map.xd_container_service_script_mode }}
    - template: jinja
    - context:
      containers: {{ xdcontainer.containers }}
      xd_home: {{ xdcontainer.home }}
      xd_user: {{ xdcontainer.user }}
      logs_dir: {{ xdcontainer.logs_dir }}

{%- endif %}

#Check if hsqldb service is running watch config files
hsqldb_service:
  service.running:
    - enable: True
    - reload: True
    - name: hsqldb
    - sig: hsqldb-server
    - watch:
      - file: {{ xd_map.hsql_service_script }}


xd-container_service:
  service.running:
    - enable: True
    - name: xd-container
    - sig: spring.application.name=container
    - watch:
      - file: {{ xd_map.xd_container_service_script }}
      - file: xd_container_setup_servers
    - require_in:
      - service: hsqldb_service
