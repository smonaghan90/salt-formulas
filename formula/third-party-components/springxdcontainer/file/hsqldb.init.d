HSQL_BIN={{ xd_home }}/hsqldb
XD_USER={{ xd_user }}
XD_PATH=$HSQL_BIN/bin
HSQLDB_SERVER=hsqldb-server

RUNNING=$(ps aux | grep hsqldb-server | grep -v grep | wc -l)

RETVAL=0

case $1 in
  start)

        if [  "$RUNNING" -eq 0 ]
          then
          echo "Starting $HSQLDB_SERVER";
          sudo -u $XD_USER nohup $XD_PATH/$HSQLDB_SERVER   > /dev/null 2>&1 &
        else
          echo "HSQLDB already running"
        fi
        ;;
  stop)
        # Stop daemons.
        echo "Shutting down $HSQLDB_SERVER";
        for pid in `ps ax | grep -i 'hsqldb' | grep -v grep | awk '{print $1}'`
        do
         kill -9 $pid ;
        done
        if [ "$pid" ]
         then
          echo "hsqldb-server was not Running"
        fi
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep hsqldb | grep -iv sh | grep -v grep | awk {'print $1'}`
        if [ -n "$pid" ]
          then
          echo "hsqldb is Running as PID: $pid"
          RETVAL=0
        else
          echo "hsqldb-server is not Running"
          RETVAL=3
        fi

        echo "Return Code: $RETVAL"
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac
exit $RETVAL
