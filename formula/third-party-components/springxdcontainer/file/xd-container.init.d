XD_BIN={{ xd_home }}/xd
XD_USER={{ xd_user }}
XD_PATH=$XD_BIN/bin
XD_CONTAINER=xd-container
RETVAL=0

case $1 in
  start)
        # Start daemon.
        echo "Starting $XD_CONTAINER";
        pid=`ps ax | grep -i 'pring.application.name=container' | grep -v grep | awk {'print $1'}`
        if [ -n "$pid" ]
          then
          echo "xd-container is already Running as PID: $pid"
          RETVAL=0
        else
          echo "xd-container Starting"
          {% for containers in range(containers) %}
          echo "xd-container Starting # {{ containers }}"
          sudo -u $XD_USER nohup $XD_PATH/$XD_CONTAINER {{ containers }}  > /dev/null 2>&1 &
          {% endfor %}
        fi
        ;;
  stop)
        # Stop daemons.
        echo "Shutting down $XD_CONTAINER";
        pid=`ps ax | grep -i 'pring.application.name=container' | grep -v grep | awk {'print $1'}`
        if [ -n "$pid" ]
          then
          for ids in $pid; do kill -9 $ids; done
        else
          echo "xd-container was not Running"
        fi
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'pring.application.name=container' | grep -v grep | awk {'print $1'}`
        if [ -n "$pid" ]
          then
          for ids in $pid;do echo "xd-container is Running as PIDS: $ids"; done
          RETVAL=0
        else
          echo "xd-container is not Running"
          RETVAL=3
        fi

        echo "Return Code: $RETVAL"
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac
exit $RETVAL
