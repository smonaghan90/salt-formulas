{%- import 'third-party-components/elk/settings.sls' as elk with context %}
{%- import 'third-party-components/tomcat/settings.sls' as tc with context %}


debian_pkg:
  pkgrepo.managed:
    - name: {{ elk.debian_packages_url }}
    - require_in:
      - pkg: logstash_install
      - pkg: topbeat_install
      
stop_topbeat:
  cmd.run:
    - name: tmux new-session -d "sudo service topbeat stop" && sleep 4

logstash_install:
  pkg.installed:
    - name: logstash
    - install_recommends: True
    - skip_verify: True

logstash_conf:
  file.managed:
    - name: {{ elk.logstash_conf }}
    - source: {{ elk.tmpl_src }}/55-logstash.conf
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - pkg: logstash_install

logstash_tomcat_conf:
  file.managed:
    - name: {{ elk.tomcat_conf }}
    - source: {{ elk.tmpl_src }}/tomcat.conf
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      logs_dir: {{ tc.logs_dir }}
    - require:
      - pkg: logstash_install

syslog_conf:
  file.managed:
    - name: {{ elk.syslog_conf }}
    - source: {{ elk.tmpl_src}}/syslog.conf
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - require:
      - pkg: logstash_install
      
      
topbeat_conf:
   file.managed:
    - name: {{ elk.topbeat_conf }}
    - source: {{ elk.tmpl_src}}/topbeat.yml
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      period: {{ elk.period }} 
      components: {{ elk.components }} 
      tags: {{ elk.topbeat_tags }} 

topbeat_install:
  cmd.run:
    - name: |
        cat <<EOF | /bin/bash 
        export DEBIAN_FRONTEND=noninteractive
        apt-get install -y --force-yes topbeat
        EOF
    - use_vt: True       

logstash_service:
  service.running:
    - name: logstash
    - watch:
      - pkgrepo: debian_pkg
      - file: logstash_conf
      - file: logstash_tomcat_conf
    - require:
      - pkg: logstash_install

syslog_service:
  service.running:
    - name: rsyslog
    - enable: True
    - watch:
      - pkgrepo: debian_pkg
      - file: syslog_conf
    - require:
      - pkg: logstash_install
      
start_topbeat:
  cmd.run:
    - name: tmux new-session -d "sudo service topbeat start" && sleep 4
        
enable_logstash__at_boot:
  cmd.wait:
    - name: sysv-rc-conf logstash on
    - watch:
      - service: logstash_service

enable_topbeat_at_boot:
  cmd.wait:
    - name: sysv-rc-conf topbeat on
