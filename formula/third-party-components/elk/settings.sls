#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set debian_packages_url = 'deb ' + artifacts_base_url + '/debian ./' %}

#base conf
{% set logstash_version = salt['cmd.run']( 'logstash -v') %}
{% set topbeat_version = salt['cmd.run']( '/usr/bin/topbeat -version | cut -d' ' -f3') %}
{% set environment = salt['grains.get']('environment')%}
{% set components = salt['grains.get']('xanadu-components')%}

#Custom for gallop logstash
{% set logstash_conf = salt['pillar.get']('elk:logstash_conf', '/etc/rsyslog.d/55-logstash.conf') %}
{% set topbeat_conf = salt['pillar.get']('elk:topbeat_conf', '/etc/topbeat/topbeat.yml') %}
{% set syslog_conf = salt['pillar.get']('elk:syslog_conf', '/etc/logstash/conf.d/syslog.conf') %}
{% set tomcat_conf = salt['pillar.get']('elk:tomcat_conf', '/etc/logstash/conf.d/tomcat.conf') %}

#topbeat

{% set period = salt['pillar.get']('elk:topbeat_conf:period', '17') %}
{% if environment == 'prod' %}
{% set topbeat_tags = salt['pillar.get']('elk:topbeat_conf:prod_tags', ['gallopprod','eddieprod']) %}
{% elif environment == 'pp' %}
{% set topbeat_tags = salt['pillar.get']('elk:topbeat_conf:pp_tags', ['galloppp','eddiepp']) %}
{% else %}
{% endif %}


{%- set hosts = salt['pillar.get']('elk:hosts', ['elasticinp01pp.cixp.xcl.ie:9800','elasticinp02pp.cixp.xcl.ie:9800','elasticinp03pp.cixp.xcl.ie:9800']) %}
{%- set tags = salt['pillar.get']('elk:tags', ['gallopp','eddiep']) %}

#templates
{% set tmpl_src = salt['pillar.get']('nginx:config:tmpl_loc','salt://third-party-components/elk/file') %}
