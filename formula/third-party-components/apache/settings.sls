#base conf
{% set fqdn = salt['grains.get']('fqdn') %}
{% set environment = salt['grains.get']('environment') %}
{% set name = salt['pillar.get']('apache_config:name', 'apache')  %}
{% set apache_xanadu_conf = salt['pillar.get']('apache_config:config:xanadu_conf', '/etc/apache2/xanadu.conf')  %}
{% set basedir = salt['pillar.get']('apache_config:config:basedir', '/xanadu/')  %}
{% set documentroot = salt['pillar.get']('apache_config:config:documentroot', basedir + name)  %}
{% set desktoproot = salt['pillar.get']('apache_config:config:desktoproot', documentroot + '/desktop')  %}
{% set mobileroot = salt['pillar.get']('apache_config:config:mobileroot', documentroot + '/mobile')  %}
{% set httpcgiroot = salt['pillar.get']('apache_config:config:httpcgiroot', documentroot + '/http')  %}
{% set log_dir = salt['pillar.get']('apache_config:config:log_dir', basedir + '/logs/' + name)  %}
{% set loglevel = salt['pillar.get']('apache_config:config:loglevel', 'info')  %}

#conf
{% set p  = salt['pillar.get']('apache_config', {}) %}
{%- set user = p.get('user', 'apache') %}
{%- set group = p.get('group', 'apache') %}
{%- set tech_group = p.get('tech_group', '_tech') %}


#templates
{% set tmpl_src = salt['pillar.get']('apache_config:config:tmpl_loc','salt://third-party-components/apache/file') %}
