{% from "third-party-components/apache/map.jinja" import apache_map with context %}

apache_install:
  pkg.installed:
    - name: {{ apache_map.server }}
    
apache_service:
  service.running:
    - name: {{ apache_map.service }}
    - enable: True