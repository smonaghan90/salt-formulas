{% from "third-party-components/apache/map.jinja" import apache_map with context %}
{%- import 'third-party-components/apache/settings.sls' as ap with context %}

include:
  - third-party-components.apache

{{ apache_map.configfile }}:
  file.managed:
    - template: jinja
    - source:
      - {{ ap.tmpl_src }}/apache.config.jinja
    - require:
      - pkg: apache_install
    - watch_in:
      - service: apache_service
      
apache_documentroot_dir:
  file.directory:
    - name: {{ ap.documentroot }}
    - user: {{ ap.user }}
    - group: {{ ap.tech_group }}
    - dir_mode: 774
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - pkg: apache_install
    - watch_in:
      - service: apache_service
      
apache_install_dir:
  file.directory:
    - name: {{ ap.log_dir }}
    - user: {{ ap.user }}
    - group: {{ ap.tech_group }}
    - dir_mode: 774
    - recurse:
      - user
      - group
    - makedirs: True
    - require:
      - pkg: apache_install
    - watch_in:
      - service: apache_service
      
      
{{ ap.apache_xanadu_conf }}:
  file.managed:
    - template: jinja
    - source:
      - {{ ap.tmpl_src }}/xanadu.conf.jinja
    - require:
      - pkg: apache_install
      - file: apache_documentroot_dir
    - watch_in:
      - service: apache_service 
    - context:
       fqdn: {{ ap.fqdn }}
       documentroot: {{ ap.documentroot }}
       desktoproot: {{ ap.desktoproot }}
       mobileroot: {{ ap.mobileroot }}
       httpcgiroot: {{ ap.httpcgiroot }}
       log_dir: {{ ap.log_dir }}
       loglevel: {{ ap.loglevel }}
       
{{ apache_map.vhostdir }}:
  file.directory:
    - require:
      - pkg: apache_install
    - watch_in:
      - service: apache_service

{% if grains['os_family']=="Debian" %}
/etc/apache2/envvars:
  file.managed:
    - template: jinja
    - source:
      - {{ ap.tmpl_src }}/envvars.jinja
    - require:
      - pkg: apache_install
    - watch_in:
      - service: apache_service
    - context:
        log_dir: {{ ap.log_dir }}
{% endif %}