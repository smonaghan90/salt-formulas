{%- import 'third-party-components/tomcat/settings.sls' as tc with context %}
{%- import 'third-party-components/java/settings.sls' as jdk with context %}
{%- from "third-party-components/tomcat/map.jinja" import tc_map with context %}

include:
  - third-party-components.java

tomcat_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ tc.install_package_md5 }}

tomcat_version_file:
  file.managed:
    - name: {{ tc.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ tc.name }}
        hash: {{ tc.md5_hash }}
    - require:
      - health_check: tomcat_md5_available

tomcat_stop:
  cmd.wait:
    - name: service tomcat stop
    - onlyif: ps -ef | grep tomcat | grep -v grep
    - require:
      - file: {{ tc_map.service_script }}
    - watch:
      - file: tomcat_version_file

tomcat_clean:
  cmd.wait:
    - name: rm -rf {{ tc.home }}
    - watch:
      - file: tomcat_version_file
    - require:
      - cmd: tomcat_stop

tomcat_home:
  archive.extracted:
    - name: {{ tc.basedir }}
    - source: {{ tc.install_package }}
    - source_hash: {{ tc.install_package_md5 }}
    - archive_format: tar
    - if_missing: {{ tc.home }}
    - require:
      - health_check: tomcat_md5_available

tomcat_native:
  archive.extracted:
    - name: {{ tc.bin_dir }}/tomcat_native
    - source: {{ tc.bin_dir }}/tomcat-native.tar.gz
    - source_hash: {{ tc.tomcat_native_package_md5 }}
    - archive_format: tar
    - require:
      - health_check: tomcat_md5_available
    - watch:
      - file: tomcat_version_file

tomcat_install_pkg:
    pkg.installed:
      - pkgs:
        - libssl-dev
        - libapr1-dev

tomcat_native_deployed:
  cmd.wait:
    - cwd: {{ tc.bin_dir }}/tomcat_native/tomcat-native-1.1.33-src/jni/native
    - user: root
    - names:
      - ./configure --with-apr=/usr/bin/apr-1-config --with-java-home={{ jdk.home }} --prefix={{ tc.home }}
    - watch:
      - file: tomcat_version_file

tomcat_native_install:
  cmd.wait:
    - cwd: {{ tc.bin_dir }}/tomcat_native/tomcat-native-1.1.33-src/jni/native
    - user: root
    - names:
      - make
      - make install
    - watch:
      - file: tomcat_version_file

{% for dir in tc.webapps_default %}
tomcat_clean_{% print dir %}_dir:
  file.absent:
    - name: {{ tc.webapps_dir }}/{% print dir %}
{% endfor %}


tomcat_users_xml:
  file.managed:
    - name: {{ tc.config_dir }}/tomcat-users.xml
    - source: {{ tc.tmpl_src }}/tomcat-users.xml
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - mode: 644
    - template: jinja
    - show_diff : True
    - context:
      tomcat_manager_username: {{ tc.manager_username }}
      tomcat_manager_password: {{ tc.manager_password }}

{% if salt['grains.get']('customer_code') == 'EDDIE' %}

tomcat_eddie_log_rotate:
  file.managed:
    - name: {{ tc.log_rotate_d }}/tomcat
    - source: {{ tc.tmpl_src }}/{{ tc.customer_code }}/tomcat
    - user: root
    - group: root
    - mode: 644
    - template: jinja
    - context:
      logs_dir: {{ tc.logs_dir }}
      rotate_size: {{ tc.rotate_size }}

tomcat_eddie_server_xml:
  file.managed:
    - name: {{ tc.config_dir }}/server.xml
    - source: {{ tc.tmpl_src }}/{{ tc.customer_code }}/server.xml
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - mode: 644
    - template: jinja
    - show_diff : True
    - context:
      logs_dir: {{ tc.logs_dir }}
      port: {{ tc.port }}
      protocol: {{ tc.protocol }}
      connectionTimeout: {{ tc.connectionTimeout }}
      URIEncoding: {{ tc.URIEncoding }}
      redirectPort: {{ tc.redirectPort }}
      maxHttpHeaderSize: {{ tc.maxHttpHeaderSize }}
      maxThreads: {{ tc.maxThreads }}
      minSpareThreads: {{ tc.minSpareThreads }}
      enableLookups: {{ tc.enableLookups }}
      disableUploadTimeout: {{ tc.disableUploadTimeout }}
      acceptCount: {{ tc.acceptCount }}
      scheme: {{ tc.scheme }}
      secure: {{ tc.secure }}
      SSLEnabled: {{ tc.SSLEnabled }}
      clientAuth: {{ tc.clientAuth }}
      sslProtocol: {{ tc.sslProtocol }}
      keystoreFile: {{ tc.keystoreFile }}
      keystorePass: {{ tc.keystorePass }}

{% endif %}


{% if salt['grains.get']('customer_code') == 'EDDIE' and 'federator' in grains['xanadu-components'] %}
{% set flag = 'Federator' %}
{% elif salt['grains.get']('customer_code') == "EDDIE" %}
{% set flag = 'NonFederator' %}

tomcat_eddie_home_env_cfg:
  file.managed:
    - name: {{ tc.bin_dir }}/setenv.sh
    - source: {{ tc.tmpl_src }}/{{ tc.customer_code }}/setenv.sh
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - template: jinja
    - context:
      fluentd_host: {{ tc.fluentd_host }}
      fluentd_port: {{ tc.fluentd_port }}
      java_home: {{  jdk.home  }}
      logs_dir: {{ tc.logs_dir }}
      tomcat_home: {{ tc.home }}
      tomcat_mem: {{ tc.tomcat_mem }}
      udpGroup: {{ tc.udpGroup }}
    - require:
      - archive: tomcat_home

tomcat_eddie_catalina_sh:
  file.managed:
    - name: {{ tc.bin_dir }}/catalina.sh
    - source: {{ tc.tmpl_src }}/{{ tc.customer_code }}/catalina.sh
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - mode: 744
    - template: jinja
    - context:
      logs_dir: {{ tc.logs_dir }}

{% else %}
{% set flag = 'Normal' %}

tomcat_server_xml:
  file.managed:
    - name: {{ tc.config_dir }}/server.xml
    - source: {{ tc.tmpl_src }}/server.xml
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - mode: 644
    - template: jinja
    - context:
      logs_dir: {{ tc.logs_dir }}
      port: {{ tc.port }}
      protocol: {{ tc.protocol }}
      connectionTimeout: {{ tc.connectionTimeout }}
      URIEncoding: {{ tc.URIEncoding }}
      redirectPort: {{ tc.redirectPort }}
      maxHttpHeaderSize: {{ tc.maxHttpHeaderSize }}
      maxThreads: {{ tc.maxThreads }}
      minSpareThreads: {{ tc.minSpareThreads }}
      enableLookups: {{ tc.enableLookups }}
      disableUploadTimeout: {{ tc.disableUploadTimeout }}
      acceptCount: {{ tc.acceptCount }}
      scheme: {{ tc.scheme }}
      secure: {{ tc.secure }}
      SSLEnabled: {{ tc.SSLEnabled }}
      clientAuth: {{ tc.clientAuth }}
      sslProtocol: {{ tc.sslProtocol }}
      keystoreFile: {{ tc.keystoreFile }}
      keystorePass: {{ tc.keystorePass }}

tomcat_home_env_cfg:
  file.managed:
    - name: {{ tc.bin_dir }}/setenv.sh
    - source: {{ tc.tmpl_src }}/setenv.sh
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - template: jinja
    - context:
      java_home: {{  jdk.home  }}
      logs_dir: {{ tc.logs_dir }}
      tomcat_home: {{ tc.home }}
      tomcat_mem: {{ tc.tomcat_mem }}
    - require:
      - archive: tomcat_home

tomcat_catalina_sh:
  file.managed:
    - name: {{ tc.bin_dir }}/catalina.sh
    - source: {{ tc.tmpl_src }}/catalina.sh
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - mode: 744
    - template: jinja
    - context:
      logs_dir: {{ tc.logs_dir }}


{% endif %}

tomcat_logging_props:
  file.managed:
    - name: {{ tc.config_dir }}/logging.properties
    - source: {{ tc.tmpl_src }}/logging.properties
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - mode: 644
    - template: jinja
    - context:
      logs_dir: {{ tc.logs_dir }}

tomcat_permissions:
  file.directory:
    - name: {{ tc.home }}
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - recurse:
      - user
      - group
{% if flag == 'Federator' %}
    - require:
      - file: tomcat_users_xml
      - archive: tomcat_home
{% elif flag == 'NonFederator' %}
    - require:
      - file: tomcat_users_xml
      - file: tomcat_eddie_server_xml
      - archive: tomcat_home
{% else %}
    - require:
      - file: tomcat_users_xml
      - file: tomcat_server_xml
      - archive: tomcat_home
{% endif %}

tomcat_log_permissions:
  file.directory:
    - name: {{ tc.logs_dir }}
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - dir_mode: 775
    - makedirs: True
    - require:
      - archive: tomcat_home

tomcat_baselog_permissions:
  file.directory:
    - name: {{ tc.base_logs_dir }}
    - user: {{ tc.user }}
    - group: {{ tc.group }}
    - dir_mode: 775
    - makedirs: True
    - require:
      - archive: tomcat_home

{%- if tc_map.service_script %}
{{ tc_map.service_script }}:
  file.managed:
    - source: {{ tc.tmpl_src }}/{{ tc_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ tc_map.service_script_mode }}
    - template: jinja
    - context:
      tomcat_home: {{ tc.home }}
      tomcat_user: {{ tc.user }}

tomcat_service:
  service.running:
    - name: tomcat
    - sig: org.apache.catalina.startup.Bootstrap
    - require:
      - file: tomcat_baselog_permissions
      - file: tomcat_log_permissions
      - file: {{ tc_map.service_script }}
{% if flag == 'Federator' %}
    - watch:
      - archive: tomcat_native
      - file: tomcat_eddie_log_rotate
      - file: tomcat_eddie_server_xml
      - file: {{ tc_map.service_script }}
      - file: tomcat_users_xml
      - file: tomcat_logging_props
{% elif flag == 'NonFederator' %}
    - watch:
      - archive: tomcat_native
      - file: tomcat_eddie_log_rotate
      - file: tomcat_eddie_server_xml
      - file: tomcat_eddie_home_env_cfg
      - file: tomcat_eddie_catalina_sh
      - file: {{ tc_map.service_script }}
      - file: tomcat_users_xml
      - file: tomcat_logging_props
{% else %}
    - watch:
      - archive: tomcat_native
      - file: tomcat_server_xml
      - file: tomcat_home_env_cfg
      - file: tomcat_catalina_sh
      - file: {{ tc_map.service_script }}
      - file: tomcat_users_xml
      - file: tomcat_logging_props
{% endif %}
{%- endif %}

enable_tomcat_at_boot:
  cmd.run:
    - name: sysv-rc-conf tomcat on


log_rotate_job:
  cron.present:
    - name: /usr/sbin/logrotate /etc/logrotate.d/tomcat
    - identifier: TOMCAT
    - user: root
    - minute: '*/35'
    
log_delete_job:
  cron.present:
    - name: /usr/bin/find /xanadu/logs/{{ tc.tomcat_installed_version }}/*.log* -mtime +30 -type f -exec rm -rf {} \;
    - identifier: TOMCAT_DELETE_LOGS
    - user: root
    - minute: '00'
    - hour: '01'

catalina_delete_job:
  cron.present:
    - name: /usr/bin/find /xanadu/logs/{{ tc.tomcat_installed_version }}/*catalina* -mtime +30 -type f -exec rm -rf {} \;
    - identifier: TOMCAT_DELETE_CATALINA
    - user: root
    - minute: '35'
    - hour: '01'

