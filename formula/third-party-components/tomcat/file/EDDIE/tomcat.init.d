CATALINA_HOME={{ tomcat_home }}
CATALINA_USER={{ tomcat_user }}
RETVAL=0

cd $CATALINA_HOME
case $1 in
  start)
    # Start daemon.
    sudo -u $CATALINA_USER $CATALINA_HOME/bin/startup.sh
  ;;
  stop)
    # Stop daemons.
    echo "Shutting down $CATALINA_USER";
    pid=`ps ax | grep -i 'org.apache.catalina.startup.Bootstrap' | grep -v grep | awk '{print $1}'`
    if [ -n "$pid" ]
       then
        kill -9 $pid
       else
        echo "Tomcat was not Running"
    fi
  ;;
  restart)
    # Stop daemons.
    echo "Shutting down $CATALINA_USER";
    pid=`ps ax | grep -i 'org.apache.catalina.startup.Bootstrap' | grep -v grep | awk '{print $1}'`
    if [ -n "$pid" ]
       then
        kill -9 $pid
       else
        echo "Tomcat was not Running"
    fi
    sudo -u $CATALINA_USER $CATALINA_HOME/bin/startup.sh
  ;;
  status)
    pid=`ps ax | grep -i 'org.apache.catalina.startup.Bootstrap' | grep -v grep | awk '{print $1}'`
    if [ -n "$pid" ]
      then
      echo "Tomcat is Running as PID: $pid"
      RETVAL=0
    else
      echo "Tomcat is not Running"
      RETVAL=3
    fi

    echo "Return Code: $RETVAL"
    return $RETVAL
    ;;
 *)
    echo "Usage: $0 {start|stop|restart|status}"
    exit 1
esac
exit $RETVAL
