export CATALINA_HOME="{{ tomcat_home }}"
export CATALINA_OPTS="{{ tomcat_mem }}"
export PATH=$CATALINA_HOME/bin:$PATH
export LD_LIBRARY_PATH="{{ tomcat_home }}/lib:$LD_LIBRARY_PATH"
