#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set install_package = artifacts_base_url + "/" + pillar['tomcat_config']['name'] + '.tar.gz' %}
{% set install_package_md5 = artifacts_base_url + "/" + pillar['tomcat_config']['name']  + '.tar.gz.MD5' %}
{% set tomcat_native_package_md5 = artifacts_base_url + "/tomcat-native.tar.gz.MD5" %}
{% set tomcat_installed_version = pillar['tomcat_config']['name'] %}


{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}
{% set environment = salt['grains.get']('environment') %}
{% set customer_code = salt['grains.get']('customer_code') %}

#base conf

{% set name = pillar['tomcat_config']['name'] %}
{% set home = pillar['base_install_dir'] + "/" + name %}
{% set basedir =  pillar['base_install_dir'] %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_tomcat.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = base_logs_dir + "/" + name %}
{% set bin_dir = basedir + "/" + name + "/bin" %}
{% set config_dir = basedir + "/" + name + "/conf" %}
{% set log_rotate_d = "/etc/logrotate.d" %}
{% set os_version = salt['grains.get']('lsb_distrib_release') %}
#Remove the default tomcat webapps Junk
{% set flag = False %}
{% set webapps_dir = basedir + "/" + name + "/webapps" %}
{% set webapps_default = ['examples','host-manager','ROOT','docs'] %}


#conf
{% set p  = salt['pillar.get']('tomcat_config', {}) %}
{%- set user = p.get('user', 'tomcat') %}
{%- set group = p.get('group', 'tomcat') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set pc = p.get('config', {}) %}


{% set rotate_size = salt['pillar.get']('log_rotate:tomcat_rotate_size', "200M") %}

{% set fluentd_enabled  = salt['pillar.get']('fluentd:enabled', "True") %}
{% set fluentd_host  = salt['pillar.get']('fluentd:fluentd_host', "mdb02S1stg.cix.xcl.ie") %}
{% set fluentd_port  = salt['pillar.get']('fluentd:fluentd_port', "24224") %}

{% set udpGroup = pillar['tomcat_config']['udpGroup'] %}

{%- set manager_username = pc.get('manager_username', 'xanaduadmin') %}
{%- set manager_password = pc.get('manager_password', 'X$n$du2$15') %}
{%- set port = pc.get('port', '8080') %}
{%- set protocol = pc.get('protocol', 'HTTP/1.1') %}
{%- set connectionTimeout = pc.get('connectionTimeout', '20000') %}
{%- set URIEncoding = pc.get('URIEncoding', 'UTF-8') %}
{%- set redirectPort = pc.get('redirectPort', '8443') %}
{%- set maxHttpHeaderSize = pc.get('maxHttpHeaderSize', '8192') %}
{%- set maxThreads = pc.get('maxThreads', '50') %}
{%- set minSpareThreads = pc.get('minSpareThreads', '10') %}
{%- set enableLookups = pc.get('enableLookups', 'false') %}
{%- set disableUploadTimeout = pc.get('disableUploadTimeout', 'true') %}
{%- set acceptCount = pc.get('acceptCount', '100') %}
{%- set scheme = pc.get('scheme', 'http') %}
{%- set secure = pc.get('secure', 'admin') %}
{%- set SSLEnabled = pc.get('SSLEnabled', 'false') %}
{%- set clientAuth = pc.get('clientAuth', 'false') %}
{%- set sslProtocol = pc.get('sslProtocol', 'TLS') %}
{%- set keystoreFile = pc.get('keystoreFile', 'None') %}
{%- set keystorePass = pc.get('keystorePass', 'None') %}


#JVM opts
{% set tomcat_mem = salt['pillar.get']('override_tomcat_mem:tomcat_mem','-Xms2048m -Xmx2048m') %}


#templates
{% set tmpl_src = salt['pillar.get']('tomcat_config:tmpl_loc','salt://third-party-components/tomcat/file') %}
