{%- import 'third-party-components/cassandra/settings.sls' as cas with context %}

include:
  - third-party-components.java

cassandra_version_file:
  file.managed:
    - name: {{ cas.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ cas.package_name }} {{ cas.cassandra_version }}
        hash: 'Please write a forumla for me'
