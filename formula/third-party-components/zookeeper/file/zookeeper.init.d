ZOOKEEPER_HOME={{ zookeeper_home }}
ZOOKEEPER_USER={{ zookeeper_user }}
ZOOKEEPER_PATH={{ zookeeper_home }}/bin/zkServer.sh
RETVAL=0

case $1 in
  start)
        # Start daemon.
        echo "Starting $ZOOKEEPER_USER";
        sudo -u $ZOOKEEPER_USER $ZOOKEEPER_PATH start
        ;;
  stop)
        # Stop daemons.
        echo "Shutting down $ZOOKEEPER_USER";
        pid=`ps ax | grep -i 'org.apache.zookeeper.server.quorum.QuorumPeerMain' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          kill -9 $pid
        else
          echo "Zookeeper was not Running"
        fi
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'org.apache.zookeeper.server.quorum.QuorumPeerMain' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "Zookeeper is Running as PID: $pid"
          RETVAL=0
        else
          echo "Zookeeper is not Running"
          RETVAL=3
        fi

        echo "Return Code: $RETVAL"
  		return $RETVAL
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac
exit $RETVAL
