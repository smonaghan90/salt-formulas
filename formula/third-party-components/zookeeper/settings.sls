#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set install_package = artifacts_base_url + "/" + pillar['zookeeper_config']['name'] + '.tar.gz' %}
{% set install_package_md5 = artifacts_base_url + "/" + pillar['zookeeper_config']['name']  + '.tar.gz.MD5' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}

#base conf
{% set home = pillar['base_install_dir'] + "/" + pillar['zookeeper_config']['name'] %}
{% set name = pillar['zookeeper_config']['name'] %}
{% set basedir =  pillar['base_install_dir'] %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_zookeeper.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar['zookeeper_config']['name'] %}
{% set config_dir = basedir + "/" + pillar['zookeeper_config']['name'] + "/conf" %}
{% set data = basedir + "/" + pillar['zookeeper_config']['name'] + "/data" %}

#conf
{% set p  = salt['pillar.get']('zookeeper_config', {}) %}
{%- set user = p.get('user', 'zookeeper') %}
{%- set group = p.get('group', 'zookeeper') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set pc = p.get('config', {}) %}
{% set g  = salt['grains.get']('zookeeper', {}) %}
{% set gc = g.get('config', {}) %}
{%- set bind_address      = gc.get('bind_address', '0.0.0.0') %}
{%- set data_dir = pc.get('data_dir', data) %}


{%- set port = pc.get('port', '2181') %}
{%- set jmx_port = pc.get('jmx_port', '2183') %}
{%- set purge_interval = pc.get('purge_interval', '12') %}
{%- set max_client_cnxns = pc.get('max_client_cnxns', 'None') %}
{%- set snap_retain_count = pc.get('snap_retain_count', 'None') %}
{%- set snap_count = pc.get('snapcount', 'None') %}
{%- set log_level = pc.get('log_level', 'INFO') %}
%}


#JVM conf
{%- set max_perm_size = pc.get('max_perm_size', '128') %}
{%- set max_heap_size = pc.get('max_heap_size', '1024') %}
{%- set initial_heap_size = pc.get('initial_heap_size', '256') %}
{%- set jvm_opts = pc.get('jvm_opts', 'None') %}
{%- set tapiki_opt = pc.get('tapiki_opt', '') %}
#Cluster settings
#Extract and build the server string for clustering
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

{% set environment = salt['grains.get']('environment') %}
{% set fqdn = salt['network.get_hostname']() %}

{%- set force_mine_update = salt['mine.send']('network.get_hostname') %}
{%- set zookeepers_host_dict = salt['mine.get']('roles:zookeeper', 'network.get_hostname', 'grain') %}
{%- set zookeepers_ids = zookeepers_host_dict.keys() %}
{%- set zookeepers_hosts = zookeepers_host_dict.values() %}
{%- set zookeeper_host_num = zookeepers_ids | length() %}

{%- if zookeeper_host_num == 0 %}
# this will fail to even render but provide a hint as to what's wrong
{{ 'No zookeeper nodes are defined (you need to set roles:zookeeper at least for one node in your cluster' }}
{%- elif zookeeper_host_num % 2 == 1 %}
# for 1, 3, 5 ... nodes just return the list
{%- set node_count = zookeeper_host_num %}

{%- elif zookeeper_host_num % 2 == 0 %}
# for 2, 4, 6 ... nodes return (n -1)
{%- set node_count = zookeeper_host_num - 1 %}
{%- endif %}

{%- set zookeepers_with_ids = {} %}
{%- for i in range(node_count) %}
{%- do zookeepers_with_ids.update({zookeepers_ids[i] : '{0:d}'.format(i) + '+' + zookeepers_hosts[i] })  %}
{%- endfor %}

# a plain list of hostnames
{%- set zookeepers = zookeepers_with_ids.values() | sort() %}

{%- set zookeeper_host = (zookeepers | first()).split('+') | last() %}


{%- set connection_string = [] %}
{%- for n in zookeepers %}
{%- do connection_string.append( n.split('+') | last() + ':' + port | string ) %}
{% endfor %}

# return myid for later
{%- set myid = zookeepers_with_ids.get(grains.id, '').split('+')|first() %}

{%- set zk = {} %}
{%- do zk.update( {
                           'myid': myid,
                           'port': port,
                           'jmx_port': jmx_port,
                           'data_dir': data_dir,
                           'snap_count': snap_count,
                           'snap_retain_count': snap_retain_count,
                           'purge_interval': purge_interval,
                           'max_client_cnxns': max_client_cnxns,
                           'myid_path': data_dir + '/myid',
                           'zookeeper_host' : zookeeper_host,
                           'zookeepers' : zookeepers,
                           'zookeepers_with_ids' : zookeepers_with_ids.values(),
                           'connection_string' : ','.join(connection_string),
                           'initial_heap_size': initial_heap_size,
                           'max_heap_size': max_heap_size,
                           'max_perm_size': max_perm_size,
                           'tapiki_opt': tapiki_opt,
                           'jvm_opts': jvm_opts,
                           'log_level': log_level,
                        }) %}


#templates
{% set tmpl_src = salt['pillar.get']('zookeeper:config:tmpl_loc','salt://third-party-components/zookeeper/file') %}
