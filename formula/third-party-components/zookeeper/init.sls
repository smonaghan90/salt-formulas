{%- import 'third-party-components/zookeeper/settings.sls' as zk with context %}
{%- from "third-party-components/zookeeper/map.jinja" import zk_map with context %}
{%- import 'third-party-components/java/settings.sls' as jdk with context %}

include:
  - third-party-components.java

zookeeper_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ zk.install_package_md5 }}

zookeeper_version_file:
  file.managed:
    - name: {{ zk.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ zk.name }}
        hash: {{ zk.md5_hash }}
    - require:
      - health_check: zookeeper_md5_available
  cmd.wait:
    - name: rm -rf {{ zk.home }}
    - watch:
      - file: zookeeper_version_file

zookeeper_home:
  archive.extracted:
    - name: {{ zk.basedir }}
    - source: {{ zk.install_package }}
    - source_hash: {{ zk.install_package_md5 }}
    - archive_format: tar
    - if_missing: {{ zk.home }}
    - require:
      - health_check: zookeeper_md5_available

{{ zk.data_dir }}:
  file.directory:
    - user: {{ zk.user }}
    - group: {{ zk.group }}
    - makedirs: True

zookeeper_cfg:
  file.managed:
    - name: {{ zk.config_dir }}/zoo.cfg
    - source: {{ zk.tmpl_src }}/zoo.cfg
    - user: {{ zk.user }}
    - group: {{ zk.group }}
    - mode: 644
    - template: jinja
    - context:
      data_dir: {{ zk.data_dir }}
      port: {{ zk.port }}
      snap_count: {{ zk.snap_count }}
      snap_retain_count: {{ zk.snap_retain_count }}
      purge_interval: {{ zk.purge_interval }}
      max_client_cnxns: {{ zk.max_client_cnxns }}
      zookeepers: {{ zk.zookeepers }}
      bind_address: {{ zk.bind_address }}

zookeeper_myid:
  file.managed:
    - name: {{ zk.data_dir}}/myid
    - source: {{ zk.tmpl_src }}/myid
    - user: {{ zk.user }}
    - group: {{ zk.group }}
    - mode: 644
    - template: jinja
    - context:
      myid: {{ zk.myid }}

zookeeper_log4j_cfg:
  file.managed:
    - name: {{ zk.config_dir }}/log4j.properties
    - source: {{ zk.tmpl_src }}/log4j.properties
    - user: {{ zk.user }}
    - group: {{ zk.group }}
    - mode: 644
    - template: jinja

zookeeper_env_cfg:
  file.managed:
    - name: {{ zk.config_dir }}/zookeeper-env.sh
    - source: {{ zk.tmpl_src }}/zookeeper-env.sh
    - user: {{ zk.user }}
    - group: {{ zk.group }}
    - mode: 644
    - template: jinja
    - context:
      java_home: {{ jdk.home }}
      logs_dir: {{ zk.logs_dir }}
      jmx_port: {{ zk.jmx_port }}
      initial_heap_size: {{ zk.initial_heap_size }}
      max_heap_size: {{ zk.max_heap_size }}
      max_perm_size: {{ zk.max_perm_size }}
      jvm_opts: {{ zk.jvm_opts }}
      log_level: {{ zk.log_level }}

zookeeper_permissions:
  file.directory:
    - name: {{ zk.home }}
    - user: {{ zk.user }}
    - group: {{ zk.group }}
    - recurse:
      - user
      - group
    - require:
      - file: zookeeper_log4j_cfg
      - file: zookeeper_env_cfg
      - archive: zookeeper_home

zookeeper_log_permissions:
  file.directory:
    - name: {{ zk.base_logs_dir }}
    - user: root
    - group: {{ zk.tech_group }}
    - dir_mode: 774
    - makedirs: True
    - require:
      - archive: zookeeper_home

{%- if zk_map.service_script %}
{{ zk_map.service_script }}:
  file.managed:
    - source: {{ zk.tmpl_src }}/{{ zk_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ zk_map.service_script_mode }}
    - template: jinja
    - context:
      zookeeper_home: {{ zk.home }}
      zookeeper_user: {{ zk.user }}

zookeeper_service:
  service.running:
    - name: zookeeper
    - enable: True
    - sig: org.apache.zookeeper.server.quorum.QuorumPeerMain
    - require:
      - file: {{ zk.data_dir }}
      - file: zookeeper_log_permissions
    - watch:
      - file: zookeeper_cfg
      - file: zookeeper_log4j_cfg
      - file: zookeeper_env_cfg
{%- endif %}


{%- if zk.hipchat_enabled == True %}

zookeeper_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ zk.auth_token }}
    - room_id: {{ zk.room_id }}
    - message: 'Artifact version [{{ zk.name }}] to the [{{ zk.environment }}] environment [{{ zk.fqdn }}] successfull.'
    - html: True
    - message_color: green
    - notify: True
    - onchanges:
      - service: zookeeper_service

zookeeper_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ zk.auth_token }}
    - room_id: {{ zk.room_id }}
    - message: 'Artifact version [{{ zk.name }}] to the [{{ zk.environment }}] environment [{{ zk.fqdn }}] failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: zookeeper_hipchat_notifier_deploy_finished

{%- endif %}
