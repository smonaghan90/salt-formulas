
{%- import 'third-party-components/redis/settings.sls' as redis with context %}
{%- from "third-party-components/redis/map.jinja" import redis_map with context %}



redis_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ redis.install_package_md5 }}


redis_version_file:
  file.managed:
    - name: {{ redis.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ redis.name }}
        hash: {{ redis.md5_hash }}
    - require:
      - health_check: redis_md5_available

redis_log_dir:
  file.managed:
    - name: {{ redis.logs_dir }}/redis_6379.log
    - makedirs: True
    - show_diff: True
    - user: {{ redis.user }}
    - group: {{ redis.group }}


redis_set_file_limit:
  file.managed:
    - source: {{ redis.tmpl_src }}/limits.conf
    - name: /etc/security/limits.conf
    - user: root
    - group: root

redis_update_limits:
  cmd.run:
    - names:
      - sysctl -p
      - sysctl net.core.somaxconn={{ redis.maxclients }}


redis_home:
  archive.extracted:
    - name: {{ redis.basedir }}
    - source: {{ redis.install_package }}
    - source_hash: {{ redis.install_package_md5 }}
    - archive_format: tar
    - user: {{ redis.user }}
    - group: {{ redis.group }}
    - if_missing: {{ redis.home }}
    - require:
      - health_check: redis_md5_available
    - watch:
      - file: redis_version_file

  cmd.run:
    - cwd: {{ redis.home }}
    - user: root
    - names:
      - make
    - watch:
      - file: redis_version_file
    - require:
      - archive: redis_home


redis_create_etc:
  file.directory:
    - name: {{ redis.etc }}
    - user: {{ redis.user }}
    - group: {{ redis.group }}
    - makedirs: True
    - recurse:
      - user
      - group
    - require:
      - archive: redis_home

redis_copy_config:
  file.managed:
    - source: {{ redis.tmpl_src }}/redis.conf
    - name: {{ redis.etc }}/redis.conf
    - user: {{ redis.user }}
    - group: {{ redis.group }}
    - template: jinja
    - context:
      pidfile: {{ redis.pidfile }}
      dir: {{ redis.dir }}
      lua_time_limit: {{ redis.lua_time_limit }}
      latency_monitor_threshold: {{ redis.latency_monitor_threshold }}
      notify_keyspace_events: {{ redis.notify_keyspace_events }}
      slowlog_log_slower_than: {{ redis.slowlog_log_slower_than }}
      slowlog_max_len: {{ redis.slowlog_max_len }}
      hash_max_ziplist_entries: {{ redis.hash_max_ziplist_entries }}
      hash_max_ziplist_value: {{ redis.hash_max_ziplist_value }}
      list_max_ziplist_entries: {{ redis.list_max_ziplist_entries }}
      list_max_ziplist_value: {{ redis.list_max_ziplist_value }}
      zset_max_ziplist_entries: {{ redis.zset_max_ziplist_entries }}
      zset_max_ziplist_value: {{ redis.zset_max_ziplist_value }}
      set_max_intset_entries: {{ redis.set_max_intset_entries }}
      hll_sparse_max_bytes: {{ redis.hll_sparse_max_bytes }}
      activerehashing: '{{ redis.activerehashing }}'
      client_output_buffer_limit_1: {{ redis.client_output_buffer_limit_1 }}
      client_output_buffer_limit_2: {{ redis.client_output_buffer_limit_2 }}
      client_output_buffer_limit_3: {{ redis.client_output_buffer_limit_3 }}
      hz: {{ redis.hz }}
      daemonize: '{{ redis.daemonize }}'
      port: {{ redis.port }}
      tcp_backlog: {{ redis.tcp_backlog }}
      loglevel: {{ redis.loglevel }}
      timeout: {{ redis.timeout }}
      tcp_keepalive: {{ redis.tcp_keepalive }}
      logfile: {{ redis.logfile }}
      databases: {{ redis.databases }}
      save_1: {{ redis.save_1 }}
      save_10: {{ redis.save_10 }}
      save_10000: {{ redis.save_10000 }}
      stop_writes_on_bgsave_error: '{{ redis.stop_writes_on_bgsave_error }}'
      rdbcompression: '{{ redis.rdbcompression }}'
      rdbchecksum: '{{ redis.rdbchecksum }}'
      dbfilename: {{ redis.dbfilename }}
      slave_serve_stale_data: '{{ redis.slave_serve_stale_data }}'
      slave_read_only: '{{ redis.slave_read_only }}'
      repl_diskless_sync: '{{ redis.repl_diskless_sync }}'
      repl_diskless_sync_delay: {{ redis.repl_diskless_sync_delay }}
      repl_disable_tcp_nodelay: '{{ redis.repl_disable_tcp_nodelay }}'
      slave_priority: {{ redis.slave_priority }}
      maxclients: {{ redis.maxclients }}
      maxmemory_policy: {{ redis.maxmemory_policy }}
      appendonly: '{{ redis.appendonly }}'
      appendfilename: {{ redis.appendfilename }}
      appendfsync: {{ redis.appendfsync }}
      no_appendfsync_on_rewrite: '{{ redis.no_appendfsync_on_rewrite }}'
      auto_aof_rewrite_percentage: {{ redis.auto_aof_rewrite_percentage }}
      auto_aof_rewrite_min_size: {{ redis.auto_aof_rewrite_min_size }}
      aof_load_truncated: '{{ redis.aof_load_truncated }}'
      aof_rewrite_incremental_fsync: '{{ redis.aof_rewrite_incremental_fsync }}'

redis_copy_sentinel_config:
  file.managed:
    - source: {{ redis.tmpl_src }}/sentinel.conf
    - name: {{ redis.etc }}/sentinel.conf
    - user: {{ redis.user }}
    - group: {{ redis.group }}
    - template: jinja
    - context:
      master: {{ redis.master }}
      slaves: {{ redis.slaves }}
      sentinelpidfile: {{ redis.sentinelpidfile }}
      sentinellogfile: {{ redis.sentinellogfile }}
      dir: {{ redis.dir }}


{%- if redis_map.redis_service_script %}

{{ redis_map.redis_service_script }}:
  file.managed:
    - source: {{ redis.tmpl_src }}/{{ redis_map.redis_service_script_source }}
    - user: root
    - group: root
    - mode: {{ redis_map.redis_service_script_mode }}
    - template: jinja
    - context:
      pidfile: {{ redis.pidfile }}
      port: {{ redis.port }}
      redis_server_path: {{ redis.redis_server_path }}
      redis_cli_path: {{ redis.redis_cli_path }}
      xd_home: {{ redis.home }}
      xd_user: {{ redis.user }}
      logs_dir: {{ redis.logs_dir }}
      watch:
        - file: redis_copy_config
        - file: redis_version_file


redis_service:
  service.running:
    - enable: True
    - name: redis
    - sig: redis-server
    - watch:
      - file: {{ redis_map.redis_service_script }}
      - file: redis_copy_config
      - file: redis_version_file
    - require:
      - archive: redis_home
      - file: redis_copy_config


{%- endif %}

{%- if redis_map.sentinel_service_script %}

{{ redis_map.sentinel_service_script }}:
  file.managed:
    - source: {{ redis.tmpl_src }}/{{ redis_map.sentinel_service_script_source }}
    - user: root
    - group: root
    - mode: {{ redis_map.sentinel_service_script_mode }}
    - template: jinja
    - context:
      pidfile: {{ redis.pidfile }}
      sentinelpidfile: {{ redis.sentinelpidfile }}
      port: {{ redis.port }}
      redis_server_path: {{ redis.redis_server_path }}
      redis_cli_path: {{ redis.redis_cli_path }}
      redis_sentinel_path: {{ redis.redis_sentinel_path }}
      xd_home: {{ redis.home }}
      xd_user: {{ redis.user }}
      logs_dir: {{ redis.logs_dir }}
      watch:
        - file: redis_copy_config
        - file: redis_version_file

sentinel_service:
  service.running:
    - enable: True
    - name: sentinel
    - sig: redis-sentinel
    - watch:
      - file: redis_copy_sentinel_config
      - file: {{ redis_map.sentinel_service_script }}


{%- endif %}
