#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set install_package = artifacts_base_url + "/" + pillar['redis_config']['name'] + '.tar.gz' %}
{% set install_package_md5 = artifacts_base_url + "/" + pillar['redis_config']['name']  + '.tar.gz.MD5' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}

#base conf
{% set name = pillar['redis_config']['name'] %}
{% set basedir =  pillar['base_install_dir'] %}

{% set home  =  pillar['base_install_dir']  + "/" + pillar['redis_config']['name'] %}


{% set md5_file = pillar['install_packages_dir'] + '/xc_version_redis.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar['redis_config']['name'] %}

{% set etc = '/etc/redis' %}
{% set redis_server_path = home + '/src/' %}
{% set redis_sentinel_path = home + '/src/redis-sentinel ' %}
{% set redis_cli_path = home + '/src/redis-cli -p ' %}


#Redis config
{% set p  = salt['pillar.get']('redis_config', {}) %}

#
{%- set user = p.get('user', 'redis') %}
{%- set group = p.get('group', 'redis') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set pc = p.get('config', {}) %}
{%- set daemonize = pc.get('daemonize', 'yes') %}
{%- set port = pc.get('port', '6379') %}
{%- set tcp_backlog = pc.get('tcp_backlog', '511') %}
{%- set timeout = pc.get('timeout', '0') %}
{%- set loglevel = pc.get('loglevel', 'notice') %}
{%- set pidfile = pc.get('pidfile', '/xanadu/redis-3.0.7/redis_6379.pid') %}
{%- set sentinelpidfile = pc.get('sentinelpidfile', '/xanadu/redis-3.0.7/sentinelpidfile.pid') %}
{%- set tcp_keepalive = pc.get('tcp_keepalive', '0') %}
{%- set logfile = logs_dir + '/' + pc.get('logfile', '6379.log') %}
{%- set sentinellogfile = logs_dir + '/' + pc.get('sentinellogfile', 'sentinel.log') %}
{%- set databases = pc.get('databases', '0') %}
{%- set save_1 = pc.get('save_1', '0') %}
{%- set save_10 = pc.get('save_10', '300 10') %}
{%- set save_10000 = pc.get('save_10000', '60 10000') %}
{%- set stop_writes_on_bgsave_error = pc.get('stop_writes_on_bgsave_error', 'yes') %}
{%- set rdbcompression = pc.get('rdbcompression', 'yes') %}
{%- set rdbchecksum = pc.get('rdbchecksum', 'yes') %}
{%- set dbfilename = pc.get('dbfilename', 'dump.rdb') %}
{%- set stop_writes_on_bgsave_error = pc.get('stop_writes_on_bgsave_error', 'yes') %}
{%- set dir = "./" %}
{%- set slave_serve_stale_data = pc.get('slave_serve_stale_data', 'yes') %}
{%- set slave_read_only = pc.get('slave_read_only', 'yes') %}
{%- set repl_diskless_sync = pc.get('repl_diskless_sync', 'no') %}
{%- set repl_diskless_sync_delay = pc.get('repl_diskless_sync_delay', '5') %}
{%- set repl_disable_tcp_nodelay = pc.get('repl_disable_tcp_nodelay', 'no') %}
{%- set slave_priority = pc.get('slave_priority', '100') %}
{%- set maxclients = pc.get('maxclients', '10000') %}
{%- set maxmemory_policy = pc.get('maxmemory_policy', 'volatile_ttl') %}
{%- set appendonly = pc.get('appendonly', 'no') %}
{%- set appendfilename = pc.get('appendfilename', 'appendonly.aof' ) %}
{%- set appendfsync = pc.get('appendfsync', 'everysec' ) %}
{%- set no_appendfsync_on_rewrite = pc.get('no_appendfsync_on_rewrite', 'no' ) %}
{%- set auto_aof_rewrite_percentage = pc.get('auto_aof_rewrite_percentage', '100' ) %}
{%- set auto_aof_rewrite_min_size = pc.get('auto_aof_rewrite_min_size', '64mb') %}

{%- set aof_load_truncated = pc.get('aof_load_truncated', 'yes') %}
{%- set lua_time_limit = pc.get('lua_time_limit', '5000' ) %}
{%- set slowlog_log_slower_than = pc.get('slowlog_log_slower_than', '10000' ) %}
{%- set slowlog_max_len = pc.get('slowlog_max_len', '128' ) %}
{%- set latency_monitor_threshold = pc.get('latency_monitor_threshold', '0' ) %}

{%- set hash_max_ziplist_entries = pc.get('hash_max_ziplist_entries', '512' ) %}
{%- set hash_max_ziplist_value = pc.get('hash_max_ziplist_value', '64' ) %}

{%- set list_max_ziplist_entries = pc.get('list_max_ziplist_entries', '512' ) %}
{%- set list_max_ziplist_value = pc.get('list_max_ziplist_value', '64' ) %}

{%- set zset_max_ziplist_entries = pc.get('zset_max_ziplist_entries', '128' ) %}
{%- set zset_max_ziplist_value = pc.get('zset_max_ziplist_value', '64' ) %}

{%- set set_max_intset_entries = pc.get('set_max_intset_entries', '512' ) %}
{%- set hll_sparse_max_bytes = pc.get('hll_sparse_max_bytes', '3000' ) %}
{%- set activerehashing = pc.get('activerehashing', 'yes' ) %}


{%- set notify_keyspace_events = pc.get('notify_keyspace_events', '0' ) %}
{%- set hash_max_ziplist_entries = pc.get('hash_max_ziplist_entries', '512' ) %}
{%- set hash_max_ziplist_value = pc.get('hash_max_ziplist_value', '64' ) %}

{%- set list_max_ziplist_entries = pc.get('list_max_ziplist_entries', '512' ) %}
{%- set list_max_ziplist_value = pc.get('list_max_ziplist_value', '64' ) %}
{%- set set_max_intset_entries = pc.get('set_max_intset_entries', '512' ) %}
{%- set zset_max_ziplist_entries = pc.get('zset_max_ziplist_entries', '128' ) %}
{%- set zset_max_ziplist_value = pc.get('zset_max_ziplist_value', '64' ) %}
{%- set hll_sparse_max_bytes = pc.get('hll_sparse_max_bytes', '3000' ) %}
{%- set activerehashing = pc.get('activerehashing', 'yes' ) %}
{%- set client_output_buffer_limit_1 = pc.get('client_output_buffer_limit_1', 'normal 0 0 0' ) %}
{%- set client_output_buffer_limit_2 = pc.get('client_output_buffer_limit_2', 'slave 256mb 64mb 60' ) %}
{%- set client_output_buffer_limit_3 = pc.get('client_output_buffer_limit_3', 'pubsub 32mb 8mb 60' ) %}

{%- set hz = pc.get('hz', '10' ) %}
{%- set aof_rewrite_incremental_fsync = pc.get('aof_rewrite_incremental_fsync', 'yes' ) %}

{% set p  = salt['pillar.get']('redis_config', {}) %}

{% set pcc = p.get('cluster', {}) %}
{%- set master = pcc.get('master', '127.0.0.1') %}
{%- set slaves = pcc.get('slaves', '127.0.0.1') %}














#templates
{% set tmpl_src = salt['pillar.get']('redis:config:tmpl_loc','salt://third-party-components/redis/files') %}
