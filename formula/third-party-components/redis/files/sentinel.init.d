#!/bin/sh
#
# Simple Redis init.d script conceived to work on Linux systems
# as it does use of the /proc filesystem.

REDISPORT={{ port }}
EXEC={{ redis_server_path }}
CLIEXEC="{{ redis_cli_path }} "
USER=redis


NAME=redis-sentinel
BIN={{ redis_sentinel_path }}
SENTINEL_PID={{ sentinelpidfile }}
CMD=$1

start() {
        echo "Starting $NAME ..."
        exec 2>&1 sudo -u $USER $BIN /etc/redis/sentinel.conf  &
        echo $! > "${SENTINEL_PID}";
}

stop() {
        PID=`cat $SENTINEL_PID`
        echo "Stopping $NAME ($PID) ..."
        sudo -u $USER kill $(pgrep sentinel)

}

restart() {
        echo "Restarting $NAME ..."
        stop
        start
}

case "$CMD" in
        start)
                start
                ;;
        stop)
                stop
                ;;
        restart)
                restart
                ;;
        *)
                echo "Usage $0 {start|stop|restart}"
esac
