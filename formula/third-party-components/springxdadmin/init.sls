{%- import 'third-party-components/springxdadmin/settings.sls' as xda with context %}
{%- from "third-party-components/springxdadmin/map.jinja" import xd_map with context %}
{%- import 'third-party-components/kafka/settings.sls' as kfa with context %}


include:
  - third-party-components.java

xd_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xda.install_package_md5 }}


xd_jar_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ xda.jar_package_md5 }}


springadmin_version_file:
  file.managed:
    - name: {{ xda.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ xda.name }}
        hash: {{ xda.md5_hash }}
    - require:
      - health_check: xd_md5_available


xd_home:
  archive.extracted:
    - name: {{ xda.basedir }}
    - source: {{ xda.install_package }}
    - source_hash: {{ xda.install_package_md5 }}
    - archive_format: zip
    - if_missing: {{ xda.home }}
    - require:
      - health_check: xd_md5_available
    - watch:
      - file: springadmin_version_file



xd_download_extra_jars:
  cmd.run:
    - name: |
        wget -NqO- {{ xda.jar_package }} | tar xvz -C {{ xda.xd_lib }}
    - require:
      - health_check: xd_jar_md5_available
    - watch:
      - file: springadmin_version_file


xd_permissions:
  file.directory:
    - name: {{ xda.home }}
    - user: {{ xda.user }}
    - group: {{ xda.group }}
    - makedirs: True
    - recurse:
      - user
      - group
    - require:
      - archive: xd_home

xd_script_permissions:
  file.directory:
    - name: {{ xda.xd_bin }}
    - user: {{ xda.user }}
    - group: {{ xda.group }}
    - makedirs: True
    - mode: 744
    - recurse:
      - user
      - group
      - mode
    - require:
        - archive: xd_home

#Spring XD admin
{%- if xd_map.hsql_service_script %}
{{ xd_map.hsql_service_script }}:
  file.managed:
    - source: {{ xda.tmpl_src }}/{{ xd_map.hsql_service_script_source }}
    - user: root
    - group: root
    - mode: {{ xd_map.hsql_service_script_mode }}
    - template: jinja
    - context:
      xd_home: {{ xda.home }}
      xd_user: {{ xda.user }}
      logs_dir: {{ xda.logs_dir }}


#Set [permission on bin files]
{{ xda.hsqldb_bin }}:
  file.directory:
    - user: {{ xda.user }}
    - group: {{ xda.group }}
    - mode: 744
    - makedirs: True
    - recurse:
      - user
      - group
      - mode
    - require:
        - archive: xd_home

{%- endif %}

#Springxd admin servers
xd_admin_setup_servers:
  file.managed:
    {% if xda.customer_code == 'xdc1' %}
    - source: {{ xda.tmpl_src }}/xdc1-server.yml
    {% else %}
    - source: {{ xda.tmpl_src }}/servers.yml
    {% endif %}
    - name:  {{ xda.config_dir}}/servers.yml
    - user: {{ xda.user }}
    - group: {{ xda.group }}
    - mode: 644
    - template: jinja
    - context:
      kafka_cluster: {{ xda.cluster }}
      zookeeper_str: {{ kfa.zookeepers }}
      number_of_admin_servers: {{ xda.number_of_admin_servers }}
      adminServers: {{ xda.adminServers }}
      adminMemory: {{ xda.adminMemory }}
      containers: {{ xda.containers }}
      containerMemory: {{ xda.containerMemory }}
      hdfs_master: {{ xda.hdfs_master }}
      hdfs_module_path: {{ xda.hdfs_module_path }}
      environment: {{ xda.environment }}
      port: {{ xda.port }}
    - require:
        - archive: xd_home




#xd_admin service
{%- if xd_map.xd_admin_service_script %}
{{ xd_map.xd_admin_service_script }}:
  file.managed:
    - source: {{ xda.tmpl_src }}/{{ xd_map.xd_admin_service_script_source }}
    - user: root
    - group: root
    - mode: {{ xd_map.xd_admin_service_script_mode }}
    - template: jinja
    - context:
      xd_home: {{ xda.home }}
      xd_user: {{ xda.user }}
      logs_dir: {{ xda.logs_dir }}

{%- endif %}

#Check if hsqldb service is running watch config files
hsqldb_service:
  service.running:
    - enable: True
    - name: hsqldb
    - sig: hsqldb-server
    - watch:
      - file: {{ xd_map.hsql_service_script }}

xd_admin_service:
  service.running:
    - enable: True
    - name: xd-admin
    - sig: spring.application.name=admin
    - watch:
      - file: xd_admin_setup_servers
      - file: {{ xd_map.xd_admin_service_script }}
    - require_in:
      - service: hsqldb_service


successful_xd_admin_deployment:
  health_check.wait_for_url:
    - timeout: 60 #this is in seconds
    - url: http://{{ salt['grains.get']('fqdn') }}:{{ xda.port }}/admin-ui/#/jobs/definitions
    - match_text: "Login"
    - require:
      - file: {{ xd_map.xd_admin_service_script }}
      - file: xd_admin_setup_servers
      - service: xd_admin_service
