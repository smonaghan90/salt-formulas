XD_BIN={{ xd_home }}/xd
XD_USER={{ xd_user }}
XD_PATH=$XD_BIN/bin
XD_ADMIN=xd-admin





RETVAL=0

case $1 in
  start)
    echo "Starting $XD_ADMIN";
    sudo -u $XD_USER nohup $XD_PATH/$XD_ADMIN  > /dev/null 2>&1 &
  ;;
  stop)
        # Stop daemons.
        echo "Shutting down $XD_ADMIN";
        pid=`ps ax | grep -i 'spring.application.name=admin' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          kill -9 $pid
        else
          echo "xd-admin was not Running"
        fi
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'spring.application.name=admin' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "xd-admin is Running as PID: $pid"
          RETVAL=0
        else
          echo "xd-admin is not Running"
          RETVAL=3
        fi

        echo "Return Code: $RETVAL"
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac
exit $RETVAL
