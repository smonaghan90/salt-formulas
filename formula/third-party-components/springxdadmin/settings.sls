#artifacts
{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}

{% set install_package = artifacts_base_url + "/" + pillar['springxdadmin_config']['name'] + '.zip'  %}
{% set install_package_md5 = artifacts_base_url + "/" + pillar['springxdadmin_config']['name'] + '.zip.MD5' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}
{%- set customer_code = salt['grains.get']('customer_code') %}


{% set jars_base_url = pillar['certified_platform_base_url'] + "/certified_jars/"  + pillar['certified_platform'] %}
{% set jar_package = jars_base_url + "/certified_jars.tar.gz"  %}
{% set jar_package_md5 = jars_base_url + "/certified_jars.tgz.MD5" %}





#base conf
{% set install_dir = pillar['base_install_dir'] %}
{% set name = pillar['springxdadmin_config']['name'] %}
{% set basedir =  pillar['base_install_dir'] %}
{% set home = basedir + "/" + pillar['springxdadmin_config']['name']  %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_springxd.txt' %}
{% set hsqldb_bin = basedir + "/" + pillar['springxdadmin_config']['name'] + "/hsqldb/bin" %}
{% set xd_lib = basedir + "/" + pillar['springxdadmin_config']['name'] + "/xd/lib/" %}

{% set xd_bin = basedir + "/" + pillar['springxdadmin_config']['name'] + "/xd/bin/" %}
{% set number_of_admin_servers =  pillar['springxdadmin_config']['number_of_admin_servers']  %}
{% set adminServers =  pillar['springxdadmin_config']['adminServers']  %}
{% set adminMemory =  pillar['springxdadmin_config']['adminMemory']  %}
{% set containers =  pillar['springxdadmin_config']['containers']  %}
{% set containerMemory =  pillar['springxdadmin_config']['containerMemory']  %}
#XD settomgs
{% set transport = pillar['springxdadmin_config']['transport'] %}


{% set port =  pillar['springxdadmin_config']['port']  %}

#HDFS path
{% set environment = salt['grains.get']('environment') %}
{% set hdfs_master = pillar['hdfs_master'] %}
{% set hdfs_module_path = pillar['hdfs_module_path']  %}



#confF
{% set p  = salt['pillar.get']('users:java_info', {}) %}
{%- set user = p.get('user', 'java') %}
{%- set group = p.get('group', '_tech') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set logs_dir = pillar['base_log_dir'] + "/" + pillar['springxdadmin_config']['name'] %}
{% set config_dir = basedir + "/" + pillar['springxdadmin_config']['name'] + "/xd/config" %}


#Fetch kafka cluster
{% set pk  = salt['pillar.get']('kafka_config', {}) %}
{% set cluster = pillar['kafka_config']['cluster']['nodes']  %}




{% set p  = salt['pillar.get']('springxdadmin_config', {}) %}


#templates
{% set tmpl_src = salt['pillar.get']('springxdadmin:config:tmpl_loc','salt://third-party-components/springxdadmin/file') %}
