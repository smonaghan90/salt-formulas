{%- import 'third-party-components/zabbix/settings.sls' as zabbix with context %}

zabbix-agent_install:
  pkg:
    - installed
    - name: zabbix-agent
  service:
    - running
    - name: zabbix-agent
    - enable: True
    - require:
      - pkg: zabbix-agent

percona_zabbix_libs:
  cmd.run:
    - name: wget https://www.percona.com/downloads/percona-monitoring-plugins/1.1.5/percona-zabbix-templates_1.1.5-1_all.deb && dpkg -i percona-zabbix-templates_1.1.5-1_all.deb

/etc/zabbix/zabbix_agentd.conf:
  file:
    - managed
    - source:
      - salt://third-party-components/zabbix/files/default/etc/zabbix/zabbix_agentd.conf.jinja
    - template: jinja
    - context:
      server:  {{ zabbix.server  }}
      Hostname: {{ zabbix.Hostname }}
    - require:
      - pkg: zabbix-agent
    - watch_in:
      - service: zabbix-agent
