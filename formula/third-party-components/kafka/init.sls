{%- import 'third-party-components/kafka/settings.sls' as kf with context %}
{%- import 'third-party-components/java/settings.sls' as jdk with context %}
{%- from "third-party-components/kafka/map.jinja" import kf_map with context %}

# Set ROLE



include:
  - third-party-components.java
  - third-party-components.kafka


kafka_md5_available:
  health_check.wait_for_url:
    - timeout: 3 #this is in seconds
    - url: {{ kf.install_package_md5 }}

kafka_version_file:
  file.managed:
    - name: {{ kf.md5_file }}
    - makedirs: True
    - show_diff: True
    - contents: |
        version: {{ kf.name }}
        hash: {{ kf.md5_hash }}
    - require:
      - health_check: kafka_md5_available
  cmd.wait:
    - name: rm -rf {{ kf.home }}
    - watch:
      - file: kafka_version_file

kafka_home:
  archive.extracted:
    - name: {{ kf.basedir }}
    - source: {{ kf.install_package }}
    - source_hash: {{ kf.install_package_md5 }}
    - archive_format: tar
    - if_missing: {{ kf.home }}
    - require:
      - health_check: kafka_md5_available

{%- if kf_map.profile_localtion %}
set_kafka_home_env_variable:
  file.managed:
    - name: {{ kf_map.profile_localtion }}/kafka-env.sh
    - source: {{ kf.tmpl_src }}/kafka-env.sh
    - template: jinja
    - require:
      - archive: kafka_home
{%- endif %}


kafka_cfg:
  file.managed:
    - name: {{ kf.config_dir }}/server.properties
    - source: {{ kf.tmpl_src }}/server.properties
    - user: {{ kf.user }}
    - group: {{ kf.group }}
    - mode: 644
    - template: jinja
    - context:
      logs_dir: {{ kf.logs_dir }}
      broker_id: {{ kf.myid }}
      port: {{ kf.port }}
      host_name: {{ kf.host_name }}
      advertised_host_name: {{ kf.advertised_host_name }}
      num_partitions: {{ kf.num_partitions }}
      zookeeper_connect: {{ kf.zookeeper_connect }}
      log_dirs: {{ kf.log_dirs }}
      log_index_interval_bytes: {{ kf.log_index_interval_bytes }}
      log_retention_hours: {{ kf.log_retention_hours }}
      log_flush_interval_ms: {{ kf.log_flush_interval_ms }}
      log_flush_interval_messages: {{ kf.log_flush_interval_messages }}
      log_flush_scheduler_interval_ms: {{ kf.log_flush_scheduler_interval_ms }}
      log_roll_hours: {{ kf.log_roll_hours }}
      log_segment_bytes: {{ kf.log_segment_bytes }}
      log_cleaner_enable: '{{ kf.log_cleaner_enable }}'
      log_retention_check_interval_ms: {{ kf.log_retention_check_interval_ms }}
      zookeeper_str: {{ kf.zookeepers }}
      zk_port: {{ kf.zk_port}}
      num_replica_fetchers: {{ kf.num_replica_fetchers }}
      replica_fetch_wait_max_ms: {{ kf.replica_fetch_wait_max_ms }}
      replica_fetch_max_bytes: {{ kf.replica_fetch_max_bytes }}
      replica_high_watermark_checkpoint_interval_ms: {{ kf.replica_high_watermark_checkpoint_interval_ms }}
      replica_socket_receive_buffer_bytes: {{ kf.replica_socket_receive_buffer_bytes }}
      replica_lag_time_max_ms: {{ kf.replica_lag_time_max_ms }}
      replica_lag_max_messages: {{ kf.replica_lag_max_messages }}
      controller_socket_timeout_ms: {{ kf.controller_socket_timeout_ms }}
      controller_message_queue_size: {{ kf.controller_message_queue_size }}
      fqdn: {{ kf.fqdn }}
      kafka_id: {{ kf.kafka_id }}

server_cfg:
  file.managed:
    - name: {{ kf.config_dir }}/producer.properties
    - source: {{ kf.tmpl_src }}/producer.properties
    - user: {{ kf.user }}
    - group: {{ kf.group }}
    - mode: 644
    - template: jinja
    - context:
      fqdn: {{ kf.fqdn }}


kafka_permissions:
  file.directory:
    - name: {{ kf.home }}
    - user: {{ kf.user }}
    - group: {{ kf.group }}
    - recurse:
      - user
      - group
    - require:
      - file: kafka_cfg
      - archive: kafka_home

kafka_log_permissions:
  file.directory:
    - name: {{ kf.base_logs_dir }}/{{ kf.name }}
    - user: kafka
    - group: {{ kf.tech_group }}
    - makedirs: True
    - dir_mode: 774
    - require:
      - archive: kafka_home

kafka_run-class_sh:
  file.managed:
    - name: {{ kf.home }}/bin/kafka-run-class.sh
    - source: {{ kf.tmpl_src }}/kafka-run-class.sh
    - user: {{ kf.user }}
    - group: {{ kf.group }}
    - mode: 774
    - template: jinja
    - context:
      logs_dir: {{ kf.logs_dir }}


kafka-server-start.sh:
  file.managed:
    - name: {{ kf.home }}/bin/kafka-server-start.sh
    - source: {{ kf.tmpl_src }}/kafka-server-start.sh
    - user: {{ kf.user }}
    - group: {{ kf.group }}
    - mode: 774
    - template: jinja
    - context:
      xmx: {{ kf.xmx }}
      xms: {{ kf.xms }}


{%- if kf_map.service_script %}
{{ kf_map.service_script }}:
  file.managed:
    - source: {{ kf.tmpl_src }}/{{ kf_map.service_script_source }}
    - user: root
    - group: root
    - mode: {{ kf_map.service_script_mode }}
    - template: jinja
    - context:
      kafka_home: {{ kf.home }}
      kafka_user: {{ kf.user }}
      logs_dir: {{ kf.logs_dir }}

kafka_service:
  service.running:
    - name: kafka
    - enable: True
    - sig: kafka.Kafka
    - require:
      - file: set_kafka_home_env_variable
      - archive: kafka_home
      - file: kafka_permissions
      - file: kafka_log_permissions
    - watch:
      - file: kafka_cfg
      - file: server_cfg
{%- endif %}



{%- if kf.hipchat_enabled == True %}

kafka_hipchat_notifier_deploy_finished:
  hipchat_integration.send_message:
    - auth_token: {{ kf.auth_token }}
    - room_id: {{ kf.room_id }}
    - message: '[{{ kf.environment }}] - Artifact version [{{ kf.name }}] to the [{{ kf.environment }}] environment [{{ kf.fqdn }}]  successfull.'
    - html: True
    - message_color: green
    - notify: True
    - onchanges:
      - service: kafka_service

kafka_hipchat_notifier_deploy_failed:
  hipchat_integration.send_message:
    - auth_token: {{ kf.auth_token }}
    - room_id: {{ kf.room_id }}
    - message: 'Artifact version [{{ kf.name }}] to the [{{ kf.environment }}] environment [{{ kf.fqdn }}]  failed.'
    - html: True
    - message_color: red
    - notify: True
    - onfail:
      - hipchat_integration: kafka_hipchat_notifier_deploy_finished

{%- endif %}
