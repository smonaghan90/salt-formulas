{%- import 'third-party-components/kafka/settings.sls' as kf with context %}

export KAFKA_HOME={{ kf.home }}
export PATH=$KAFKA_HOME/bin:$PATH
