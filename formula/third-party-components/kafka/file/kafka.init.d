KAFKA_HOME={{ kafka_home }}
KAFKA_USER={{ kafka_user }}
KAFKA_PATH=$KAFKA_HOME/bin
KAFKA_CONFIG=$KAFKA_HOME/config/server.properties
RETVAL=0

case $1 in
  start)
        # Start daemon.
        echo "Starting $KAFKA_USER";
        sudo -u $KAFKA_USER $KAFKA_PATH/kafka-server-start.sh -daemon $KAFKA_CONFIG
        ;;
  stop)
        # Stop daemons.
        echo "Shutting down $KAFKA_USER";
        pid=`ps ax | grep -i 'kafka.Kafka' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          kill -9 $pid
        else
          echo "Kafka was not Running"
        fi
        ;;
  restart)
        $0 stop
        sleep 2
        $0 start
        ;;
  status)
        pid=`ps ax | grep -i 'kafka.Kafka' | grep -v grep | awk '{print $1}'`
        if [ -n "$pid" ]
          then
          echo "Kafka is Running as PID: $pid"
          RETVAL=0
        else
          echo "Kafka is not Running"
          RETVAL=3
        fi

        echo "Return Code: $RETVAL"
  		return $RETVAL
        ;;
  *)
        echo "Usage: $0 {start|stop|restart|status}"
        exit 1
esac
exit $RETVAL
