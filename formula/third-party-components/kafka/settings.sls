#artifacts


{% set artifacts_base_url = pillar['certified_platform_base_url'] + "/"  + pillar['certified_platform_version'] %}
{% set install_package = artifacts_base_url + "/" + pillar['kafka_config']['name'] + '.tgz' %}
{% set install_package_md5 = artifacts_base_url + "/" + pillar['kafka_config']['name']  + '.tgz.MD5' %}
{%- set md5_hash = salt['xanadu_utils.get_md5'](install_package_md5) %}

#base conf
{% set home = pillar['base_install_dir'] + "/" + pillar['kafka_config']['name'] %}
{% set name = pillar['kafka_config']['name'] %}
{% set basedir =  pillar['base_install_dir'] %}
{% set md5_file = pillar['install_packages_dir'] + '/xc_version_kafka.txt' %}
{% set base_logs_dir = pillar['base_log_dir'] %}
{% set logs_dir = pillar['kafka_config']['config']['log_dirs'] %}
{% set config_dir = basedir + "/" + pillar['kafka_config']['name'] + "/config" %}

#Make sure kafka_id is only set once; I am great

{% if salt['grains.get']('kafka_id') %}
{% set kafka_id = salt['grains.get']('kafka_id') %}
{% else %}
{% set temp = salt['grains.setval']('kafka_id',range(10000)|random) %}
{% set kafka_id = salt['grains.get']('kafka_id') %}
{%- endif %}


#conf
{% set p  = salt['pillar.get']('kafka_config', {}) %}
{%- set user = p.get('user', 'kafka') %}
{%- set group = p.get('group', 'kafka') %}
{%- set tech_group = p.get('tech_group', '_tech') %}
{% set pc = p.get('config', {}) %}
{%- set broker_id = pc.get('broker_id', '0') %}
{%- set port = pc.get('port', '9092') %}
{%- set zk_port = pc.get('zk_port', '2181') %}
{%- set host_name = pc.get('host_name', 'None') %}
{%- set advertised_host_name = pc.get('advertised_host_name', 'None') %}
{%- set num_partitions = pc.get('num_partitions', '100') %}
{%- set zookeeper_connect = pc.get('zookeeper_connect', 'localhost:2181') %}
#hipchat api
{% set hipchat_enabled  = salt['pillar.get']('hipchat:enabled', "False") %}
{% set auth_token = pillar['hipchat']['auth_token'] %}
{% set room_id = pillar['hipchat']['room_id'] %}

{% set fqdn = salt['network.get_hostname']() %}
{% set environment = salt['grains.get']('environment') %}





#Log configuration
#Replications may changed based on platform
{%- set num_replica_fetchers = pc.get('num_replica_fetchers','4') %}
{%- set replica_fetch_wait_max_ms = pc.get('replica_fetch_wait_max_ms', '500') %}
{%- set replica_fetch_max_bytes = pc.get('replica_fetch_max_bytes', '2097152') %}
{%- set replica_high_watermark_checkpoint_interval_ms = pc.get('replica_high_watermark_checkpoint_interval_ms', '5000') %}
{%- set replica_socket_receive_buffer_bytes = pc.get('replica_socket_receive_buffer_bytes', '65536') %}
{%- set replica_lag_time_max_ms = pc.get('replica_lag_time_max_ms', '10000') %}
{%- set replica_lag_max_messages = pc.get('replica_lag_max_messages', '4000') %}
{%- set controller_socket_timeout_ms = pc.get('controller_socket_timeout_ms', '30000') %}
{%- set controller_message_queue_size = pc.get('controller_message_queue_size', '10') %}

{% set xmx  =  pc.get('xmx', '1G') %}
{% set xms  =  pc.get('xms', '1G') %}

{%- set log_index_interval_bytes = pc.get('log_index_interval_bytes', '4096') %}
{%- set log_retention_hours = pc.get('log_retention_hours', '12') %}
{%- set log_flush_interval_ms = pc.get('log_flush_interval_ms','10000') %}
{%- set log_flush_interval_messages = pc.get('log_flush_interval_messages', '20000') %}
{%- set log_flush_scheduler_interval_ms = pc.get('log_flush_scheduler_interval_ms', '2000')%}
{%- set log_roll_hours = pc.get('log_roll_hours', '12') %}
{%- set log_retention_check_interval_ms = pc.get('log_retention_check_interval_ms' , '300000')%}
{%- set log_segment_bytes = pc.get('log_segment_bytes', '1073741824')%}
{%- set log_cleaner_enable = pc.get('log_cleaner_enable', 'false') %}
{%- set log_dirs = pc.get('log_dirs', '/xanadu/kafka-logs')%}
#Extract Zookeepers for kafka connection string Verify we have correct cluster number
{%- set force_mine_update = salt['mine.send']('network.get_hostname') %}
{%- set zookeepers_host_dict = salt['mine.get']('roles:zookeeper', 'network.get_hostname', 'grain') %}
{%- set zookeepers_ids = zookeepers_host_dict.keys() %}
{%- set zookeepers_hosts = zookeepers_host_dict.values() %}
{%- set zookeeper_host_num = zookeepers_ids | length() %}
{%- if zookeeper_host_num == 0 %}

{{ 'No zookeeper nodes are defined (you need to set roles:zookeeper at least for one node in your cluster' }}
{%- elif zookeeper_host_num % 2 == 1 %}

{%- set node_count = zookeeper_host_num %}

{%- elif zookeeper_host_num % 2 == 0 %}

{%- set node_count = zookeeper_host_num - 1 %}
{%- endif %}


{%- set zookeepers_with_ids = {} %}
{%- for i in range(node_count) %}
{%- do zookeepers_with_ids.update({zookeepers_ids[i] : '{0:d}'.format(i) + '+' + zookeepers_hosts[i] })  %}
{%- endfor %}

# a plain list of hostnames
{%- set zookeepers = zookeepers_with_ids.values() | sort() %}

{%- set zookeeper_host = (zookeepers | first()).split('+') | last() %}


{%- set connection_string = [] %}
{%- for n in zookeepers %}
{%- do connection_string.append( n.split('+') | last() + ':' + port | string ) %}
{% endfor %}

# return myid for later base broker ID off this
{%- set myid = zookeepers_with_ids.get(grains.id, '').split('+')|first() %}


#templates

{% set tmpl_src = salt['pillar.get']('kafka_config:config:tmpl_loc','salt://third-party-components/kafka/file') %}
{% set tmpl_script = salt['pillar.get']('kafka_config:config:tmpl_script','salt://' + environment + '/kafka/file') %}
