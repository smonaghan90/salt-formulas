#conf
{% set p  = salt['pillar.get']('syndic_config', {}) %}
{% set pc = p.get('config', {}) %}
{%- set syndic_enabled = p.get('enabled', 'False') %}
{%- set syndic_role = p.get('role', 'minion') %}
{%- set syndic_master = p.get('master', 'xxxxxxxxxx') %}
{%- set syndic_general_master = p.get('general_master', 'xxxxxxxxxx') %}

#templates
{% set tmpl_src = salt['pillar.get']('syndic_config:config:tmpl_loc','salt://third-party-components/salt-syndic/files') %}
