{%- import 'third-party-components/salt-syndic/settings.sls' as syndic with context %}

{%- if syndic.syndic_enabled == True %}
{%- if syndic.syndic_role == 'minon' %}

/etc/salt/minion.d/syndic.conf:
  file.managed:
    - source: {{ syndic.tmpl_src}}/minion.jinja
    - template: jinja
    - context:
      syndic_master: {{ syndic.syndic_master }} 

{% else %}
/etc/salt/master.d/syndic.conf:
  file.managed:
    - source: {{ syndic.tmpl_src}}/master.jinja
    - template: jinja

/etc/salt/minion:
  file.managed: {{ syndic.tmpl_src}}/syndic_node.jinja
    - template: jinja
    - context:
      syndic_master: {{ syndic.syndic_master }}

salt-minion:
  service.running:
    - enable: True

{%- endif %}

salt-syndic:
  service.running:
    - enable: True

{%- endif %}
