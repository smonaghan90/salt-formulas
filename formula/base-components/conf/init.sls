{% if grains['roles'] | default (None)  %}
{% for value in grains['roles'] | default (None)%}
{% if value == 'rabbitmq' %}
# Increase the allowed maximum number of open files 
max_open_files:
  file.blockreplace:
    - name: /etc/security/limits.conf
    - marker_start: "# START salt managed zone"
    - marker_end: "# END salt managed zone"
    - content: |
        {{ salt['pillar.get']('rabbitmq:openfiles:type', "rabbitmq") }}        -     nofile        {{ salt['pillar.get']('rabbitmq:openfiles:maxlimit', "65536") }} 
    - append_if_not_found: True
    - backup: '.bak'
    - show_changes: True
{% endif %}
{% endfor %}
{% endif %}
