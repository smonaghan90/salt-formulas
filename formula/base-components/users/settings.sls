#default IPA conf
{%- set ldap_conf_name = salt['pillar.get']('ldap:conf:name', '/etc/ldap/ldap.conf') %}
{%- set ldap_conf_tmpl = salt['pillar.get']('ldap:conf:tmpl', 'salt://base-components/users/files/ldap.conf') %}

{%- set ldap_secret_name = salt['pillar.get']('ldap:secret:name', '/etc/ldap.secret') %}
{%- set ldap_secret_tmpl = salt['pillar.get']('ldap:secret:tmpl', 'salt://base-components/users/files/ldap.secret') %}

{%- set nsswitch_conf_name = salt['pillar.get']('nsswitch:conf:name', '/etc/nsswitch.conf') %}
{%- set nsswitch_conf_tmpl = salt['pillar.get']('nsswitch:conf:tmpl', 'salt://base-components/users/files/nsswitch.conf') %}

{%- set sssd_conf_name = salt['pillar.get']('sssd:conf:name', '/etc/sssd/sssd.conf') %}

{%- set ipa_conf_principal = salt['pillar.get']('ipa:conf:principal', 'enroller') %}
{%- set ipa_conf_password = salt['pillar.get']('ipa:conf:password', 'XbO_YE') %}
{%- set ipa_conf_domain = salt['pillar.get']('ipa:conf:domain', 'xcl.ie') %}
{%- set ipa_conf_realm = salt['pillar.get']('ipa:conf:realm', 'XCL.IE') %}


{%- set ipa_servers = salt['pillar.get']('ipa:servers', ['ipa03.xcl.ie','ipa04.xcl.ie']) %}
