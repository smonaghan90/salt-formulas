{%- import 'base-components/users/settings.sls' as bc with context %}


{% if grains['datacenter'] != 'CIX' %}
ldap_conf:
  file.managed:
    - name: {{ bc.ldap_conf_name }}
    - source: {{ bc.ldap_conf_tmpl }}

ldap_secret:
  file.managed:
    - name: {{ bc.ldap_secret_name }}
    - source: {{ bc.ldap_secret_tmpl }}
    - mode: 600

nsswitch_conf:
  file.managed:
    - name: {{ bc.nsswitch_conf_name }}
    - source: {{ bc.nsswitch_conf_tmpl }}

sssd_sudo:
  cmd.wait:
    - name: sed -i 's/^services.*$/services = nss, pam, ssh, sudo/' /etc/sssd/sssd.conf
    - watch:
      - file: nsswitch_conf

apply_sssd_changes:
  cmd.wait:
    - name: sudo service sssd restart ; sudo service ssh restart
    - watch:
      - file: nsswitch_conf

# Add centralised users
ipa_install:
  cmd.run:
    - name: /usr/sbin/ipa-client-install  -N -p {{ bc.ipa_conf_principal }} -w {{ bc.ipa_conf_password }} -U --force-join --domain={{ bc.ipa_conf_domain }} --realm={{ bc.ipa_conf_realm }} {% for server in bc.ipa_servers %} --server={{ server }}{% endfor %}
    - creates: {{ bc.sssd_conf_name }}
    - require:
      - file: ldap_conf
      - file: ldap_secret
      - file: nsswitch_conf

restart_salt_minion:
  cmd.wait:
    - name: service salt-minion restart
    - watch:
      - cmd: ipa_install

pam_home_auto_creation_fix:
  file.append:
    - name: /etc/pam.d/common-session
    - text:
      - "session required    pam_mkhomedir.so skel=/etc/skel/ umask=0022"

{% endif %}

{% for group, args in pillar.get('groups', {}).iteritems() %}
{{group}}:
  group.present:
    - name: {{ group }}
    - gid: {{ args['gid'] }}
{% endfor %}

# Add local users
{% for user, args in pillar.get('users', {}).iteritems() %}
{{ args['user'] }}:
  group.present:
    - gid: {{ args['gid'] }}
  user.present:
    - uid: {{ args['uid'] }}
    - shell: /bin/bash
    {% if grains['os'] == 'Ubuntu' %}
    - groups:
      - {{ args['group'] }}
      - {{ args['tech_group'] }}
    {% endif %}
{% endfor %}
