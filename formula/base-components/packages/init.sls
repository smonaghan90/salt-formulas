{% if grains['os'] == 'Ubuntu' %}
base_packages:
  pkg.installed:
    - pkgs:
      - net-tools
      - tcl8.5
      - sendxmpp
      - psmisc
      - ntpdate
      - mc
      - apt-file
      - zsh
      - tmux
      - iputils-ping
      - vim
      - less
      - zip
      - bc
      - lsof
      - sysv-rc-conf
      - libyaml-perl
      - zabbix-agent
      - libjson-perl
      - libconfig-auto-perl
      - lshw
      - screen
      - sysstat
      - iotop
      - htop
      - telnet
      - atop
      - iftop
      - socat
      - iptraf
      - procps
      - strace
      - curl
      - tree
      - nmap
      - fastjar
      - dnsutils
      - dbus

trusty_repo:
  pkgrepo.managed:
    - humanname: Trusty Repository
    - name: deb http://ie.archive.ubuntu.com/ubuntu/ trusty universe
    - file: /etc/apt/sources.list.d/trusty.list


/etc/apt/preferences.d/ipa:
  file.append:
    - text: |
        Package: freeipa-client
        Pin: version 3.3.4-0ubuntu3
        Pin-Priority: 1000

        Package: python-freeipa
        Pin: version 3.3.4-0ubuntu3
        Pin-Priority: 1000

{% if grains['osrelease'] == '14.04' %}
freeipa:
  pkg.installed:
    - pkgs:
      - freeipa-client
    - allow_updates: false
{% elif grains['osrelease'] != '14.04' %}

freeipa:
  pkg.installed:
    - pkgs:
      - python-freeipa
      - freeipa-client
    - require:
      - pkgrepo: trusty_repo
{% endif %}

{% endif %}




enable_minion_at_boot:
  cmd.wait:
    - name: sysv-rc-conf salt-minion on
    - watch:
      - file: /etc/apt/preferences.d/ipa
