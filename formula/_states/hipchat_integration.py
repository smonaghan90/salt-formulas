import salt.exceptions

def __virtual__():
    """
    Load if the module hipchat_integration exists
    """

    return 'hipchat_integration' if 'hipchat_integration.send_hipchat_message' in __salt__ else False


def send_message(auth_token, room_id,  message, html=True, message_color='yellow', notify=False, name="Test"):
    """Sends a notification message to a HipChat room.

    For more information see HipChat's API V2 documentation:
    https://www.hipchat.com/docs/apiv2/method/send_room_notification

    :param name: The Room Notification Token
    :type name: str
    :param room_id: The name or id of the room
    :type room_id: int or str
    :param message: the HTML or text message to send
    :type message: str
    :param html: Determines if the format of the ``message`` is HTML
                 or plain text
    :type html: bool
    :param message_color: The background color for the message
    :type message_color: str
    :param notify: Whether or not this message should trigger a
                   notification for people in the room (sound, color, etc.)
    :type notify: bool
    """
    
    available_colors = ['yellow', 'red', 'green', 'purple', 'gray', 'random']
    if message_color not in available_colors:
        raise ValueError('\'%s\' is not a valid color, possible colors are: %s'
                         % (message_color, ', '.join(available_colors)))

    result = __salt__['hipchat_integration.send_hipchat_message'](auth_token, room_id, message, html, message_color, notify)
    
    ret = {
           'auth_token': auth_token,
           'result': True,
           'changes': {},
           'comment': message,
           'name': name
          }

    return ret