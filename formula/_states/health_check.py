# -*- coding: utf-8 -*-

def __virtual__():
    """
    Load if the module health_check exists
    """

    return 'health_check' if 'health_check.available' in __salt__ else False



def wait_for_url(name, url='https://www.google.ie', match_text=None, timeout=180, username=None, password=None):
    """
    Wait for a url to load

    Notice that if the server is not accessible we will wait
    for the value of the timeout for it to start

    url : https://www.google.ie
        the URL of the web page we want to contact
    timeout : 180
        timeout for the html page to be available

    Example:

    .. code-block:: yaml

        jboss-service:
          service:
            - running
            - name: jboss
            - enable: True

        wait-for-application-a:
          health_check.wait-for-url:
            - timeout: 300
            - url: http://10.160.7.2/index.html
            - match_text: text to match
            - username: user
            - password: password
            - require:
              - service: jboss-service

        application_a:
          jboss:
            - war_deployed
            - name: /ran
            - war: salt://jenkins-1.2.4.war
            - require:
              - health_check: wait-for-application-a
    """

    result = __salt__['health_check.available'](url, match_text, timeout, username, password)
    ret = {
            'name': name,
            'result': result,
            'changes': {},
            'comment': ('{0} is ready'.format(url) if result
               else '{0} is not ready'.format(url))
          }

    return ret
