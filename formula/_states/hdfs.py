import salt.exceptions

def __virtual__():
    """
    Load if the module hdfs exists
    """
    return 'hdfs' if 'hdfs.put_local_file' in __salt__ else False



def put(name,file_location,master,env,version):
    """
    Retrieve data node from hdfs master and push file to datanode
    Directory structure example '/dev/snapshots/*.jar'

    """
    result = __salt__['hdfs.put_local_file'](file_location,master,env,version)

    return {
            'name': name,
            'result': True,
            'changes': {},
            'file_location' : file_location,
            'comment': 'Uploaded ' + file_location + ' to ' + master + " environment " + env + " \n path:/ " + version
          }

    return ret
