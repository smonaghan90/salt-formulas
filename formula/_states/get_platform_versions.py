# -*- coding: utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import salt

def __virtual__():
    """
    Load if the module get_versions exists
    """

    return 'get_platform_versions' if 'get_platform_versions.get' in __salt__ else False


def versions(name, info_list, alone=False, ver_string=''):
    infos = list((el for el in info_list))
    val = __salt__['get_platform_versions.get'](infos, infos, alone, ver_string)
    return {
            'name': name,
            'result': True,
            'changes': {},
            'comment': val
          }

