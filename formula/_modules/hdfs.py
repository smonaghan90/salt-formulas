import requests
import re
import logging
import os


log = logging.getLogger(__name__)


def put_file(latest,master,env,version):
    # fetch Artifact from nexus for this request anything that is not 200 is an error
    # latest = hdfs.put_file http://nexus.inf.cdc.ot01.net:8081/nexus/service/local/artifact/maven/content?g=com.xanaduconsultancy&a=xdc-core&v=2.0.4-SNAPSHOT&r=snapshots&e=war
    # master = http://10.140.44.2:50070/
    # env = dev , qa , pp prod
    # e.g #put_file("http://nexus.inf.cdc.ot01.net:8081/nexus/service/local/artifact/maven/content?g=com.xanaduconsultancy&a=xdc-integration-module-common&v=2.0.4-SNAPSHOT&r=snapshots&e=jar","192.168.100.115","qa","snapshot")

    try:
        response = requests.get(latest,stream=True)
        if response.status_code !=  200:
            log.warning("Error: Unexpected response {}".format(response.content))
            return "Error: Unexpected response {}".format(response.content)

        filename = response.headers['content-disposition']
        fname = re.findall("filename=(.+)",filename)[0]
        fname =  fname.replace('"','')

    except requests.exceptions.RequestException as e:
        log.warning("Error: {} ".format(e))
        return "Error: {} ".format(e)

    # Write the content of the response to a file taking the name from the headers
    try:
        with open(fname, "wb") as jar:
            jar.write(response.content)
            jar.close()
    except IOError:
        print "IOError could not write file!"
        exit(0)

    try:
        hdfsr = requests.put("http://" + master + ":50070/webhdfs/v1/" + env + "/" + version + "/" + fname + "?op=CREATE&data=true&overwrite=true")
        if hdfsr.status_code != 201:
            log.warning("Error: Unexpected response {}".format(hdfsr.content))
            return "Error: Unexpected response {}".format(hdfsr.content)

        node_url =  hdfsr.url
        payload = {'file' : open(fname,'rb')}
        hdfsr = requests.put(node_url,files=payload)

        if hdfsr.status_code != 201:
            log.warning("Error: Unexpected response {}".format(hdfsr.content))
            return "Error: Unexpected response {}".format(hdfsr.content)
        log.log("File " + fname + "Copied to " + env + "/" + version +  " on node " + node_url )

    except requests.exceptions.RequestException as e:
        return "Error: {}".format(e)


def put_local_file(file_location,master,env,version):
    # put a local file into HDFS
    # e.g #put_local_file("/path/xdc-polling-manager.properties","10.140.44.2","dev","config")

    file_name = os.path.basename(file_location)
    try:
        hdfsr = requests.put("http://" + master + ":50070/webhdfs/v1/" + env + "/" + version + "/" + file_name + "?op=CREATE&data=true&overwrite=true")
        if hdfsr.status_code != 201:
            return "Error: Unexpected response {}".format(hdfsr.content)

        node_url =  hdfsr.url
        payload = {'file' : open(file_location,'rb')}
        hdfsr = requests.put(node_url,files=payload)
        print hdfsr

        if hdfsr.status_code != 201:
            return "Error: Unexpected response {}".format(hdfsr.content)

    except requests.exceptions.RequestException as e:
        return "Error: {}".format(e)
