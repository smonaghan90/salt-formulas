import salt

def _get_ubuntu_pkg_version(pkg):
  cmd = "dpkg-query --showformat='${Version}' --show "
  cmd += pkg
  return __salt__['cmd.run'](cmd)


def get(pkg_name):
  """
  Used to get info about the ubuntu packages versions

  pkg_name: package you are interested to know

  CLI Examples:

  .. code-block:: bash

  salt \*  get_ubuntu_pkg_version.get "wget"
  """
  return _get_ubuntu_pkg_version(pkg_name)