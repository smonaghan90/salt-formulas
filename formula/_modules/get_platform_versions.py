import salt
import logging

log = logging.getLogger(__name__)


def _get_grain(grain):
    return __salt__['grains.get'](grain)


def _get_grains(all_grains):
    ret = ""
    for grain in all_grains:
        if _get_grain(grain) is '':
            continue
        ret += "{} version is {}\n".format(grain, _get_grain(grain))
    return ret

##########


def _get_pillar(pillar):
    return __salt__['pillar.get'](pillar)


def _get_pillars(all_pillars):
    ret = ""
    for pillar in all_pillars:
        if _get_pillar(pillar) is '':
            continue
        ret += "{} version is {}\n".format(pillar, _get_pillar(pillar))
    return ret

##########

def get(pillars_list, grains_list, alone=False, ver_string=''):
    """
    Used to get info about the components versions

    pillars_list: list of the pillars that contain version
    grains_list: list of the grains that contain version
    alone: if False we're using grains/pillars
    ver_string: ig alone is True this is the string we're going to use.

    CLI Examples:

    .. code-block:: bash

    salt \*  get_platform_versions.get "['zookeeper:name']" "[os]"
    salt \* get_platform_versions.get "[dummy]" "[dummy]" True "test"
    """
    result = None
    if alone is True:
        result = ver_string
    else:
        try:
            result = _get_grains(grains_list)
        except Exception, e:
            log.warning(e)
        if result:
            log.trace("Version string from grains is : " + result)
        else:
            log.warning("No valid grains data found")

        try:
            result += _get_pillars(pillars_list)
        except Exception, e:
            log.warning(e)
        if result:
            log.trace("Version string from pillars is : " + result)
        else:
            log.warning("No valid pillars data found")

    return result
