import json
import requests

def send_hipchat_message(auth_token, room_id,  message, html=True, message_color='yellow', notify=False):

    url = 'https://api.hipchat.com/v2/room/%s/notification' % room_id

    payload = {
        'message': message,
        'message_format': 'html' if html else 'text',
        'color': message_color,
        'notify': notify
    }

    r = requests.post(url, params={'auth_token': auth_token},
                      headers={'Content-Type': 'application/json'},
                      data=json.dumps(payload))

    r.raise_for_status()
    return r
