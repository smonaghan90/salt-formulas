# -*- coding: utf-8 -*-

import salt
import urllib2
import logging


log = logging.getLogger(__name__)


def get_md5(url='http://www.xanaduconsultancy.com/'):
    """
     .. code-block:: bash

        salt '*' xanadu_utils.get_md5
        salt '*' xanadu_utils.get_md5 http://localhost:8083/nexus/content/sites/certified_platform/1.0/jdk1.7.0_79.tar.gz.MD5
    """
    result = None
    try:
        response = urllib2.urlopen(url, timeout=60)
        data = response.read()
        result = data.split()[0]
    except Exception, e:
        log.warning(e)
    if result:
        log.debug("the MD5 is : " + result)
    else:
        log.warning("Could not find the md5 on url " + url)
    return result