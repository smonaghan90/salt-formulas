# -*- coding: utf-8 -*-

import urllib2
import time
import logging
import socket
import base64

log = logging.getLogger(__name__)


def available(url='https://www.google.ie', match_text=None, timeout=60, username=None, password=None):
    """
    Used to test if a url is available and optionally match a text string on the content of the page

    url : https://www.google.ie
        the URL of the server to contact
    timeout : 180
        timeout for HTTP request

    CLI Examples:

    .. code-block:: bash

        salt '*' health_check.available
        salt '*' health_check.available https://www.google.ie
    """

    wget_result = _wget(url, timeout=timeout, username=username, password=password)
    result = wget_result['res']
    if result and match_text:
        result = False
        for line in wget_result['msg']:
            if match_text in line:
                result = True
                break

    return result


def _wget(url='http://www.xanaduconsultancy.com', timeout=60, username=None, password=None):
    """
    A private function used to issue a wget to a page

    url
        the URL of the server to contact
        example: http://localhost:8080/index.html
    opts
        a dict of arguments
    timeout
        timeout for HTTP request

    return value is a dict in the from of::

        {
            res: [True|False]
            msg: list of lines we got back from the manager or error
        }
    """

    ret = {
        'res': True,
        'msg': []
    }    

    response_code = -1
    counter = 0
    timeout = int(timeout)

    request = urllib2.Request(url)
    if username:
        log.debug("Setting up the authentication")
        base64string = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')
        request.add_header("Authorization", "Basic %s" % base64string)

    while response_code != 200 and counter < timeout:
        try:                
            log.trace("Connecting... to {0}".format(url))
            response = urllib2.urlopen(request, timeout=timeout)
            log.trace("Connection opened")
            response_code = response.getcode()    
            ret['res'] = True        
            ret['msg'] = response.read().splitlines()                                    
        except Exception, e:   
            ret['res'] = False
            ret['msg'] = 'Failed to contact application'
            log.warning(e)
        except socket.timeout, t:
            ret['res'] = False
            ret['msg'] = 'Failed to contact application'
            log.warning("Timed out")

        counter += 1
                
        if response_code != 200:            
            ret['res'] = False
            time.sleep(float(1))

    log.debug('Result #{0} #{1}'.format(ret['res'], ret['msg']))                                        

    return ret
